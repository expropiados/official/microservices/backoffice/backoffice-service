package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence;

import com.amazonaws.util.IOUtils;
import org.expropiados.cuidemonos.backofficeservice.config.backoffice.storage.AwsStorageConfig;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.converter.MultipartFileConverter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.s3.ClientCompanyImageS3Adapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.thirdparty.AwsStorageAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@TestPropertySource("/application-test.yml")
@Import({CompanyPersistenceAdapter.class, ClientCompanyImageS3Adapter.class})
@ComponentScan(basePackageClasses = {CompanyMapper.class, AwsStorageAdapter.class, AwsStorageConfig.class, MultipartFileConverter.class})
@ContextConfiguration
@Disabled("Disable until a better way to test with sql without duplicated data errors has been found!")
class RegisterCompanyPersistenceTest {
    @Autowired
    private CompanyPersistenceAdapter companyPersistenceAdapter;

    @Autowired
    private ClientCompanyImageS3Adapter clientCompanyImageS3Adapter;

    private static MultipartFile multipartFile;

    @BeforeEach
    void init() throws IOException {
        File file = new File("src/test/resources/files/ArbolDeProblemas.jpg");
        FileInputStream input = new FileInputStream(file);
        multipartFile = new MockMultipartFile("test1",
                file.getName(), MediaType.IMAGE_JPEG_VALUE, IOUtils.toByteArray(input));
    }

    @Test
    void registerCompanyWithoutImageSuccess(){
        var registeredCompany = new ClientCompanyToRegister("PUCP",
                "1234567890", "test@gmail.com", "Av. Universitaria 123", "993823924");
        var uuid = UUID.randomUUID();
        var company = companyPersistenceAdapter.registerCompany(registeredCompany, uuid);
        assertThat(company).isNotNull();
        assertThat(company.getUuid()).isEqualTo(uuid);
        assertThat(company.getFullName()).isEqualTo("El nombre completo es PUCP");
    }

    @Test
    void registerCompanyWithImageException(){
        var registeredCompany = new ClientCompanyToRegister("PUCP",
                "1234567890", "test@gmail.com", "Av. Universitaria 123", "993823924");

        //registeredCompany.setMultipartFile(multipartFile);
        var imageURL = clientCompanyImageS3Adapter.
                saveClientCompanyImageFile(multipartFile);
        assertThat(imageURL).isNull();
    }
}
