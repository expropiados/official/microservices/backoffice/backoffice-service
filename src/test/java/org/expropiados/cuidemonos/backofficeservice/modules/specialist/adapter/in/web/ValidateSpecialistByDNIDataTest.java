package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.ValidateSpecialistByDocumentUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = ValidateSpecialistByDocumentController.class)
@Disabled("Disabled because of authorization headers missed. Mock jwt helper for controller first with right credentials.")
class ValidateSpecialistByDNIDataTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ValidateSpecialistByDocumentUseCase validateSpecialistByDocumentUseCase;

    @Test
    void testValidateSpecialistByDNISuccess() throws Exception {

        var dni = "88888888";

        mockMvc.perform(get("/specialists/checking",dni)
                .param("document", dni)).andExpect(status().isOk());
        then(validateSpecialistByDocumentUseCase).should()
                .validateSpecialistByDocument(dni);

    }

}
