package org.expropiados.cuidemonos.backofficeservice.modules.company.application;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
public class ValidateClientCompanyServiceTest {

    private final GetClientCompanyPort getClientCompanyPort = Mockito.mock(GetClientCompanyPort.class);
    private final ValidateClientCompanyService validateClientCompanyService = new ValidateClientCompanyService(getClientCompanyPort);

    @Test
    void validateClientCompanyServiceTest(){
        var clientCompany = new Company(1L,"ARTROSCOPICTRAUMA S.A.C", "20538856674","Jr. Ucayali","992847123","test@gmail.com", null,UUID.randomUUID());

        when(getClientCompanyPort.getClientCompanyByRUC(eq(clientCompany.getRuc()))).thenReturn(clientCompany);

        ClientCompanyChecked result = validateClientCompanyService.validateClientCompany(clientCompany.getRuc());
        assertThat(result).isNotNull();
        assertThat(result.getIsRegistered()).isEqualTo(true);
        assertThat(result.getClientCompany()).isEqualTo(clientCompany);
    }
}
