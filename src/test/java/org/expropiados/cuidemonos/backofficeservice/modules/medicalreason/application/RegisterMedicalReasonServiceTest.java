package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.RegisterMedicalReasonService;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.RegisterMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class RegisterMedicalReasonServiceTest {

    private final RegisterMedicalReasonPort registerMedicalReasonPort = Mockito.mock(RegisterMedicalReasonPort.class);
    private final RegisterMedicalReasonService registerMedicalReasonService = new RegisterMedicalReasonService(registerMedicalReasonPort);

    @Test
    void registerMedicalReasonFailure(){
        when(registerMedicalReasonPort.registerMedicalReason(null)).thenReturn(null);
        var result = registerMedicalReasonService.registerMedicalReason(null);
        assertThat(result).isNull();
    }

    @Test
    void registerMedicalReasonEmpty(){
        var medicalReasonToRegister = new MedicalReasonToRegister();
        medicalReasonToRegister.setDescription("Descripcion");
        medicalReasonToRegister.setName("Nombre");

        var medicalReason = new MedicalReason();
        medicalReason.setDescription("Descripcion");
        medicalReason.setName("Nombre");
        medicalReason.setId(1L);

        when(registerMedicalReasonPort.registerMedicalReason(medicalReasonToRegister)).thenReturn(medicalReason);
        var result = registerMedicalReasonService.registerMedicalReason(medicalReasonToRegister);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
    }
}
