package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMedicalReasonMonthlyFreqReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetMedicalReasonMonthlyFreqReportServiceTest {
    private final GetMedicalReasonMonthlyFreqReportPort getFreqReportPort = Mockito.mock(GetMedicalReasonMonthlyFreqReportPort.class);
    private final GetMedicalReasonMonthlyFreqReportService getFreqReportService = new GetMedicalReasonMonthlyFreqReportService(getFreqReportPort);

    @Test
    void getMedicalReasonMonthlyFreqReportFailure() throws IOException {
        var clientCompanyId = 1L;
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);

        when(getFreqReportPort.getMedicalReasonMonthlyFreqReport(startDate, endDate, clientCompanyId)).thenReturn(null);
        var result = getFreqReportService.getMedicalReasonMonthlyFreqReport(startDate, endDate, clientCompanyId);

        assertThat(result).isNull();
    }
}
