package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@Tag("UnitTest")
@Disabled("Disabled until we can configure the api for testing")
class ValidateSpecialistByDNIServiceTest {

    private final GetSpecialistsPort getSpecialistPort = Mockito.mock(GetSpecialistsPort.class);
    private final GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
    private final ValidateSpecialistByDocumentService validateSpecialistByDNIService = new ValidateSpecialistByDocumentService(getSpecialistPort,getUserPort);
    private final TypeDocument type = TypeDocument.DNI;
    private final User user = User.builder().id(1L).uuid(UUID.randomUUID())
            .document("76272879").name("Melvin").fatherLastname("Gomez").motherLastname("Garcia")
            .birthdate(LocalDate.parse("2020-02-28"))
            .build();
    private final Specialist specialist = Specialist.builder().id(1L).user(user).build();

    @Test
    void validateSpecialistByDNIServiceTest(){
        String dni = "76829482";
        when(getUserPort.getUserByDocument(dni)).thenReturn(user);
        when(getSpecialistPort.getSpecialistByIdUser(any(Long.class))).thenReturn(null);

        SpecialistChecked result = validateSpecialistByDNIService.validateSpecialistByDocument(user.getDocument());
        assertThat(result).isNotNull();
        assertThat(result.getIsSpecialist()).isFalse();
        //assertThat(result.getUser()).isEqualTo(user);
    }

    @Test
    void validateSpecialistRegisteredByDNISuccess(){
        String dni = "76829482";
        when(getUserPort.getUserByDocument(dni)).thenReturn(user);
        when(getSpecialistPort.getSpecialistByIdUser(eq(user.getId()))).thenReturn(specialist);

        SpecialistChecked result = validateSpecialistByDNIService.validateSpecialistByDocument(user.getDocument());
        assertThat(result).isNotNull();
        //assertThat(result.getIsSpecialist()).isEqualTo(true);
        //assertThat(result.getUser()).isEqualTo(user);
    }

    @Test
    void validateUserNotRegisteredByDocumentSuccess(){
        String dni = "76272879";
        when(getUserPort.getUserByDocument(dni)).thenReturn(null);
        var result = validateSpecialistByDNIService.validateSpecialistByDocument(dni);
        assertThat(result).isNotNull();
        assertThat(result.getIsSpecialist()).isFalse();
        assertThat(result.getUser()).isNull();
        assertThat(result.getUserApi().getNumeroDocumento()).isEqualTo(dni);
    }
}
