package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMonthlyPostPollReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetMonthlyPostPollReportServiceTest {

    private final GetMonthlyPostPollReportPort getPostPollReportPort = Mockito.mock(GetMonthlyPostPollReportPort.class);
    private final GetMonthlyPostPollReportService getPostPollReportService = new GetMonthlyPostPollReportService(getPostPollReportPort);

    @Test
    void getMonthlyPostPollReportFailure() throws IOException {
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);
        var specialistUUID = UUID.randomUUID();

        when(getPostPollReportPort.getMonthlyPostPollReport(startDate, endDate, specialistUUID)).thenReturn(null);
        var result = getPostPollReportService.getMonthlyPostPollReport(startDate, endDate, specialistUUID);

        assertThat(result).isNull();
    }
}
