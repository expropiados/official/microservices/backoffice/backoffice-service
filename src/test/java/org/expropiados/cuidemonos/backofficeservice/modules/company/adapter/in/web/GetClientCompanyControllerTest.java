package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompaniesUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetClientCompaniesController.class)
@Disabled("Disabled because of authorization headers missed. Mock jwt helper for controller first with right credentials.")
class GetClientCompanyControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GetClientCompaniesUseCase getClientCompaniesUseCase;

    @Test
    void getClientCompanySuccess() throws Exception{
        mockMvc.perform(get("/companies").
                header("Content-Type", "application/json")).
                andExpect(status().isOk());

        then(getClientCompaniesUseCase).should().getClientCompanies();
    }
}
