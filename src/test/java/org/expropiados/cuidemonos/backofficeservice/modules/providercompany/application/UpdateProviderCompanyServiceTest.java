package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.UpdateProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class UpdateProviderCompanyServiceTest {
    private final UpdateProviderCompanyPort updateProviderCompanyPort = Mockito.mock(UpdateProviderCompanyPort.class);
    private final UpdateProviderCompanyService updateProviderCompanyService = new UpdateProviderCompanyService(updateProviderCompanyPort);

    @Test
    void updateProviderCompanySuccess(){
        var provider = ProviderCompany.builder().id(1L).build();
        when(updateProviderCompanyPort.updateProviderCompany(any(ProviderCompanyToUpdate.class))).thenReturn(provider);

        var result = updateProviderCompanyService.updateProviderCompany(new ProviderCompanyToUpdate("999999999", "kaskdasd", 1L, UUID.randomUUID(), "test@gmail.com", "representante@gmail.com", "Juan Diego", "Av Siempre Viva", "998271283", null));

        assertThat(result).isNotNull();
    }

    @Test
    void updateProviderCompanyFailure(){
        when(updateProviderCompanyPort.updateProviderCompany(any(ProviderCompanyToUpdate.class))).thenReturn(null);

        var result = updateProviderCompanyService.updateProviderCompany(new ProviderCompanyToUpdate("999999999", "kaskdasd", 1L, UUID.randomUUID(), "test@gmail.com", "representante@gmail.com", "Juan Diego", "Av Siempre Viva", "998271283", null));

        assertThat(result).isNull();
    }
}
