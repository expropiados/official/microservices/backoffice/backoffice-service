package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.RemoveMedicalReasonService;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.CheckIfExistMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.DeleteMedicalReasonPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

@Tag("UnitTest")
class RemoveMedicalReasonServiceTest {
    private final CheckIfExistMedicalReasonPort checkIfExistMedicalReasonPort = Mockito.mock(CheckIfExistMedicalReasonPort.class);
    private final DeleteMedicalReasonPort deleteMedicalReasonPort = Mockito.mock(DeleteMedicalReasonPort.class);
    private final RemoveMedicalReasonService removeMedicalReasonService = new RemoveMedicalReasonService(checkIfExistMedicalReasonPort, deleteMedicalReasonPort);

    @Test
    void removeMedicalReasonSuccess(){
        when(checkIfExistMedicalReasonPort.checkIfExistMedicalReason(1L)).thenReturn(true);
        removeMedicalReasonService.removeMedicalReason(1L);
    }

    @Test
    void removeMedicalReasonFailure(){
        when(checkIfExistMedicalReasonPort.checkIfExistMedicalReason(1L)).thenReturn(false);
        try{
            removeMedicalReasonService.removeMedicalReason(1L);
        }
        catch (Exception ex){

        }
    }
}
