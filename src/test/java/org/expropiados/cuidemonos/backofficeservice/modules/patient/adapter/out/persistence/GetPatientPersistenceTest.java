package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Tag("IntegrationTest")
@DataJpaTest
@Import({PatientPersistenceAdapter.class})
@ContextConfiguration
@ComponentScan(basePackageClasses = {PatientMapper.class, UserMapper.class})
@ActiveProfiles("test")
@Disabled("Disable until a better way to test with sql without duplicated data errors has been found!")
@Sql({"classpath:sql/GetUserTest.sql", "classpath:sql/GetPatientTest.sql"})
class GetPatientPersistenceTest {
    @Autowired
    private PatientPersistenceAdapter patientPersistenceAdapter;

    @Test
    void getPatientSuccess(){
        var result = patientPersistenceAdapter.getPatients();
        assertThat(result).isNotNull();
//        assertThat(result.getData()).isNotEmpty();
//        assertThat(result.getTotal()).isEqualTo(1);
//        assertThat(result.getData().get(0).getId()).isEqualTo(10);
    }
}
