package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.GetPaginatedMedicalReasonsService;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.GetPaginatedMedicalReasonsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetPaginatedMedicalReasonsServiceTest {
    private final GetPaginatedMedicalReasonsPort getPaginatedMedicalReasonsPort = Mockito.mock(GetPaginatedMedicalReasonsPort.class);
    private final GetPaginatedMedicalReasonsService getPaginatedMedicalReasonsService = new GetPaginatedMedicalReasonsService(getPaginatedMedicalReasonsPort);

    @Test
    void getPaginatedMedicalReasonsEmpty(){
        var paginator = new Paginator<MedicalReason>(new ArrayList<MedicalReason>(), 0, 10, 0L);
        when(getPaginatedMedicalReasonsPort.getPaginatedMedicalReasons(any(Filters.class))).thenReturn(paginator);
        var filters = Filters.builder().page(0).pageSize(10).search("").build();
        var result = getPaginatedMedicalReasonsService.getPaginatedMedicalReasons(filters);

        assertThat(result).isNotNull();
        assertThat(result.getData()).isEmpty();
    }

    @Test
    void getPaginatedMedicalReasonFailure(){
        when(getPaginatedMedicalReasonsPort.getPaginatedMedicalReasons(any(Filters.class))).thenReturn(null);
        var result = getPaginatedMedicalReasonsService.getPaginatedMedicalReasons(null);
        assertThat(result).isNull();
    }

    @Test
    void setGetPaginatedMedicalReasonsSuccess(){
        var medicalReason = new MedicalReason();
        medicalReason.setDescription("Descripcion");
        medicalReason.setName("Nombre");
        medicalReason.setId(1L);

        var medicalReasons = new ArrayList<MedicalReason>(1);
        medicalReasons.add(medicalReason);
        var paginator = new Paginator<MedicalReason>(medicalReasons, 0, 10, 0L);
        when(getPaginatedMedicalReasonsPort.getPaginatedMedicalReasons(any(Filters.class))).thenReturn(paginator);
        var filters = Filters.builder().page(0).pageSize(10).search("").build();
        var result = getPaginatedMedicalReasonsService.getPaginatedMedicalReasons(filters);

        assertThat(result).isNotNull();
        assertThat(result.getData()).isNotEmpty();
        assertThat(result.getData().size()).isEqualTo(1);
    }
}
