package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class ValidatePatientByDocumentServiceTest {

    private final GetPatientPort getPatientPort= Mockito.mock(GetPatientPort.class);
    private final GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
    private final ValidatePatientByDocumentService validatePatientByDocumentService =
            new ValidatePatientByDocumentService(getUserPort, getPatientPort);

    private final User user = User.builder().id(1L).uuid(UUID.randomUUID())
            .document("76272879").name("Melvin").fatherLastname("Gomez").motherLastname("Garcia")
            .build();

    private final Patient patient = Patient.builder().id(1L).user(user).uuid(UUID.randomUUID()).build();

    @Test
    void validateUserRegisteredByDNISuccess(){
        String dni = "76272879";
        when(getUserPort.getUserByDocument(dni)).thenReturn(user);
        when(getPatientPort.getByUserId(any(Long.class))).thenReturn(Optional.empty());
        var result = validatePatientByDocumentService.validatePatientByDocument(dni);
        assertThat(result).isNotNull();
        assertThat(result.getIsPatient()).isFalse();
        assertThat(result.getUser()).isNotNull();
        assertThat(result.getUser().getDocument()).isEqualTo(dni);
    }

    @Test
    void validatePatientRegisteredByDNISuccess(){
        String dni = "76272879";
        when(getUserPort.getUserByDocument(dni)).thenReturn(user);
        when(getPatientPort.getByUserId(1L)).thenReturn(Optional.of(patient));
        var result = validatePatientByDocumentService.validatePatientByDocument(dni);
        assertThat(result).isNotNull();
        assertThat(result.getIsPatient()).isTrue();
        assertThat(result.getUser()).isNotNull();
        assertThat(result.getUser().getDocument()).isEqualTo(dni);
    }

    @Test
    void validateUserNotRegisteredByDNISuccess(){
        String dni = "76272879";
        when(getUserPort.getUserByDocument(dni)).thenReturn(null);
        var result = validatePatientByDocumentService.validatePatientByDocument(dni);
        assertThat(result).isNotNull();
        assertThat(result.getIsPatient()).isFalse();
        assertThat(result.getUser()).isNull();
        assertThat(result.getUserApi().getNumeroDocumento()).isEqualTo(dni);
    }
}
