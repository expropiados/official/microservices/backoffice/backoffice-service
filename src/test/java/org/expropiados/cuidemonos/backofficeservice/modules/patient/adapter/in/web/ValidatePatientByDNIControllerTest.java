package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.ValidatePatientByDocumentUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = ValidatePatientByDNIController.class)
@Disabled("Disabled because of authorization headers missed. Mock jwt helper for controller first with right credentials.")
class ValidatePatientByDNIControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ValidatePatientByDocumentUseCase validatePatientByDocumentUseCase;

    @Test
    void testValidateSpecialistByDNISuccess() throws Exception {
        var dni = "88888888";
        var headers = new HttpHeaders();

        mockMvc.perform(
                get("/patients/checking", dni)
                        .param("dni", dni)
                        .headers(headers))
                .andExpect(status()
                        .isOk());

        then(validatePatientByDocumentUseCase)
                .should()
                .validatePatientByDocument(dni);
    }
}
