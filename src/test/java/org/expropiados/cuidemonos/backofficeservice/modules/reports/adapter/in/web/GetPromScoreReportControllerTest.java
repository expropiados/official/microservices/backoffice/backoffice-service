package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetPromScoreReportController.class)
class GetPromScoreReportControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private GetPromScoreReportUseCase getPromScoreReportUseCase;

    @Test
    void getPromScoreMonthlyReport() throws Exception{
        String params = "{ \"initDate\": \"2021-07-01\", \"endDate\": \"2021-10-01\", " +
                "\"clientCompanyId\": \""+1+"\"}";

        var headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        mockMvc.perform(get("/reports/appointment/prom_score").
                content(params).
                headers(headers)).
                andExpect(status().is(400));
    }
}
