package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.TokenBody;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetHoursReportUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetHoursReportController.class)
class GetHoursReportControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetHoursReportUseCase getHoursReportUseCase;

    @MockBean
    private JwtHelper jwtHelper;

    @Test
    void getHoursReport() throws Exception{
        var specialistUUID = UUID.randomUUID();
        String params = "{ \"initDate\": \"2021-07-01\", \"endDate\": \"2021-10-01\", " +
                "\"specialistUUID\": \""+specialistUUID+"\"}";

        var headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        mockMvc.perform(get("/reports/availabilities/hours").
                content(params).
                headers(headers)).
                andExpect(status().is(400));
    }

}
