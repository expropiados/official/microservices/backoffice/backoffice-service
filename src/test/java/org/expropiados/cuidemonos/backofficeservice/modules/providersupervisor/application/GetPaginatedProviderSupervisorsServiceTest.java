package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application;

import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.out.GetPaginatedProviderSupervisorsPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetPaginatedProviderSupervisorsServiceTest {

    private final GetPaginatedProviderSupervisorsPort getPaginatedProviderSupervisorsPort = Mockito.mock(GetPaginatedProviderSupervisorsPort.class);
    private final GetPaginatedProviderSupervisorsService getPaginatedProviderSupervisorsService = new GetPaginatedProviderSupervisorsService(getPaginatedProviderSupervisorsPort);

    @Test
    void getPaginatedProviderSupervisorsFailure(){
        when(getPaginatedProviderSupervisorsPort.getPaginatedProviderSupervisors(null)).thenReturn(null);
        var result = getPaginatedProviderSupervisorsService.getPaginatedProviderSupervisors(null);

        assertThat(result).isNull();
    }
}
