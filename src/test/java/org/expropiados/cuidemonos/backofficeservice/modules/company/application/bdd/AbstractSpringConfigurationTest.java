package org.expropiados.cuidemonos.backofficeservice.modules.company.application.bdd;


import org.junit.Before;
import io.cucumber.spring.CucumberContextConfiguration;
import org.expropiados.cuidemonos.backofficeservice.BackofficeServiceApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = BackofficeServiceApplication.class, loader = SpringBootContextLoader.class)
@CucumberContextConfiguration
public abstract class AbstractSpringConfigurationTest {

    @Autowired(required = false)
    private TestRestTemplate restTemplate;

    protected MockMvc mockMvc;

    @LocalServerPort
    int port;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .build();
    }

    protected static final String HOST = "http://localhost";

    public TestRestTemplate getRestTemplate() {

        return restTemplate != null ? restTemplate : new TestRestTemplate();
    }

    public ResponseEntity<?> invokeRESTCall(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<?> aux) {

        return getRestTemplate().exchange(url, method, requestEntity, aux);
    }

    public HttpHeaders getDefaultHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }


    public String getContextURL(){
        return HOST+":"+((Integer) port).toString();
    }

}
