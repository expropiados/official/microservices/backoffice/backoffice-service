package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetHoursReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetHoursReportServiceTest {

    private final GetHoursReportPort getHoursReportPort = Mockito.mock(GetHoursReportPort.class);
    private final GetHoursReportService getHoursReportService = new GetHoursReportService(getHoursReportPort);

    @Test
    void getHoursReportFailure() throws IOException {
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);
        var specialistUUID = UUID.randomUUID();
        when(getHoursReportPort.getHoursReport(startDate, endDate, specialistUUID)).thenReturn(null);
        var result = getHoursReportService.getHoursReport(startDate, endDate, specialistUUID);

        assertThat(result).isNull();
    }
}
