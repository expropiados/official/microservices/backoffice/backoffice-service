package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPatientUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetPatientController.class)
@Disabled("Disabled because of authorization headers missed. Mock jwt helper for controller first with right credentials.")
class GetPatientControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GetPatientUseCase getPatientUseCase;

    @Test
    void getPatientSuccess() throws Exception{
        mockMvc.perform(get("/patients").
                header("Content-Type", "application/json")).
                andExpect(status().isOk());
        then(getPatientUseCase).should().getPatients();

    }
}
