package org.expropiados.cuidemonos.backofficeservice.modules.user.application;

import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.RegisterUserRoleService;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserRolePort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class RegisterUserServiceTest {
    private final RegisterUserRolePort registerUserRolePort = Mockito.mock(RegisterUserRolePort.class);
    private final RegisterUserRoleService registerUserRoleService = new RegisterUserRoleService(registerUserRolePort);

    @Test
    void registerUserRoleSuccess(){
        doNothing().when(registerUserRolePort).registerUserRole("test2@gmail.com", UserRole.CLIENT_SUPERVISOR);
        registerUserRoleService.registerUserRole("test2@gmail.com", UserRole.CLIENT_SUPERVISOR);
    }
}
