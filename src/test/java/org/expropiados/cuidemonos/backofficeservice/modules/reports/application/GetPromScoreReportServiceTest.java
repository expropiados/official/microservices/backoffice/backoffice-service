package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetPromScoreReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetPromScoreReportServiceTest {

    private final GetPromScoreReportPort getPromScoreReportPort = Mockito.mock(GetPromScoreReportPort.class);
    private final GetPromScoreReportService getPromScoreReportService = new GetPromScoreReportService(getPromScoreReportPort);

    @Test
    void getPromScoreMonthlyReportFailure() throws IOException {
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);
        var specialistUUID = UUID.randomUUID();

        when(getPromScoreReportPort.getPromPostPollReport(any(GetPromScoreReportDTO.class))).thenReturn(null);
        var result = getPromScoreReportService.getPromPostPollReport(null);

        assertThat(result).isNull();
    }
}
