package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetPromScoreMonthlyReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetPromScoreMonthlyReportServiceTest {

    private final GetPromScoreMonthlyReportPort getPromScoreReportPort = Mockito.mock(GetPromScoreMonthlyReportPort.class);
    private final GetPromScoreMonthlyReportService getPromScoreMonthlyReportService = new GetPromScoreMonthlyReportService(getPromScoreReportPort);

    @Test
    void getPromScoreMonthlyReportFailure() throws IOException {
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);
        var specialistUUID = UUID.randomUUID();

        when(getPromScoreReportPort.getPromScoreMonthlyReport(startDate, endDate)).thenReturn(null);
        var result = getPromScoreMonthlyReportService.getPromScoreMonthlyReport(startDate, endDate);

        assertThat(result).isNull();
    }
}
