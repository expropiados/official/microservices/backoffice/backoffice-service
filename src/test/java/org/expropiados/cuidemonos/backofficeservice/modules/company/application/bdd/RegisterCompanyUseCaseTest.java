package org.expropiados.cuidemonos.backofficeservice.modules.company.application.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;

@Tag("SystemTest")
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features/register-a-company.feature"})
class RegisterCompanyUseCaseTest {

}
