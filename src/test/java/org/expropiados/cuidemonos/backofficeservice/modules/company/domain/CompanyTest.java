package org.expropiados.cuidemonos.backofficeservice.modules.company.domain;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("UnitTest")
class CompanyTest {

    @Test
    void getFullNameSuccess() {
        Company company = new Company(1L, "PUCP", "1234567890",
                "Av. Universtaria 123", "993823924", "test@gmail.com",null,
                UUID.randomUUID());
        assertThat(company.getFullName()).isEqualTo("El nombre completo es PUCP");
    }
}
