package org.expropiados.cuidemonos.backofficeservice.modules.company.application.bdd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.http.*;


import static org.assertj.core.api.Assertions.assertThat;

public class RegisterCompanyStepDef extends AbstractSpringConfigurationTest{

    private ResponseEntity<?> response;

    private ObjectMapper mapper = new ObjectMapper();

    private ClientCompanyToRegister clientCompanyToRegister;

    @Given("a company to be registered")
    public void a_company_to_be_registered() {
        clientCompanyToRegister = new ClientCompanyToRegister("PUCP",
                "1234567890", "test@gmail.com", "Av. Universitaria 123",  "993823924");
    }

    @When("the User registers the company")
    public void the_user_registers_the_company(){
        String url = getContextURL()+"/companies";
        String params = "{ \"name\": \"PUCP\", \"ruc\": \"1234567890\", " +
                "\"address\": \"Av. Universitaria 123\", \"phone\": \"993823924\"}";
        HttpEntity<String> requestEntity = new HttpEntity<>(params, getDefaultHttpHeaders());
        response = invokeRESTCall(url, HttpMethod.POST, requestEntity, Company.class);
    }

    @Then("the company is registered")
    public void the_company_is_registered() {
        var responseBody = (Company) response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseBody.getRuc()).isEqualTo(clientCompanyToRegister.getRuc());
        assertThat(responseBody.getPhone()).isEqualTo(clientCompanyToRegister.getPhone());
        assertThat(responseBody.getName()).isEqualTo(clientCompanyToRegister.getName());
        assertThat(responseBody.getAddress()).isEqualTo(clientCompanyToRegister.getAddress());

        assertThat(responseBody.getId()).isNotNull();
        assertThat(responseBody.getUuid()).isNotNull();
    }
}
