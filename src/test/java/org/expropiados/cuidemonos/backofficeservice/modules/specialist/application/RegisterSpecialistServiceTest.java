package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.SaveSpecialistPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
@Disabled("Disable for warnings, fix in next iteration")
class RegisterSpecialistServiceTest {

    private final SaveSpecialistPort saveSpecialistPort = Mockito.mock(SaveSpecialistPort.class);
    private final RegisterUserPort registerUserPort = Mockito.mock(RegisterUserPort.class);
    private final SaveImageProfilePort saveImageProfilePort = Mockito.mock(SaveImageProfilePort.class);
    private final GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
    private final ValidateSpecialistByDocumentService validateSpecialistByDNIService = Mockito.mock(ValidateSpecialistByDocumentService.class);

    private final UserToRegister userToRegister = new UserToRegister("77589563", TypeDocument.DNI, "Pepe Lucho", "Fernandez", "Castro",  LocalDate.parse("2020-02-28"), UUID.randomUUID());

    private final User user = User.builder().id(1L).uuid(UUID.randomUUID())
            .document("77589563").name("Pepe Lucho").fatherLastname("Fernandez").motherLastname("Castro")
            .uuid(UUID.randomUUID())
            .birthdate(LocalDate.parse("2020-02-28"))
            .build();

    private final SpecialistChecked specialistChecked = new SpecialistChecked(false, null, user);

    @Test
    void registerSpecialistSuccess() {

        var specialistToRegister = new SpecialistToRegister();
        specialistToRegister.setUser(userToRegister);
        specialistToRegister.setSpecialty("medicine");
        var specialist = Specialist.builder().id(1L).user(user).build();
        when(validateSpecialistByDNIService.validateSpecialistByDocument(specialistToRegister.getUser().getDocument())).thenReturn(specialistChecked);
        when(getUserPort.getUserByDocument(specialistToRegister.getUser().getDocument())).thenReturn(user);

        when(registerUserPort.registerUser(eq(userToRegister), any(UUID.class))).thenReturn(user);
        //if (!specialistChecked.getIsSpecialist()) {
        when(saveSpecialistPort.saveSpecialist(eq(specialistToRegister), eq(user))).thenReturn(specialist);
        //    try {
        //        var result = registerSpecialistService.registerSpecialist(specialistToRegister);
        //
        //        assertThat(result).isNotNull();
        //        assertThat(result.getId()).isEqualTo(specialist.getId());
        //        assertThat(result.getUser()).isEqualTo(specialist.getUser());
        /*    }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            throw new RegisterUserException(user.getDni(),"specialist");
        }*/
    }

}
