package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMedicalReasonMonthlyReportPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetMedicalReasonMonthlyReportServiceTest {

    private final GetMedicalReasonMonthlyReportPort getMonthlyPort = Mockito.mock(GetMedicalReasonMonthlyReportPort.class);
    private final GetMedicalReasonMonthlyReportService getMonthlyService = new GetMedicalReasonMonthlyReportService(getMonthlyPort);

    @Test
    void getMedicalReasonMonthlyReportFailure() throws IOException {
        var startDate = LocalDate.now();
        var endDate = LocalDate.now().plusMonths(2);

        when(getMonthlyPort.getMedicalReasonMonthlyReport(startDate, endDate)).thenReturn(null);
        var result = getMonthlyService.getMedicalReasonMonthly(startDate, endDate);

        assertThat(result).isNull();
    }
}
