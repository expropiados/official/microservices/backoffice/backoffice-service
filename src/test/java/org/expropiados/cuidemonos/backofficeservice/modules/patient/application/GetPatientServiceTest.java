package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetPatientServiceTest {
    private final GetPatientPort getPatientPort = Mockito.mock(GetPatientPort.class);
    private final GetPatientService getPatientService = new GetPatientService(getPatientPort);

    @Test
    void getPatientSuccess(){
        var filters = Filters.builder()
                .page(0).pageSize(10).build();
        when(getPatientPort.getPatients()).thenReturn(
                new ArrayList<>());
        var result = getPatientService.getPatients();
        assertThat(result).isNotNull();
//        assertThat(result.getData()).isEmpty();
    }
}
