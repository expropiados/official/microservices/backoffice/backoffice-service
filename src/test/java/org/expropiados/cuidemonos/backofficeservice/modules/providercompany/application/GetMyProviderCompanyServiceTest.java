package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import org.expropiados.cuidemonos.backofficeservice.config.backoffice.ProviderCompanyConfig;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.GetProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetMyProviderCompanyServiceTest {
    private final ProviderCompanyConfig providerCompanyConfig = Mockito.mock(ProviderCompanyConfig.class);
    private final GetProviderCompanyPort getProviderCompanyPort = Mockito.mock(GetProviderCompanyPort.class);
    private final GetMyProviderCompanyService getMyProviderCompanyService = new GetMyProviderCompanyService(providerCompanyConfig, getProviderCompanyPort);

    @Test
    void getMyProviderCompanySuccess(){
        var provider = ProviderCompany.builder().id(1L).build();
        var uuid = UUID.randomUUID();
        when(providerCompanyConfig.getUuid()).thenReturn(uuid);
        when(getProviderCompanyPort.getProviderCompany(uuid)).thenReturn(Optional.of(provider));
        var result = getMyProviderCompanyService.getMyProviderCompany();

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
    }

    @Test
    void getMyProviderCompanyFailure(){
        var uuid = UUID.randomUUID();
        when(providerCompanyConfig.getUuid()).thenReturn(uuid);
        when(getProviderCompanyPort.getProviderCompany(uuid)).thenReturn(Optional.empty());
        var result = getMyProviderCompanyService.getMyProviderCompany();

        assertThat(result).isNull();
    }
}
