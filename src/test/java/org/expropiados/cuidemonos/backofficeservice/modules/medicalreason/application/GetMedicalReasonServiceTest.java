package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.GetMedicalReasonService;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetMedicalReasonServiceTest {
    private final GetMedicalReasonPort getMedicalReasonPort = Mockito.mock(GetMedicalReasonPort.class);
    private final GetMedicalReasonService getMedicalReasonService = new GetMedicalReasonService(getMedicalReasonPort);


    @Test
    void getMedicalReasonSuccess(){
        var medicalReason = new MedicalReason();
        medicalReason.setId(1L);
        medicalReason.setName("Name");
        medicalReason.setDescription("Descripcion");
        when(getMedicalReasonPort.getMedicalReason(any(Long.class))).thenReturn(Optional.of(medicalReason));

        var result = getMedicalReasonService.getMedicalReason(1L);

        assertThat(result.getId()).isEqualTo(1L);
    }

    @Test
    void getMedicalReasonException(){
        when(getMedicalReasonPort.getMedicalReason(any(Long.class))).thenReturn(Optional.empty());
        MedicalReason result;
        try{
            result = getMedicalReasonService.getMedicalReason(1L);
        }catch (Exception ex){

        }
    }
}
