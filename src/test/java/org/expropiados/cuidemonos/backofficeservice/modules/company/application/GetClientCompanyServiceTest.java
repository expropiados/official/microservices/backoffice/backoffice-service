package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetClientCompanyServiceTest {
    private final GetClientCompanyPort getClientCompanyPort = Mockito.mock(GetClientCompanyPort.class);
    private final GetClientCompaniesService getClientCompanyService = new GetClientCompaniesService(getClientCompanyPort);

    @Test
    void getClientCompanySuccess(){
        when(getClientCompanyPort.getClientCompanies()).thenReturn(new ArrayList<>());
        var result = getClientCompanyService.getClientCompanies();
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

}
