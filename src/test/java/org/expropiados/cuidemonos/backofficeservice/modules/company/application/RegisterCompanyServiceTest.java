package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.RegisterCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.SaveClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class RegisterCompanyServiceTest {
    private final RegisterCompanyPort registerCompanyPort = Mockito.mock(RegisterCompanyPort.class);
    private final SaveClientCompanyPort saveClientCompanyPort = Mockito.mock(SaveClientCompanyPort.class);
    private final RegisterCompanyService registerCompanyService = new RegisterCompanyService(registerCompanyPort, saveClientCompanyPort);
    private static final String IMAGE_URL = "https://cdn.discordapp.com/attachments/586743196944564226/847681053815603210/image0.jpg";

    @Test
    void registerCompanyWithoutImageSuccess(){
        ClientCompanyToRegister clientCompanyToRegister =
                new ClientCompanyToRegister("PUCP","0123456789",
                        "test@gmail.com", "Av. Universtaria 123", "993823924");
        UUID uuid = UUID.randomUUID();
        Company company = new Company(1L, "PUCP", "0123456789",
                "Av. Universtaria 123", "993823924", "test@gmail.com", null, uuid);
        when(registerCompanyPort.registerCompany(eq(clientCompanyToRegister), any(UUID.class))).thenReturn(company);
        var result = registerCompanyService.registerCompany(clientCompanyToRegister);
        assertThat(result.getName()).isEqualTo(clientCompanyToRegister.getName());
        assertThat(result.getRuc()).isEqualTo(clientCompanyToRegister.getRuc());
        assertThat(result.getAddress()).isEqualTo(clientCompanyToRegister.getAddress());
        assertThat(result.getPhone()).isEqualTo(clientCompanyToRegister.getPhone());
        assertThat(result.getUuid()).isEqualTo(uuid);
    }

    @Test
    void registerCompanyWithImageSuccess(){
        ClientCompanyToRegister clientCompanyToRegister =
                new ClientCompanyToRegister("PUCP","0123456789","test@gmail.com",
                        "Av. Universtaria 123", "993823924");
        var multipartFile = Mockito.mock(MultipartFile.class);
        clientCompanyToRegister.setMultipartFile(multipartFile);
        UUID uuid = UUID.randomUUID();
        Company company = new Company(1L, "PUCP", "0123456789",
                "Av. Universtaria 123",
                "993823924", "test@gmail.com", IMAGE_URL, uuid);

        when(saveClientCompanyPort.saveClientCompanyImageFile(any(MultipartFile.class))).thenReturn(IMAGE_URL);
        when(registerCompanyPort.registerCompany(eq(clientCompanyToRegister), any(UUID.class))).thenReturn(company);
        var result = registerCompanyService.registerCompany(clientCompanyToRegister);
        assertThat(result.getName()).isEqualTo(clientCompanyToRegister.getName());
        assertThat(result.getRuc()).isEqualTo(clientCompanyToRegister.getRuc());
        assertThat(result.getAddress()).isEqualTo(clientCompanyToRegister.getAddress());
        assertThat(result.getPhone()).isEqualTo(clientCompanyToRegister.getPhone());
        assertThat(result.getUuid()).isEqualTo(uuid);
        assertThat(result.getImageSourceFile()).isEqualTo(IMAGE_URL);
    }
}
