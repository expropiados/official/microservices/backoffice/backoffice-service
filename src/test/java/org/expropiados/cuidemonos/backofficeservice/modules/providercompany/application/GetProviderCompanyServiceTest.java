package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.GetProviderCompanyService;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.GetProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetProviderCompanyServiceTest {
    private final GetProviderCompanyPort getProviderCompanyPort = Mockito.mock(GetProviderCompanyPort.class);
    private final GetProviderCompanyService getProviderCompanyService = new GetProviderCompanyService(getProviderCompanyPort);

    @Test
    void getProviderCompanySuccess(){
        var uuid = UUID.randomUUID();
        var provider = ProviderCompany.builder().id(1L).build();
        when(getProviderCompanyPort.getProviderCompany(uuid)).thenReturn(Optional.of(provider));

        var result = getProviderCompanyService.getProviderCompany(uuid);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isNotNull();
    }

    @Test
    void getProviderCompanyFailure(){
        var uuid = UUID.randomUUID();
        when(getProviderCompanyPort.getProviderCompany(uuid)).thenReturn(Optional.empty());

        var result = getProviderCompanyService.getProviderCompany(uuid);

        assertThat(result.isEmpty()).isTrue();
    }
}
