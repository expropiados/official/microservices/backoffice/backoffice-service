package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({PatientPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {PatientMapper.class, UserMapper.class})
@ContextConfiguration
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@Sql({"classpath:sql/GetUserTest.sql", "classpath:sql/GetPatientTest.sql"})
@Disabled("disabled because of sql race condition error")
class ValidatePatientByDNIPersistenceTest {

    @Autowired
    private PatientPersistenceAdapter patientPersistenceAdapter;

    @Test
    void validatePatientByDNISuccess(){
        var patient = patientPersistenceAdapter.getByDNI("12345678");

        assertThat(patient).isPresent();
        var patientExists = patient.get();
        assertThat(patientExists.getUser().getDocument()).isEqualTo("12345678");
        assertThat(patientExists.getUser().getId()).isEqualTo(10L);
    }
}
