package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import com.amazonaws.util.IOUtils;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.RegisterCompanyUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = RegisterCompanyController.class)
@Disabled("Disabled because of authorization headers missed. Mock jwt helper for controller first with right credentials.")
class RegisterCompanyControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RegisterCompanyUseCase registerCompanyUseCase;

    private static final ClientCompanyToRegister CLIENT_COMPANY_TO_REGISTER = new ClientCompanyToRegister("PUCP","1234567890",
            "test@gmail.com", "Av. Universitaria 123", "993823924");

    private static MultipartFile multipartFile = null;

    @BeforeEach
    void init() throws Exception{
        File file = new File("src/test/resources/files/ArbolDeProblemas.jpg");
        FileInputStream input = new FileInputStream(file);
        multipartFile = new MockMultipartFile("test1",
                file.getName(), MediaType.IMAGE_JPEG_VALUE, IOUtils.toByteArray(input));
    }

    @Test
    void testRegisterCompanySuccess() throws Exception{
        String params = "{ \"name\": \"PUCP\", \"ruc\": \"12345678901\", " +
                "\"address\": \"Av. Universitaria 123\", \"phone\": \"993823924\"}";
        mockMvc.perform(post("/companies").
                content(params).
                header("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)).andExpect(status().isOk());
    }
}
