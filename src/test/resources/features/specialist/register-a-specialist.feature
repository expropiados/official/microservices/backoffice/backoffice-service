Feature: Register a specialist
  Scenario: User register a specialist successfully
    Given the User is the entity
    When the User registers one specialist
    Then the specialist is registered

  Scenario: User is not entity and tries to register a specialist
    Given the User is not the entity
    When the User tries to register a specialist
    Then an Exception occurs

  Scenario: User tries to register a specialist with corrupted image
    Given the User is the entity
    When the User registers one specialist with corrupted image
    Then an Exception occurs

  Scenario: User tries to register a specialist with invalid DNI
    Given the User is the entity
    When the User registers one specialist with with invalid DNI
    Then an Exception occurs

  Scenario: User tries to register a specialist with invalid email
    Given the User is the entity
    When the User registers one specialist with with invalid email
    Then an Exception occurs