package org.expropiados.cuidemonos.backofficeservice.helpers.jwt;

import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.AuthenticationException;

@Getter
public class InvalidRoleException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_002";
    private final String message;
    private final transient Object data;

    public InvalidRoleException(Integer role) {
        super();
        this.message = String.format("Invalid role '%s'. Need to send a right role.", role);
        this.data = null;
    }
}
