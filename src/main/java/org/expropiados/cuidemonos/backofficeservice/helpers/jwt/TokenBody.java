package org.expropiados.cuidemonos.backofficeservice.helpers.jwt;

import lombok.Builder;
import lombok.Value;

import java.util.Date;
import java.util.UUID;

@Value
@Builder
public class TokenBody {
    UUID userUuid;
    UUID patientUuid;
    UUID specialistUuid;
    UUID clientSupervisorUuid;

    Long userId;
    Long patientId;
    Long specialistId;
    Long clientSupervisorId;
    Long providerSupervisorId;

    Long patientCompanyId;
    String patientCompanyName;

    Long clientSupervisorCompanyId;
    String clientSupervisorCompanyName;

    String providerSupervisorCompanyName;

    String userEmail;
    String userImageUrl;

    String username;
    Date loggedDateTime;
    Date expirationDateTime;
}
