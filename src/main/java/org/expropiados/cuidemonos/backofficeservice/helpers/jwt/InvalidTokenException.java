package org.expropiados.cuidemonos.backofficeservice.helpers.jwt;

import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.AuthenticationException;

@Getter
public class InvalidTokenException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_001";
    private final String message;
    private final transient Object data;

    public InvalidTokenException(String token) {
        super();
        this.message = String.format("Invalid token '%s'. Please login again.", token);
        this.data = null;
    }
}
