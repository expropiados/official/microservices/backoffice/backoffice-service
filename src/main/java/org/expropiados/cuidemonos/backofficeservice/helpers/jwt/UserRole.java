package org.expropiados.cuidemonos.backofficeservice.helpers.jwt;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum UserRole {
    PATIENT(0),
    SPECIALIST(1),
    CLIENT_SUPERVISOR(2),
    PROVIDER_SUPERVISOR(3),
    ADMIN(4);

    private final Integer value;

    public static UserRole valueOf(Integer value) {
        switch (value) {
            case 0: return UserRole.PATIENT;
            case 1: return UserRole.SPECIALIST;
            case 2: return UserRole.CLIENT_SUPERVISOR;
            case 3: return UserRole.PROVIDER_SUPERVISOR;
            case 4: return UserRole.ADMIN;
            default: return null;
        }
    }

    public boolean isIn(UserRole ...roles) {
        return Arrays.stream(roles)
                .anyMatch(role -> this == role);
    }
}
