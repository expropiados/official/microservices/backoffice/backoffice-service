package org.expropiados.cuidemonos.backofficeservice.helpers.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.security.JwtConfig;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.Helper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;
import java.util.UUID;

@Helper
@RequiredArgsConstructor
public class JwtHelper {

    private final JwtConfig jwtConfig;
    private final Logger logger = LoggerFactory.getLogger(JwtHelper.class);

    private Claims decode(String jwt) {
        var key = getKey();

        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }

    private Key getKey() {
        return Keys.hmacShaKeyFor(jwtConfig.getSecret().getBytes());
    }

    public TokenBody decodeToken(String token) {
        try {
            var jwt = token.startsWith("Bearer ") ? token.split("Bearer ")[1] : token;
            var claims = decode(jwt);

            var userUuid = claims.get("us_uid", String.class);
            var patientUuid = claims.get("pa_uid", String.class);
            var specialistUuid = claims.get("sp_uid", String.class);
            var clientSupervisorUuid = claims.get("cs_uid", String.class);

            return TokenBody.builder()
                    .userId(claims.get("us_id", Long.class))
                    .patientId(claims.get("pa_id", Long.class))
                    .specialistId(claims.get("sp_id", Long.class))
                    .clientSupervisorId(claims.get("cs_id", Long.class))
                    .providerSupervisorId(claims.get("ps_id", Long.class))

                    .patientCompanyId(claims.get("pa_com_id", Long.class))
                    .patientCompanyName(claims.get("pa_com", String.class))

                    .clientSupervisorCompanyId(claims.get("cs_com_id", Long.class))
                    .clientSupervisorCompanyName(claims.get("cs_com", String.class))

                    .providerSupervisorCompanyName(claims.get("cs_com", String.class))

                    .userUuid(userUuid == null ? null : UUID.fromString(userUuid))
                    .patientUuid(patientUuid == null ? null : UUID.fromString(patientUuid))
                    .specialistUuid(specialistUuid == null ? null : UUID.fromString(specialistUuid))
                    .clientSupervisorUuid(clientSupervisorUuid == null ? null : UUID.fromString(clientSupervisorUuid))

                    .userEmail(claims.get("email", String.class))
                    .userImageUrl(claims.get("img", String.class))

                    .username(claims.getSubject())
                    .loggedDateTime(claims.getIssuedAt())
                    .expirationDateTime(claims.getExpiration())
                    .build();

        } catch (Exception exception) {
            logger.error(exception.getMessage());
            throw new InvalidTokenException(token);
        }
    }

    public UserRole getRole(TokenBody tokenBody, Integer role) {
        var userRole = UserRole.valueOf(role);
        if (userRole == UserRole.PATIENT && tokenBody.getPatientId() != null) {
            return userRole;
        }

        if (userRole == UserRole.SPECIALIST && tokenBody.getSpecialistId() != null) {
            return userRole;
        }

        if (userRole == UserRole.CLIENT_SUPERVISOR && tokenBody.getClientSupervisorId() != null) {
            return userRole;
        }

        if (userRole == UserRole.PROVIDER_SUPERVISOR && tokenBody.getProviderSupervisorId() != null) {
            return userRole;
        }

        throw new InvalidRoleException(role);
    }
}
