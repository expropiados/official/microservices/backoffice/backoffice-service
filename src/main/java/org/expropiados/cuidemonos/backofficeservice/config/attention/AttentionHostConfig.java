package org.expropiados.cuidemonos.backofficeservice.config.attention;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties("attention.context")
public class AttentionHostConfig {
    private String host;
}
