package org.expropiados.cuidemonos.backofficeservice.config.backoffice.storage;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties("backoffice.storage.paths")
public class StoragePathsConfig {
    private String bulk;
}
