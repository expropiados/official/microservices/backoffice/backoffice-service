package org.expropiados.cuidemonos.backofficeservice.config.security;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "security")
public class SecurityHostConfig {
    private String host;
}
