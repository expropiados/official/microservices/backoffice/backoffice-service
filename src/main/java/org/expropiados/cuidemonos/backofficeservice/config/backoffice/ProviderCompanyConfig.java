package org.expropiados.cuidemonos.backofficeservice.config.backoffice;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "backoffice.provider-company")
public class ProviderCompanyConfig {
    UUID uuid;
}
