package org.expropiados.cuidemonos.backofficeservice.hexagonal.converter;


import org.expropiados.cuidemonos.backofficeservice.hexagonal.StorageAdapter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class used to convert a MultipartFile to a File stored in the current directory web
 * the application is running
 *
 * This method is no longer acceptable because files are stored in a directory where
 * could be protected or current app user (OS user) is not allow to create files in current
 * directory.
 * <p> Use {@link org.expropiados.cuidemonos.backofficeservice.helpers.files.TmpFileHelper} instead.
 */
@StorageAdapter
public class MultipartFileConverter {
    public File convertMultipartFileToFile(MultipartFile multipartFile) throws IOException {
        File file = new File(multipartFile.getOriginalFilename());
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(multipartFile.getBytes());
        fileOutputStream.close();
        return file;
    }
}
