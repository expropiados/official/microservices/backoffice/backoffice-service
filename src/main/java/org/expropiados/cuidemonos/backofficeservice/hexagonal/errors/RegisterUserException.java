package org.expropiados.cuidemonos.backofficeservice.hexagonal.errors;

import lombok.Getter;
import lombok.Value;

@Getter
public class RegisterUserException extends RuntimeException implements NotFoundException{

    private final String code = "USR_002";
    private final String message;
    private final transient Object data;

    @Value
    static class Data {
        String dni;
    }

    public RegisterUserException(String dni, String rol) {
        super();
        this.message = String.format("%s %s registered", rol, dni);
        this.data = new Data(dni);
    }

}
