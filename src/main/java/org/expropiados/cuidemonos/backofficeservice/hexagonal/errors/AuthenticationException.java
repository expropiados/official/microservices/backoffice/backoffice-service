package org.expropiados.cuidemonos.backofficeservice.hexagonal.errors;

/**
 * Authentication exception should be used for errors with tokens and login
 **/
public interface AuthenticationException extends BusinessException {
}
