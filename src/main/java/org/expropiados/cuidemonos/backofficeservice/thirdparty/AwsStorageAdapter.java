package org.expropiados.cuidemonos.backofficeservice.thirdparty;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.converter.MultipartFileConverter;
import org.expropiados.cuidemonos.backofficeservice.config.backoffice.storage.AwsStorageConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * Class used to store a MultipartFile into a S3 Bucket for AWS
 * This method is no longer acceptable to manage MultipartFiles because of Spring Boot coupling.
 * <p> Use {@link AwsStorageExternalService} instead.
 */
@Data
@org.expropiados.cuidemonos.backofficeservice.hexagonal.StorageAdapter
@RequiredArgsConstructor
public class AwsStorageAdapter {
    private final AwsStorageConfig awsStorageConfig;
    private final MultipartFileConverter multipartFileConverter;
    private final Logger logger = LoggerFactory.getLogger(AwsStorageAdapter.class);

    public AmazonS3 getS3Client(){
        var awsCredentials = new BasicAWSCredentials(
                awsStorageConfig.getCredentials().getAccessKey(),
                awsStorageConfig.getCredentials().getSecretKey());

        return AmazonS3ClientBuilder.standard()
                .withRegion(awsStorageConfig.getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public String saveFile(MultipartFile multipartFile) {
        try {
            var file = multipartFileConverter.convertMultipartFileToFile(multipartFile);

            var request = new PutObjectRequest(awsStorageConfig.getBucket().getName(), file.getName(), file)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            this.getS3Client().putObject(request);

            return this.getS3Client()
                    .getUrl(awsStorageConfig.getBucket().getName(), file.getName())
                    .toString();

        } catch (Exception e) {
            logger.error("An error occurred while saving the data file to the AWS S3 service", e);
            return null;
        }
    }
}
