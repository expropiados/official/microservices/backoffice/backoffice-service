package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface PatientMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "profileImage", target = "profileImage")
    @Mapping(source = "isBlocked", target = "isBlocked")
    @Mapping(source = "clientCompanyId", target = "clientCompanyId")
    @Mapping(source = "uuid", target = "uuid")
    Patient toPatient(PatientJpaEntity specialistJpaEntity);
    List<Patient> toPatients(List<PatientJpaEntity> patientJpaEntities);

    @InheritInverseConfiguration
    @Mapping(target = "clientCompany", ignore = true)
    PatientJpaEntity toPatientJpaEntity(Patient patient);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "user",target = "user")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "address", target = "address")
    @Mapping(target = "clientCompanyId", ignore = true)
    @Mapping(source = "profileImage", target = "profileImage")
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "clientCompany", ignore = true)
    PatientJpaEntity toPatientJpaEntity(PatientToRegister patientToRegister);

    @Mapping(source = "patientId", target = "id")
    @Mapping(source = "user",target = "user")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "address", target = "address")
    @Mapping(target = "clientCompanyId", ignore = true)
    @Mapping(source = "profileImage", target = "profileImage")
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "clientCompany", ignore = true)
    PatientJpaEntity toPatientJpaEntity(PatientToEdit patientToRegister);
}

