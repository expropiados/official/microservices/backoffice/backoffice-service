package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreMonthlyReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetPromScoreMonthlyReportPort {
    GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException;
}
