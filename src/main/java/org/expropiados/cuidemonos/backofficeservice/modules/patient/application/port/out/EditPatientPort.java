package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

public interface EditPatientPort {
    Patient editPatient(PatientToEdit patientToEdit, Patient patient);
}
