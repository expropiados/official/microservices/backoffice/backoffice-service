package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyReport;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMedicalReasonMonthlyReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMedicalReasonMonthlyReportPort;

import java.io.IOException;
import java.time.LocalDate;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyReportService implements GetMedicalReasonMonthlyReportUseCase {

    private final GetMedicalReasonMonthlyReportPort getMedicalReasonMonthlyReportPort;

    @Override
    public GetMedicalReasonMonthlyReport getMedicalReasonMonthly(LocalDate initDate, LocalDate endDate) throws IOException {
        return getMedicalReasonMonthlyReportPort.getMedicalReasonMonthlyReport(initDate, endDate);
    }
}
