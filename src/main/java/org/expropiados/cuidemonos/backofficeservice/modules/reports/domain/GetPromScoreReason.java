package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetPromScoreReason {
    private double[] data;
}
