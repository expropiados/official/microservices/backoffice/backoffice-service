package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.ClientSupervisorChecked;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ValidateClientSupervisorByDocumentUseCase;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ValidateClientSupervisorByDocumentController {

    private final JwtHelper jwtHelper;
    private final ValidateClientSupervisorByDocumentUseCase validateClientSupervisorByDocumentUseCase;

    @GetMapping("/companies/supervisor/checking")
    public ClientSupervisorChecked validateSpecialistByDocument(@RequestParam(name = "document", required = false)String document,
                                                                @RequestHeader("Authorization") String token,
                                                                @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return validateClientSupervisorByDocumentUseCase.validateSpecialistByDocument(document);
    }
}
