package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import java.io.File;

public interface StoreBulkLoadFilePort {
    String storeBulkLoadFile(File file);
}
