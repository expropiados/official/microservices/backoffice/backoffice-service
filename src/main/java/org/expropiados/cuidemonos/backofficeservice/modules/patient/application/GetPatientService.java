package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPatientService implements GetPatientUseCase {

    public final GetPatientPort getPatientPort;

    @Override
    public List<Patient> getPatients() {
        return getPatientPort.getPatients();
    }
}
