package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetPaginatedClientCompaniesUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetPaginatedClientCompaniesPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

@UseCase
@RequiredArgsConstructor
public class GetPaginatedClientCompaniesService implements GetPaginatedClientCompaniesUseCase {
    private final GetPaginatedClientCompaniesPort getPaginatedClientCompaniesPort;

    @Override
    public Paginator<Company> getPaginatedClientCompanies(Filters filters) {
        return getPaginatedClientCompaniesPort.getPaginatedClientCompanies(filters);
    }
}
