package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyFreqReport;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMedicalReasonMonthlyFreqReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMedicalReasonMonthlyFreqReportPort;

import java.io.IOException;
import java.time.LocalDate;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyFreqReportService implements GetMedicalReasonMonthlyFreqReportUseCase {

    private final GetMedicalReasonMonthlyFreqReportPort getMedicalReasonMonthlyFreqReportPort;

    @Override
    public GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(LocalDate initDate, LocalDate endDate, Long clientCompanyId) throws IOException {
        return getMedicalReasonMonthlyFreqReportPort.getMedicalReasonMonthlyFreqReport(initDate, endDate, clientCompanyId);
    }
}
