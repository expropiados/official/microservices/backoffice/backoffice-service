package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetClientBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetProviderBulkLoadUseCase;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetBulkLoadController {

    private final GetProviderBulkLoadUseCase providerBulkLoadUseCase;
    private final GetClientBulkLoadUseCase clientBulkLoadUseCase;
    private final JwtHelper jwtHelper;

    @GetMapping("/bulk-loads/{bulkId}")
    public BulkLoadWithResponsible getBulkLoad(@PathVariable Long bulkId,
                                               @RequestHeader("Authorization") String token,
                                               @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole == UserRole.CLIENT_SUPERVISOR) {
            var companyId = tokenBody.getClientSupervisorCompanyId();
            return clientBulkLoadUseCase.getClientBulkLoad(bulkId, companyId);
        }

        if (userRole == UserRole.PROVIDER_SUPERVISOR) {
            return providerBulkLoadUseCase.getProviderBulkLoad(bulkId);
        }

        throw new NotAuthorizedForRoleException(userRole);
    }
}
