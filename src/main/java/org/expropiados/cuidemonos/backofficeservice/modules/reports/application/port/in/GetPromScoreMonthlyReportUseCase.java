package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreMonthlyReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetPromScoreMonthlyReportUseCase {
    GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException;
}
