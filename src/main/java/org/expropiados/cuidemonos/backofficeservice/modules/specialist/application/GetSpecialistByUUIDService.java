package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetSpecialistByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetSpecialistByUUIDService implements GetSpecialistByUUIDUseCase {

    private final GetSpecialistsPort getSpecialistPort;

    @Override
    public Specialist getSpecialistByUUID(UUID specialistUUID) {
        var specialist = getSpecialistPort.getSpecialistByUUID(specialistUUID);
        if (specialist.isEmpty()) return null;
        return specialist.get();
    }
}
