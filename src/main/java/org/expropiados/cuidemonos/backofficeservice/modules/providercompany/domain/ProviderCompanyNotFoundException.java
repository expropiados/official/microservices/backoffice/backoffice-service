package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

import java.util.UUID;

@Getter
public class ProviderCompanyNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_PRC_001";
    private final String message;
    private final transient Object data;


    @Value
    static class Data {
        UUID companyUUID;
    }

    public ProviderCompanyNotFoundException(UUID companyUUID) {
        super();
        this.message = String.format("Provider Company %c not found", companyUUID);
        this.data = new Data(companyUUID);
    }
}
