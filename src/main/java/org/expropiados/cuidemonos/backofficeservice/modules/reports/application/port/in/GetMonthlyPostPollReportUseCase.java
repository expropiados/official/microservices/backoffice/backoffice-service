package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMonthlyPostPollReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

public interface GetMonthlyPostPollReportUseCase {
    GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) throws IOException;
}
