package org.expropiados.cuidemonos.backofficeservice.modules.user.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TypeDocument {

    DNI(0),
    PASAPORTE(1),
    CARNET(2);

    @Getter
    private final Integer value;
}
