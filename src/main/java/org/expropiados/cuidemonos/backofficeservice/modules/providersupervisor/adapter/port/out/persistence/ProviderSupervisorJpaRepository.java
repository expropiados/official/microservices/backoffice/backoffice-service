package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProviderSupervisorJpaRepository extends JpaRepository<ProviderSupervisorJpaEntity, Long> {
    @Query("SELECT PS FROM ProviderSupervisorJpaEntity PS WHERE UPPER(PS.user.document) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(PS.user.name) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(PS.user.fatherLastname) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(PS.user.motherLastname) LIKE CONCAT('%', UPPER(:search), '%') ORDER BY PS.user.id")
    Page<ProviderSupervisorJpaEntity> searchProviderSupervisors(Pageable pageable, @Param("search") String search);
}
