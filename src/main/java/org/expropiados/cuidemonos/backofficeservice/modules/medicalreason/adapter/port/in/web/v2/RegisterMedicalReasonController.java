package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.RegisterMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class RegisterMedicalReasonController {

    private final JwtHelper jwtHelper;
    private final RegisterMedicalReasonUseCase registerMedicalReasonUseCase;

    @PostMapping(value = "/medical-reasons")
    public MedicalReason getPaginatedProviderSupervisors(@RequestBody MedicalReasonToRegister medicalReasonToRegister,
                                                         @RequestHeader("Authorization") String token,
                                                         @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (!userRole.isIn(UserRole.ADMIN, UserRole.PROVIDER_SUPERVISOR)) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        return registerMedicalReasonUseCase.registerMedicalReason(medicalReasonToRegister);
    }
}
