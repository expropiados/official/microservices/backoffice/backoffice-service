package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParameters;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompanyParametersUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyParametersPort;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class GetClientCompanyParametersService implements GetClientCompanyParametersUseCase {

    public final GetClientCompanyParametersPort getClientCompanyParametersPort;

    @Override
    public Optional<ClientCompanyParameters> getParametersFromClientCompany(Long clientCompanyId) {
        return getClientCompanyParametersPort.getParametersFromClientCompany(clientCompanyId);
    }
}
