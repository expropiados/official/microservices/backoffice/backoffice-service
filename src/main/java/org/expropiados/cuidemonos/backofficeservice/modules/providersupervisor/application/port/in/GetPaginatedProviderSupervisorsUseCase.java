package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain.ProviderSupervisor;

public interface GetPaginatedProviderSupervisorsUseCase {
    Paginator<ProviderSupervisor> getPaginatedProviderSupervisors(Filters filters);
}
