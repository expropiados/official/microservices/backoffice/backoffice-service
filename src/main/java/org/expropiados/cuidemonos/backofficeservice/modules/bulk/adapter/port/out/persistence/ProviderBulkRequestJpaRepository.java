package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProviderBulkRequestJpaRepository extends JpaRepository<ProviderBulkRequestJpaEntity, Long> {

    @Query("SELECT PB FROM ProviderBulkRequestJpaEntity PB ORDER BY PB.bulkLoad.requestedAt DESC")
    Page<ProviderBulkRequestJpaEntity> findAllOrderByBulkLoadRequestedAt(Pageable pageable);

    Optional<ProviderBulkRequestJpaEntity> findByBulkLoadId(Long bulkId);
}
