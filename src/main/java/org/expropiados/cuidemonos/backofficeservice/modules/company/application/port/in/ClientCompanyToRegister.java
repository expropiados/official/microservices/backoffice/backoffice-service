package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
public class ClientCompanyToRegister {
    @NotNull
    String name;

    @NotNull
    String ruc;

    String address;

    String email;

    String phone;

    MultipartFile multipartFile;

    String imageSourceFile;

    public ClientCompanyToRegister(){

    }

    public ClientCompanyToRegister(String name, String ruc, String email, String address, String phone){
        this.name = name;
        this.ruc = ruc;
        this.address = address;
        this.phone = phone;
        this.imageSourceFile = null;
        this.multipartFile = null;
    }
}
