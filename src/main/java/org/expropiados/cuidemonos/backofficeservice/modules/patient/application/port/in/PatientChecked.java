package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

@Getter
@Builder
@AllArgsConstructor
public class PatientChecked {
    private final Boolean isPatient;
    private final UserApi userApi;
    private final User user;
}
