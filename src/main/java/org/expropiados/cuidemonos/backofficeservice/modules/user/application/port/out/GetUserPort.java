package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

public interface GetUserPort {

    User getUserByDocument(String document);
}
