package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.GetPaginatedMedicalReasonsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class GetPaginatedMedicalReasonsController {

    private final JwtHelper jwtHelper;
    private final GetPaginatedMedicalReasonsUseCase useCase;

    @GetMapping(value = "/medical-reasons")
    public Paginator<MedicalReason> getPaginatedMedicalReasons(Pageable pageable,
                                                            @RequestParam(value = "search", defaultValue = "") String search,
                                                            @RequestHeader("Authorization") String token,
                                                            @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        return useCase.getPaginatedMedicalReasons(filters);
    }
}
