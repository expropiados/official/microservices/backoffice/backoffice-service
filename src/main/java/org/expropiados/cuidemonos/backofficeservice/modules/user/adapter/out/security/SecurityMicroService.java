package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.security;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SecurityMicroService {

    @POST("/register")
    Call<Void> registerUserRole(@Body UserRoleRequestBody userRoleRequestBody);
}
