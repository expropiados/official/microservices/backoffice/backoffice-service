package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.in.GetPaginatedProviderSupervisorsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain.ProviderSupervisor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class GetPaginatedProviderSupervisorsController {

    private final JwtHelper jwtHelper;
    private final GetPaginatedProviderSupervisorsUseCase getPaginatedProviderSupervisorsUseCase;

    @GetMapping(value = "/provider-companies/supervisors")
    public Paginator<ProviderSupervisor> getPaginatedProviderSupervisors(Pageable pageable,
                                                                        @RequestParam(value = "search", defaultValue = "") String search,
                                                                        @RequestHeader("Authorization") String token,
                                                                        @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (!userRole.isIn(UserRole.ADMIN, UserRole.PROVIDER_SUPERVISOR)) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        return getPaginatedProviderSupervisorsUseCase.getPaginatedProviderSupervisors(filters);
    }
}
