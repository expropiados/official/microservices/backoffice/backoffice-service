package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

public interface UpdateMedicalReasonPort {
    MedicalReason updateMedicalReason(MedicalReason medicalReason);
}
