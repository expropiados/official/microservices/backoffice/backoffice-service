package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;

@Value
public class BulkLoadWithResponsible {
    BulkLoadResponsible responsible;
    BulkLoad bulkLoad;
}
