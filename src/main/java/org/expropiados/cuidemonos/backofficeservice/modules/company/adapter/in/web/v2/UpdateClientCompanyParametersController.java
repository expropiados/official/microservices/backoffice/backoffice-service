package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParametersDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.UpdateClientCompanyParametersUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.ClientCompanyNotFoundException;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@RequestMapping("v2")
@CrossOrigin
public class UpdateClientCompanyParametersController {
    private final JwtHelper jwtHelper;
    private final UpdateClientCompanyParametersUseCase updateClientCompanyParametersUseCase;

    @PostMapping("/companies/{clientCompanyId}/parameters")
    public Company updateClientCompanyParameters(@RequestHeader("Authorization") String token,
                                                 @RequestHeader("role") Integer role,
                                                 @PathVariable("clientCompanyId") Long clientCompanyId,
                                                 @RequestBody ClientCompanyParametersDTO clientCompanyParametersDTO) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole != UserRole.PROVIDER_SUPERVISOR) throw new NotAuthorizedForRoleException(userRole);

        return updateClientCompanyParametersUseCase.updateClientCompanyParameters(clientCompanyId, clientCompanyParametersDTO)
                .orElseThrow(()->new ClientCompanyNotFoundException(clientCompanyId));
    }
}
