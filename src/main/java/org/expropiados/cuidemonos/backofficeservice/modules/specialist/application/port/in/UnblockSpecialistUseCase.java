package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import java.util.UUID;

public interface UnblockSpecialistUseCase {
    Long unblockSpecialist(UUID specialistUUID);
}
