package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetHoursReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

public interface GetHoursReportUseCase {
    GetHoursReport getHoursReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) throws IOException;
}
