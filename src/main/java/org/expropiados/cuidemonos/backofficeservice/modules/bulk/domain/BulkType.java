package org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum BulkType {
    PATIENTS(0),
    SPECIALISTS(1);

    @Getter
    private final Integer value;
}
