package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.out.s3;


import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.thirdparty.AwsStorageAdapter;
import org.springframework.web.multipart.MultipartFile;

@PersistenceAdapter
@RequiredArgsConstructor
public class SpecialistImageS3Adapter implements SaveImageProfilePort {

    private final AwsStorageAdapter awsStorageAdapter;

    @Override
    public String saveImageProfile(MultipartFile file) {
        return awsStorageAdapter.saveFile(file);
    }
}
