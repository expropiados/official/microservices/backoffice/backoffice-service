package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetClientSupervisorByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetClientSupervisorByUUIDService implements GetClientSupervisorByUUIDUseCase {

    public final GetClientSupervisorPort getClientSupervisorPort;

    @Override
    public ClientSupervisor getClientSupervisorByUUID(UUID clientSupervisorUUID) {
        var result = getClientSupervisorPort.getClientSupervisorByUUID(clientSupervisorUUID);
        if (result.isEmpty()) return null;
        return result.get();
    }
}
