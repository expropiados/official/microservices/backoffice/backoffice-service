package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.EditSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.EditSpecialistPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.SpecialistNotFoundException;

@UseCase
@RequiredArgsConstructor
public class EditSpecialistService implements EditSpecialistUseCase {
    public final EditSpecialistPort editSpecialistPort;
    public final GetSpecialistsPort getSpecialistPort;
    public final SaveImageProfilePort saveImageProfilePort;

    @Override
    public Specialist editSpecialist(SpecialistToEdit specialistToEdit) {
        var result = getSpecialistPort.getBySpecialistId(specialistToEdit.getSpecialistId());
        if (result.isEmpty()) throw new SpecialistNotFoundException(specialistToEdit.getSpecialistId());
        var specialist = result.get();
        var file = specialistToEdit.getMultipartFile();
        if (file != null) {
            var imageUrl = saveImageProfilePort.saveImageProfile(file);
            specialistToEdit.setProfileImage(imageUrl);
        }
        if (file == null && specialist.getProfileImage() != null) specialistToEdit.setProfileImage(specialist.getProfileImage());
        return editSpecialistPort.editSpecialist(specialistToEdit, specialist);
    }
}
