package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistExtraInfo;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.*;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class SpecialistsPersistenceAdapter implements SaveSpecialistPort, GetSpecialistsPort, EditSpecialistPort, BlockSpecialistPort, GetSpecialistExtraInfoForSpecialistPort {

    private final SpringJpaSpecialistRepository specialistRepository;
    private final SpecialistMapper specialistMapper;
    private final UserMapper userMapper;

    @Override
    public Specialist saveSpecialist(SpecialistToRegister specialistToRegister, User user) {
        if (user == null) specialistToRegister.getUser().setUuid(UUID.randomUUID());
        var entity = specialistMapper.toSpecialistJpaEntity(specialistToRegister);
        if (user != null) entity.setUser(userMapper.toUserJpaEntity(user));
        entity.setIsBlocked(false);
        entity.setUuid(UUID.randomUUID());
        var result = specialistRepository.save(entity);
        return specialistMapper.toSpecialist(result);
    }

    @Override
    public Specialist getSpecialistByIdUser(Long idUser) {

        var specialistFound = specialistRepository.findByUserId(idUser);
        return specialistMapper.toSpecialist(specialistFound);
    }

    @Override
    public Paginator<Specialist> getSpecialists(Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = specialistRepository.searchSpecialists(pageable, filters.getSearch());

        var content = page.getContent()
                .stream()
                .map(specialistMapper::toSpecialist)
                .collect(Collectors.toList());

        return Paginator.<Specialist>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(content)
                .build();
    }

    @Override
    public Optional<Specialist> getSpecialistByUUID(UUID uuid) {
        var result = specialistRepository.findByUuid(uuid);
        return result.map(specialistMapper::toSpecialist);
    }

    @Override
    public Optional<Specialist> getBySpecialistId(Long specialistId) {
        var result = specialistRepository.findById(specialistId);
        return result.map(specialistMapper::toSpecialist);
    }

    @Override
    public Specialist editSpecialist(SpecialistToEdit specialistToEdit, Specialist specialist) {
        var entity = specialistMapper.toSpecialistJpaEntity(specialistToEdit);
        entity.getUser().setId(specialist.getUser().getId());
        entity.getUser().setUuid(specialist.getUser().getUuid());
        entity.setIsBlocked(specialist.getIsBlocked());
        entity.setUuid(specialist.getUuid());

        var result = specialistRepository.save(entity);
        return specialistMapper.toSpecialist(result);
    }

    @Override
    public Long blockSpecialist(Boolean isBlocked, UUID specialistUUID) {
        var entity = specialistRepository.findByUuid(specialistUUID);
        if (entity.isEmpty()) return -1L;
        entity.get().setIsBlocked(isBlocked);
        var result = specialistRepository.save(entity.get());
        return result.getId();
    }

    @Override
    public Optional<SpecialistExtraInfo> getSpecialistExtraInfoForSpecialist(UUID specialistUserUuid) {
        return specialistRepository
                .findByUser_Uuid(specialistUserUuid)
                .map(s -> new SpecialistExtraInfo(
                        specialistUserUuid,
                        s.getAboutMe(),
                        s.getDegree(),
                        s.rating(),
                        s.getTotalAppointments()
                ));
    }
}
