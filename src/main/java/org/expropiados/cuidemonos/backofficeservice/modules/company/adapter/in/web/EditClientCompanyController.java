package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.EditClientCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class EditClientCompanyController {
    private final JwtHelper jwtHelper;
    private final EditClientCompanyUseCase editClientCompanyUseCase;

    @PostMapping(value = "/companies/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Company registerCompany(@ModelAttribute ClientCompanyToEdit clientCompanyToEdit,
                                   @RequestHeader("Authorization") String token,
                                   @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return editClientCompanyUseCase.editClientCompany(clientCompanyToEdit);
    }
}
