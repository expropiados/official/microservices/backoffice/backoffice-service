package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@Table(name = "GENERAL_USER")
public class UserJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name ="user_uuid", nullable = false)
    private UUID uuid;

    @Column(name ="name", length = 100, nullable = false)
    private String name;

    @Column(name ="document", length = 12, nullable = false)
    @Size(min = 8, max = 12)
    private String document;

    @Column(name ="type_document", nullable = false)
    private int typeDocument;

    @Column(name ="father_lastname", length = 100, nullable = false)
    private String fatherLastname;

    @Column(name ="mother_lastname", length = 100, nullable = false)
    private String motherLastname;

    @Column(name ="birthdate")
    private LocalDate birthdate;
}
