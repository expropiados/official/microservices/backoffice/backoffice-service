package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.BlockSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.BlockSpecialistPort;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class BlockSpecialistService implements BlockSpecialistUseCase {

    private final BlockSpecialistPort blockSpecialistPort;

    @Override
    public Long blockSpecialist(UUID specialistUUID) {
        return blockSpecialistPort.blockSpecialist(true, specialistUUID);
    }
}
