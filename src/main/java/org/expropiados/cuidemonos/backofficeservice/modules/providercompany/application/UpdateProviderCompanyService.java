package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.UpdateProviderCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.UpdateProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class UpdateProviderCompanyService implements UpdateProviderCompanyUseCase {

    public final UpdateProviderCompanyPort updateProviderCompanyPort;

    @Override
    public ProviderCompany updateProviderCompany(ProviderCompanyToUpdate providerCompanyToUpdate){
        return updateProviderCompanyPort.updateProviderCompany(providerCompanyToUpdate);
    }

}
