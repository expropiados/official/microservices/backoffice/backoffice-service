package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompaniesUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetClientCompaniesController {
    private final JwtHelper jwtHelper;
    private final GetClientCompaniesUseCase getClientCompaniesUseCase;

    @GetMapping(value = "/companies")
    public List<Company> getCompanies(@RequestHeader("Authorization") String token,
                                      @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getClientCompaniesUseCase.getClientCompanies();
    }
}
