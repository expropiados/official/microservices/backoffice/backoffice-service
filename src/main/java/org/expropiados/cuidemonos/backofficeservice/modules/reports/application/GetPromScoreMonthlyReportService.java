package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreMonthlyReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetPromScoreMonthlyReportPort;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreMonthlyReport;

import java.io.IOException;
import java.time.LocalDate;

@UseCase
@RequiredArgsConstructor
public class GetPromScoreMonthlyReportService implements GetPromScoreMonthlyReportUseCase {

    public final GetPromScoreMonthlyReportPort getPromScoreMonthlyReportPort;

    @Override
    public GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException {
        return getPromScoreMonthlyReportPort.getPromScoreMonthlyReport(initDate, endDate);
    }
}
