package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetPaginatedClientSupervisorsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class GetPaginatedClientSupervisorsController {

    private final JwtHelper jwtHelper;
    private final GetPaginatedClientSupervisorsUseCase getPaginatedClientSupervisorsUseCase;

    @GetMapping(value = "/companies/{clientCompanyId}/supervisors")
    public Paginator<ClientSupervisor> getClientSupervisorsForCompany(Pageable pageable,
                                                                      @RequestParam(value = "search", defaultValue = "") String search,
                                                                      @PathVariable Long clientCompanyId,
                                                                      @RequestHeader("Authorization") String token,
                                                                      @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole != UserRole.PROVIDER_SUPERVISOR) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        return getPaginatedClientSupervisorsUseCase.getPaginatedClientSupervisors(filters, clientCompanyId);
    }
}
