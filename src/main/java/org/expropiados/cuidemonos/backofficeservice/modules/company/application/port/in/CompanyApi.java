package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CompanyApi {
    private String nombre;
    private String ubigeo;
    private String distrito;
    private String provincia;
    private String departamento;
    private String direccion;
}
