package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

import java.io.Serializable;

@Getter
public class BulkLoadNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_BLK_001";
    private final String message;
    private final transient Object data;

    @Value
    static class Data implements Serializable {
        Long bulkLoadId;
    }

    public BulkLoadNotFoundException(Long bulkLoadId) {
        super();
        this.message = String.format("Bulk load  with ID '%d' not found", bulkLoadId);
        this.data = new BulkLoadNotFoundException.Data(bulkLoadId);
    }
}
