package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoadToRegister;

public interface RegisterBulkLoadPort {
    Long registerBulkLoad(BulkLoadToRegister bulkLoadToRegister);
}
