package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToRegisterDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.RegisterPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.RegisterUserRoleUseCase;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/patients")
public class RegisterPatientController {
    private final JwtHelper jwtHelper;
    private final RegisterPatientUseCase registerPatientUseCase;
    private final PatientDTOMapper patientDTOMapper;
    private final RegisterUserRoleUseCase registerUserRoleUseCase;

    @PostMapping(value = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Patient registerPatient(@ModelAttribute PatientToRegisterDTO patientEntry,
                                   @RequestHeader("Authorization") String token,
                                   @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var registeredPatient = patientDTOMapper.toPatientToRegister(patientEntry);
        var patient = registerPatientUseCase.registerPatient(registeredPatient, patientEntry.getClientCompanyId());
        registerUserRoleUseCase.registerUserRole(patient.getEmail(), UserRole.PATIENT);
        return patient;
    }
}
