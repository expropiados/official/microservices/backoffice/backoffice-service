package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

public interface RegisterProviderBulkRequestPort {
    void registerProviderBulkRequest(Long providerSupervisorId, Long bulkId);
}
