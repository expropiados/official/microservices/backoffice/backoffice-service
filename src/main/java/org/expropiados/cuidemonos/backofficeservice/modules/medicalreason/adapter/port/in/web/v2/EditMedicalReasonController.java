package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.EditMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class EditMedicalReasonController {

    private final JwtHelper jwtHelper;
    private final EditMedicalReasonUseCase editMedicalReasonUseCase;

    @PutMapping(value = "/medical-reasons/{reasonId}")
    public MedicalReason editMedicalReason(@PathVariable Long reasonId,
                                           @RequestBody MedicalReasonToEdit medicalReasonToEdit,
                                           @RequestHeader("Authorization") String token,
                                           @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (!userRole.isIn(UserRole.ADMIN, UserRole.PROVIDER_SUPERVISOR)) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        return editMedicalReasonUseCase.editMedicalReason(reasonId, medicalReasonToEdit);
    }
}
