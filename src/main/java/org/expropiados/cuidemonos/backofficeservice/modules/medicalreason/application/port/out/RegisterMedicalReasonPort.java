package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

public interface RegisterMedicalReasonPort {
    MedicalReason registerMedicalReason(MedicalReasonToRegister medicalReasonToRegister);
}
