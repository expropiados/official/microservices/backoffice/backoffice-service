package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.EditMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.GetMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.UpdateMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

import javax.transaction.Transactional;

@UseCase
@Transactional
@RequiredArgsConstructor
public class EditMedicalReasonService implements EditMedicalReasonUseCase {
    private final GetMedicalReasonUseCase getMedicalReasonUseCase;
    private final UpdateMedicalReasonPort updateMedicalReasonPort;

    @Override
    public MedicalReason editMedicalReason(Long medicalReasonId, MedicalReasonToEdit medicalReasonToEdit) {
        var medicalReason = getMedicalReasonUseCase.getMedicalReason(medicalReasonId);
        medicalReason.setName(medicalReasonToEdit.getName());
        medicalReason.setDescription(medicalReasonToEdit.getDescription());

        return updateMedicalReasonPort.updateMedicalReason(medicalReason);
    }
}
