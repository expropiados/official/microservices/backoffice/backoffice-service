package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApisNetIDsPort {

    @GET("dni")
    Call<UserApi> getUserInformation(@Query("numero") String numero);
}
