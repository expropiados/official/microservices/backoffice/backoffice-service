package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

public interface DeleteMedicalReasonPort {
    void deleteMedicalReason(Long medicalReasonId);
}
