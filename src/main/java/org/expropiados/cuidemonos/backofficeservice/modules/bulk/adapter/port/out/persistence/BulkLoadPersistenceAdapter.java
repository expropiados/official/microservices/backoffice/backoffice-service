package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetBulkLoadPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterBulkLoadPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoadToRegister;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class BulkLoadPersistenceAdapter implements RegisterBulkLoadPort, GetBulkLoadPort {

    private final BulkLoadMapper mapper;
    private final BulkLoadJpaRepository bulkLoadJpaRepository;

    @Override
    public Long registerBulkLoad(BulkLoadToRegister bulkLoadToRegister) {
        var row = mapper.toBulkLoadJpaEntity(bulkLoadToRegister);
        var saved = bulkLoadJpaRepository.save(row);
        return saved.getId();
    }

    @Override
    public Optional<BulkLoad> getBulkLoad(Long bulkId) {
        return bulkLoadJpaRepository.findById(bulkId).map(mapper::toBulkLoad);
    }
}
