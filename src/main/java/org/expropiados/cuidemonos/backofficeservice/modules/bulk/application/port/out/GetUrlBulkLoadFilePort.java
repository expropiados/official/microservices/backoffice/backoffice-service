package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import java.net.URL;

public interface GetUrlBulkLoadFilePort {
    URL getUrlBulkLoadFile(String fileKey);
}
