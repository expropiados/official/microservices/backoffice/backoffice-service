package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkSourceFileType;

import java.net.URL;

public interface GetProviderBulkLoadSourceFileUseCase {
    URL getProviderBulkLoadSourceFile(Long bulkId, BulkSourceFileType bulkSourceFileType);
}
