package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPaginatedPatientsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPaginatedPatientsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

@UseCase
@RequiredArgsConstructor
public class GetPaginatedPatientsService implements GetPaginatedPatientsUseCase {
    private final GetPaginatedPatientsPort getPaginatedPatientsPort;

    @Override
    public Paginator<Patient> getPaginatedPatients(Filters filters, Long clientCompanyId) {
        return getPaginatedPatientsPort.getPaginatedPatients(filters, clientCompanyId);
    }
}
