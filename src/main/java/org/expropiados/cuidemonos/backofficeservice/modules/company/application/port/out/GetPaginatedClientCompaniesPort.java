package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

public interface GetPaginatedClientCompaniesPort {
    Paginator<Company> getPaginatedClientCompanies(Filters filters);
}
