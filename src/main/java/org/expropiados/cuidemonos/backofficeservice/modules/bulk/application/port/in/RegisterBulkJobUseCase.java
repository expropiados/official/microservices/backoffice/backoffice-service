package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

public interface RegisterBulkJobUseCase {
    void registerBulkJob(Long bulkId);
}
