package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.Optional;
import java.util.UUID;

public interface GetProviderCompanyPort {

    Optional<ProviderCompany> getProviderCompany(UUID uuid);
}
