package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GetPatientPort {
    List<Patient> getPatients();
    Optional<Patient> getByPatientId(Long patientId);
    Optional<Patient> getByUserId(Long userId);
    Optional<Patient> getByDNI(String dni);
    Optional<Patient> getByPatientUUID(UUID patientUUID);
}
