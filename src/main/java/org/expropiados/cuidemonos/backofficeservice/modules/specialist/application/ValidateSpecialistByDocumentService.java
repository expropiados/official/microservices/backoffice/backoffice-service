package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.AllArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.ValidateSpecialistByDocumentUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.ids.ApisNetIDsAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;

import java.io.IOException;

@UseCase
@AllArgsConstructor
public class ValidateSpecialistByDocumentService implements ValidateSpecialistByDocumentUseCase {

    private final GetSpecialistsPort getSpecialistPort;
    private final GetUserPort getUserPort;

    @Override
    public SpecialistChecked validateSpecialistByDocument(String document) {
        UserApi userApi = null;
        boolean isSpecialist;
        var user = getUserPort.getUserByDocument(document);
        if (user != null) {
            isSpecialist = checkIfUserIsSpecialist(user.getId());
        }else {
            isSpecialist = false;
            userApi = validateExistingUserWithApi(document);
        }
        return new SpecialistChecked(isSpecialist, userApi,user);
    }

    private UserApi validateExistingUserWithApi(String dni){
        var  apisNetIDsAdapter = new ApisNetIDsAdapter();
        try{
            return apisNetIDsAdapter.start(dni);
        }catch (IOException ex){
            return null;
        }
    }
    private boolean checkIfUserIsSpecialist(Long userId){
        var specialist = getSpecialistPort.getSpecialistByIdUser(userId);
        if ( specialist != null) return true;
        return false;
    }
}
