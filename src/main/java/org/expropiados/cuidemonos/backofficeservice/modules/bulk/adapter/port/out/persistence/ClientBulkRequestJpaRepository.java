package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ClientBulkRequestJpaRepository extends JpaRepository<ClientBulkRequestJpaEntity, Long> {

    @Query("SELECT CB FROM ClientBulkRequestJpaEntity CB WHERE CB.clientSupervisor.clientCompanyId = :companyId ORDER BY CB.bulkLoad.requestedAt DESC")
    Page<ClientBulkRequestJpaEntity> findAllByCompanyIdOrderByBulkLoadRequestedAt(Pageable pageable, @Param("companyId") Long clientCompanyId);

    @Query("SELECT CB FROM ClientBulkRequestJpaEntity CB WHERE CB.bulkLoadId = :bulkId AND CB.clientSupervisor.clientCompanyId = :companyId")
    Optional<ClientBulkRequestJpaEntity> findByBulkIdAndCompanyId(@Param("bulkId") Long bulkId, @Param("companyId") Long clientCompanyId);
}
