package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetAllClientBulkLoadsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetAllProviderBulkLoadsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetAllClientBulkLoadsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetAllProviderBulkLoadsPort;

@UseCase
@RequiredArgsConstructor
public class GetAllClientBulkLoadsService implements GetAllClientBulkLoadsUseCase {

    private final GetAllClientBulkLoadsPort getAllClientBulkLoadsPort;

    @Override
    public Paginator<BulkLoadWithResponsible> getAllClientBulkLoads(Long clientCompanyId, Filters filters) {
        return getAllClientBulkLoadsPort.getAllClientBulkLoads(clientCompanyId, filters);
    }
}
