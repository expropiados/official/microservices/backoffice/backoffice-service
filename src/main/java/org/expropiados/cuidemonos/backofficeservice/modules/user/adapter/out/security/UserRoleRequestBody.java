package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.security;

import lombok.Value;

@Value
public class UserRoleRequestBody {
    String email;
    String role;
}
