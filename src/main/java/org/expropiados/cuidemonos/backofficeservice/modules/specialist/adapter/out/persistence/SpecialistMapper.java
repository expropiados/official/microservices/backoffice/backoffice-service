package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface SpecialistMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "phoneNumber", source = "phoneNumber")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "profileImage", source = "profileImage")
    @Mapping(target = "isBlocked", source = "isBlocked")
    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "aboutMe", source = "aboutMe")
    @Mapping(target = "degree", source = "degree")
    @Mapping(target = "rating", expression = "java(specialistJpaEntity.rating())")
    @Mapping(target = "totalAppointments", source = "totalAppointments")
    Specialist toSpecialist(SpecialistJpaEntity specialistJpaEntity);

    @Mapping(target = "user", source = "user")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "phoneNumber", source = "phoneNumber")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "aboutMe", source = "aboutMe")
    @Mapping(target = "degree", source = "degree")
    @Mapping(target = "profileImage", source = "profileImage")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "totalRating", ignore = true)
    @Mapping(target = "totalAppointments", ignore = true)
    SpecialistJpaEntity toSpecialistJpaEntity(SpecialistToRegister specialistToRegister);

    @Mapping(target = "id", source = "specialistId")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "phoneNumber", source = "phoneNumber")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "aboutMe", source = "aboutMe")
    @Mapping(target = "degree", source = "degree")
    @Mapping(target = "profileImage", source = "profileImage")
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "totalRating", ignore = true)
    @Mapping(target = "totalAppointments", ignore = true)
    SpecialistJpaEntity toSpecialistJpaEntity(SpecialistToEdit specialistToEdit);
}
