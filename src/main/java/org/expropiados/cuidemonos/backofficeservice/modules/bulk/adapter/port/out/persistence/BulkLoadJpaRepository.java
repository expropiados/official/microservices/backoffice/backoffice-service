package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BulkLoadJpaRepository extends JpaRepository<BulkLoadJpaEntity, Long> {
}
