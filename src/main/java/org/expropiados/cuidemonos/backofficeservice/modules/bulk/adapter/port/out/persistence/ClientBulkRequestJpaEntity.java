package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.*;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence.ClientSupervisorJpaEntity;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@ToString
@Getter @Setter
@RequiredArgsConstructor
@Entity @Table(name = "CLIENT_BULK_REQUEST")
public class ClientBulkRequestJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_bulk_request_id")
    private Long id;

    @Column(name = "client_supervisor_id", nullable = false)
    private Long clientSupervisorId;

    @Column(name = "bulk_load_id", nullable = false)
    private Long bulkLoadId;


    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_supervisor_id", insertable = false, updatable = false)
    private ClientSupervisorJpaEntity clientSupervisor;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bulk_load_id", insertable = false, updatable = false)
    private BulkLoadJpaEntity bulkLoad;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ClientBulkRequestJpaEntity that = (ClientBulkRequestJpaEntity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 1682494654;
    }
}
