package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompaniesUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompanyByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/companies")
public class GetClientCompanyByUUIDController {

    private final JwtHelper jwtHelper;
    private final GetClientCompanyByUUIDUseCase getClientCompanyByUUIDUseCase;

    @GetMapping(value = "/")
    public Company getClientCompanyByUUID(@RequestParam UUID patientUUID,
                                          @RequestHeader("Authorization") String token,
                                          @RequestHeader("role") Integer role){
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        return getClientCompanyByUUIDUseCase.getClientCompanyByUUID(patientUUID);
    }
}
