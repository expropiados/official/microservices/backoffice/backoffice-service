package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;

import java.util.UUID;

public interface BlockSpecialistPort {
    Long blockSpecialist(Boolean isBlocked, UUID specialistUUID);
}
