package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetHoursReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetHoursReportPort;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetHoursReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetHoursReportService implements GetHoursReportUseCase {

    public final GetHoursReportPort getHoursReportPort;

    @Override
    public GetHoursReport getHoursReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) throws IOException {
        return getHoursReportPort.getHoursReport(startDate, endDate, specialistUUID);
    }
}
