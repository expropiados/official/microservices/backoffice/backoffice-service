package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;

public interface RegisterUserRolePort {
    void registerUserRole(String email, UserRole userRole);
}
