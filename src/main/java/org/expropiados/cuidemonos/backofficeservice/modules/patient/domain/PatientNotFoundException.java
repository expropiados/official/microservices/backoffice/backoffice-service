package org.expropiados.cuidemonos.backofficeservice.modules.patient.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class PatientNotFoundException extends RuntimeException{
    public PatientNotFoundException(){
        super("El paciente no existe");
    }
}
