package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.company.application.ClientCompanyChecked;

public interface ValidateClientCompanyUseCase {
    ClientCompanyChecked validateClientCompany(String ruc);
}
