package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

@Data
@AllArgsConstructor
public class ProviderSupervisor {
    private Long id;
    private User user;
    private String email;
    private String phoneNumber;
    private String profileImage;
    private Boolean isBlocked;
}
