package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

public interface RegisterSpecialistUseCase {

    Specialist registerSpecialist( SpecialistToRegister specialistToRegister);
}
