package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ClientCompanyToEdit {
    @NotNull
    Long id;

    @NotNull
    String name;

    @NotNull
    String ruc;

    String address;

    String email;

    String phone;

    MultipartFile multipartFile;
    
    String imageSourceFile;
}
