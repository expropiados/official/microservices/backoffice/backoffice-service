package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetReportDateRangeForClientCompanyDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.BadDateRangeException;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyFreqReport;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMedicalReasonMonthlyFreqReportUseCase;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("/reports")
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyFreqReportController {
    private final JwtHelper jwtHelper;
    private final GetMedicalReasonMonthlyFreqReportUseCase getMedicalReasonMonthly;

    @GetMapping("/pre-poll/freq_monthly")
    public GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(@RequestHeader("Authorization") String token,
                                                                               @RequestHeader("role") Integer role,
                                                                               GetReportDateRangeForClientCompanyDTO getReportDateRangeDTO) throws IOException {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.CLIENT_SUPERVISOR) throw new AuthenticationException(userRole.name());
        if (getReportDateRangeDTO.getInitDate().isAfter(getReportDateRangeDTO.getEndDate()))
            throw new BadDateRangeException(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate());
        return getMedicalReasonMonthly.getMedicalReasonMonthlyFreqReport(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate(), getReportDateRangeDTO.getClientCompanyId());
    }
}
