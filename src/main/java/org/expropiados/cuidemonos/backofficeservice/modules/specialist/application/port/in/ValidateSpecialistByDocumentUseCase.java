package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.SpecialistChecked;

public interface ValidateSpecialistByDocumentUseCase {

    SpecialistChecked validateSpecialistByDocument(String dni);
}
