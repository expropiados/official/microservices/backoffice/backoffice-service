package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetPromScoreMonthlyReport {
    private List<GetMonthlyGeneralReport> seriesLine;
    private List<String> categories;
}
