package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;

public interface RegisterUserRoleUseCase {
    void registerUserRole(String email, UserRole userRole);
}
