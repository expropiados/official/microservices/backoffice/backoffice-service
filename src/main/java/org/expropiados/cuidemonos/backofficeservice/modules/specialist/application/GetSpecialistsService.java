package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetSpecialistsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

@UseCase
@RequiredArgsConstructor
public class GetSpecialistsService implements GetSpecialistsUseCase {

    public final GetSpecialistsPort getSpecialistsPort;

    @Override
    public Paginator<Specialist> getSpecialists(Filters filters) {
        return getSpecialistsPort.getSpecialists(filters);
    }
}
