package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParameters;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParametersDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.*;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@PersistenceAdapter
@RequiredArgsConstructor
public class CompanyPersistenceAdapter implements RegisterCompanyPort, GetClientCompanyPort, EditClientCompanyPort, GetPaginatedClientCompaniesPort, UpdateClientCompanyParametersPort, GetClientCompanyParametersPort {
    private final SpringJpaCompanyRepository companyRepository;
    private final CompanyMapper companyMapper;

    @Override
    public Company registerCompany(ClientCompanyToRegister company, UUID uuid) {
        var entity = companyMapper.toCompanyEntity(company);
        entity.setAppointmentDuration(30);
        entity.setAppointmentMaxCancelled(1);
        entity.setAppointmentMaxCant(1);
        entity.setAppointmentTimeToCancel(1);
        entity.setUuid(uuid);
        entity.setCreatedAt(LocalDateTimePeruZone.now());
        var companyResponse = companyRepository.save(entity);
        return companyMapper.toCompany(companyResponse);
    }

    @Override
    public List<Company> getClientCompanies() {
        var result = companyRepository.findAll();
        return companyMapper.toClientCompanies(result);
    }

    @Override
    public Company getClientCompanyByRUC(String ruc) {
        var clientCompanyFound = companyRepository.findByRuc(ruc);
        return companyMapper.toCompany(clientCompanyFound);
    }

    @Override
    public Optional<Company> getClientCompanyById(Long id) {
        var result = companyRepository.findById(id);
        return result.map(companyMapper::toCompany);
    }

    @Override
    public Optional<Company> getClientCompanyByUUID(UUID uuid) {
        var clientCompanyFound = companyRepository.findByUuid(uuid);
        return clientCompanyFound.map(companyMapper::toCompany);
    }

    @Override
    public Company editClientCompany(ClientCompanyToEdit clientCompanyToEdit) {
        var result = companyRepository.findById(clientCompanyToEdit.getId());
        if (result.isEmpty()) return null;
        var entity = companyMapper.toCompanyEntity(clientCompanyToEdit);
        if (result.get().getImageSourceFile() != null && clientCompanyToEdit.getImageSourceFile() == null)
            entity.setImageSourceFile(result.get().getImageSourceFile());
        entity.setAppointmentTimeToCancel(result.get().getAppointmentTimeToCancel());
        entity.setAppointmentMaxCant(result.get().getAppointmentMaxCant());
        entity.setAppointmentMaxCancelled(result.get().getAppointmentMaxCancelled());
        entity.setAppointmentDuration(result.get().getAppointmentDuration());
        entity.setUpdatedAt(LocalDateTimePeruZone.now());
        entity.setUuid(result.get().getUuid());
        var response = companyRepository.save(entity);
        return companyMapper.toCompany(response);
    }

    @Override
    public Paginator<Company> getPaginatedClientCompanies(Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = companyRepository.searchClientCompanies(pageable, filters.getSearch());

        var data = page.getContent()
                .stream()
                .map(companyMapper::toCompany)
                .collect(Collectors.toList());

        return Paginator.<Company>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }

    @Override
    public Optional<ClientCompanyParameters> getParametersFromClientCompany(Long clientCompanyId) {
        var entity = companyRepository.findById(clientCompanyId);
        if (entity.isEmpty()) return Optional.empty();
        var company = entity.get();
        return Optional.of(companyMapper.toClientCompanyParameters(company));
    }

    @Override
    public Optional<Company> updateProviderCompanyParameters(Long clientCompanyId, ClientCompanyParametersDTO clientParameters) {
        var entity = companyRepository.findById(clientCompanyId);
        if (entity.isEmpty()) return Optional.empty();
        var company = entity.get();
        company.setAppointmentDuration(clientParameters.getAppointmentDuration());
        company.setAppointmentMaxCant(clientParameters.getAppointmentMaxCant());
        company.setAppointmentMaxCancelled(clientParameters.getAppointmentMaxCancelled());
        company.setAppointmentTimeToCancel(clientParameters.getAppointmentTimeToCancel());
        var result = companyRepository.save(company);
        return Optional.of(companyMapper.toCompany(result));
    }
}
