package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

public interface EditClientSupervisorPort {
    ClientSupervisor editClientSupervisor(ClientSupervisorToEdit clientSupervisorToEdit);
}
