package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.UpdateProviderCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
public class UpdateProviderCompanyController {

    private final JwtHelper jwtHelper;
    private final UpdateProviderCompanyUseCase updateProviderCompanyUseCase;

    @PostMapping("/provider-companies")
    public ProviderCompany updateProviderCompany(@RequestBody ProviderCompanyToUpdate providerCompanyToUpdate,
                                                 @RequestHeader("Authorization") String token,
                                                 @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return updateProviderCompanyUseCase.updateProviderCompany(providerCompanyToUpdate);
    }
}
