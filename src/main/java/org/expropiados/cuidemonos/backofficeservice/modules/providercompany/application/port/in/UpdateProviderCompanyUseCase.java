package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

public interface UpdateProviderCompanyUseCase {
    ProviderCompany updateProviderCompany(ProviderCompanyToUpdate providerCompany);
}
