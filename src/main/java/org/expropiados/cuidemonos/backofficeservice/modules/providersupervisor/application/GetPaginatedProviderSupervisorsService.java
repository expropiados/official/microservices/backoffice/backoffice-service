package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.in.GetPaginatedProviderSupervisorsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.out.GetPaginatedProviderSupervisorsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain.ProviderSupervisor;

@UseCase
@RequiredArgsConstructor
public class GetPaginatedProviderSupervisorsService implements GetPaginatedProviderSupervisorsUseCase {
    private final GetPaginatedProviderSupervisorsPort getPaginatedProviderSupervisorsPort;

    @Override
    public Paginator<ProviderSupervisor> getPaginatedProviderSupervisors(Filters filters) {
        return getPaginatedProviderSupervisorsPort.getPaginatedProviderSupervisors(filters);
    }
}
