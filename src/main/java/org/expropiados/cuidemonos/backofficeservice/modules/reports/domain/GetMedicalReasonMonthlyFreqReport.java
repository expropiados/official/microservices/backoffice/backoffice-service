package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetMedicalReasonMonthlyFreqReport {
    private List<GetMedicalReasonLastFrequency> motivos;
    private List<GetMedicalReasonMonthly> series;
    private List<String> categories;
}
