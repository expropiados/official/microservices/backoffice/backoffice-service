package org.expropiados.cuidemonos.backofficeservice.modules.company.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Company {
    private Long id;
    private String name;
    private String ruc;
    private String address;
    private String phone;
    private String email;
    private String imageSourceFile;
    private UUID uuid;

    public String getFullName() {
        return "El nombre completo es " + name;
    }
}
