package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TypeDocumentMapper {

    default TypeDocument toTypeDocument(Integer value){
        switch (value){
            case 0:
                return TypeDocument.DNI;

            case 1:
                return TypeDocument.PASAPORTE;

            case 2:
                return TypeDocument.CARNET;
            default:
                return null;
        }
    }
}
