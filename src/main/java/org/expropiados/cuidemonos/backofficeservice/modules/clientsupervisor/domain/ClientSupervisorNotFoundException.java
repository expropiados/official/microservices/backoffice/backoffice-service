package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

@Getter
public class ClientSupervisorNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_CLS_001";
    private final String message;
    private final transient Object data;


    @Value
    static class Data {
        Long userId;
    }

    public ClientSupervisorNotFoundException(Long userId) {
        super();
        this.message = String.format("Client Supervisor %d not found", userId);
        this.data = new ClientSupervisorNotFoundException.Data(userId);
    }
}
