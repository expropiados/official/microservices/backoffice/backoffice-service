package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;


import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.RegisterSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegisterDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.RegisterUserRoleUseCase;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/specialists")
public class RegisterSpecialistController {

    private final JwtHelper jwtHelper;
    private final RegisterSpecialistUseCase registeredSpecialistUseCase;
    private final SpecialistDTOMapper specialistDTOMapper;
    private final RegisterUserRoleUseCase registerUserRoleUseCase;

    @PostMapping(value = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Specialist registerSpecialist(@ModelAttribute SpecialistToRegisterDTO specialistToRegisterDTO,
                                         @RequestHeader("Authorization") String token,
                                         @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var specialistToRegister = specialistDTOMapper.toSpecialistToRegister(specialistToRegisterDTO);
        var specialist = registeredSpecialistUseCase.registerSpecialist(specialistToRegister);
        registerUserRoleUseCase.registerUserRole(specialist.getEmail(), UserRole.SPECIALIST);
        return specialist;
    }
}
