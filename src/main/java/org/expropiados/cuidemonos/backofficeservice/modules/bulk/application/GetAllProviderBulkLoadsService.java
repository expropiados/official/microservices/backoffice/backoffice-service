package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetAllProviderBulkLoadsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetAllProviderBulkLoadsPort;

@UseCase
@RequiredArgsConstructor
public class GetAllProviderBulkLoadsService implements GetAllProviderBulkLoadsUseCase {

    private final GetAllProviderBulkLoadsPort getAllProviderBulkLoadsPort;

    @Override
    public Paginator<BulkLoadWithResponsible> getAllProviderBulkLoads(Filters filters) {
        return getAllProviderBulkLoadsPort.getAllProviderBulkLoads(filters);
    }
}
