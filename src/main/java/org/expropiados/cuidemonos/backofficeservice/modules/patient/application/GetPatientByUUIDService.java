package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPatientByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetPatientByUUIDService implements GetPatientByUUIDUseCase {

    public final GetPatientPort getPatientPort;

    @Override
    public Patient getPatientByUUID(UUID patientUUID) {
        var patient = getPatientPort.getByPatientUUID(patientUUID);
        if (patient.isEmpty()) return null;
        return patient.get();
    }
}
