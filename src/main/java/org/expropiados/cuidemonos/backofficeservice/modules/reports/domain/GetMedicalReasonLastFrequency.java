package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Data;

@Data
public class GetMedicalReasonLastFrequency {
    private String motivo;
    private Integer rate;
    private Integer value;
}
