package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetReportDateRangeSpecialistDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.BadDateRangeException;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMonthlyPostPollReport;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetReportDateRangeDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMonthlyPostPollReportUseCase;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;


@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/reports")
public class GetMonthlyPostPollReportController {
    private final JwtHelper jwtHelper;
    private final GetMonthlyPostPollReportUseCase getMonthlyPostPollReportUseCase;

    @GetMapping("/post-poll/monthly")
    public GetMonthlyPostPollReport getMonthlyPostPollReport(@RequestHeader("Authorization") String token,
                                                             @RequestHeader("role") Integer role,
                                                             GetReportDateRangeSpecialistDTO getReportDateRangeDTO) throws IOException {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.PROVIDER_SUPERVISOR) throw new AuthenticationException(userRole.name());
        if (getReportDateRangeDTO.getInitDate().isAfter(getReportDateRangeDTO.getEndDate()))
            throw new BadDateRangeException(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate());
        return getMonthlyPostPollReportUseCase.getMonthlyPostPollReport(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate(), getReportDateRangeDTO.getSpecialistUUID());
    }
}
