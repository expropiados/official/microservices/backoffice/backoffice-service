package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import java.io.File;

public interface RegisterClientBulkLoadUseCase {
    void registerClientBulkLoad(File file, String originalFilename, Long clientSupervisorId);
}
