package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpecialistExtraInfo {
    private UUID specialistUserUuid;
    private String aboutMe;
    private String degree;
    private Float rating;
    private Long totalAppointments;
}
