package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.EditClientSupervisorUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.EditClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisorNotFoundException;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;

@UseCase
@RequiredArgsConstructor
public class EditClientSupervisorService implements EditClientSupervisorUseCase {

    public final EditClientSupervisorPort editClientSupervisorPort;
    public final SaveImageProfilePort saveImageProfilePort;

    @Override
    public ClientSupervisor editClientSupervisor(ClientSupervisorToEdit clientSupervisorToEdit) {
        var file = clientSupervisorToEdit.getMultipartFile();
        if (file != null) {
            var imageUrl = saveImageProfilePort.saveImageProfile(file);
            clientSupervisorToEdit.setProfileImage(imageUrl);
        }

        var result =  editClientSupervisorPort.editClientSupervisor(clientSupervisorToEdit);
        if (result == null) throw new ClientSupervisorNotFoundException(clientSupervisorToEdit.getClientSupervisorId());
        return result;
    }
}
