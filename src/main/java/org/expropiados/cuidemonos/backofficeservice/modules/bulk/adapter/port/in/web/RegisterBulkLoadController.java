package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.files.TmpFileHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterClientBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterProviderBulkLoadUseCase;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RegisterBulkLoadController {
    private final RegisterProviderBulkLoadUseCase providerBulkLoadUseCase;
    private final RegisterClientBulkLoadUseCase clientBulkLoadUseCase;
    private final TmpFileHelper tmpFileHelper;
    private final JwtHelper jwtHelper;

    @PostMapping(value = "/bulk-loads", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void registerSpecialistsBulkLoad(@RequestParam("file") MultipartFile csvMultipartFile,
                                            @RequestHeader("Authorization") String token,
                                            @RequestHeader("role") Integer role) throws IOException {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var originalFilename = csvMultipartFile.getOriginalFilename();
        var csvFile = tmpFileHelper.multipartToTmpFile(csvMultipartFile);

        if (userRole == UserRole.CLIENT_SUPERVISOR) {
            var supervisorId = tokenBody.getClientSupervisorId();
            clientBulkLoadUseCase.registerClientBulkLoad(csvFile, originalFilename, supervisorId);
            return;
        }

        if (userRole == UserRole.PROVIDER_SUPERVISOR) {
            var supervisorId = tokenBody.getProviderSupervisorId();
            providerBulkLoadUseCase.registerBulkLoad(csvFile, originalFilename, supervisorId);
            return;
        }

        throw new NotAuthorizedForRoleException(userRole);
    }
}
