package org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum BulkState {
    PENDING(0),
    PROGRESS(1),
    FINISHED(2),
    FAILED(3);

    @Getter
    private final Integer value;
}
