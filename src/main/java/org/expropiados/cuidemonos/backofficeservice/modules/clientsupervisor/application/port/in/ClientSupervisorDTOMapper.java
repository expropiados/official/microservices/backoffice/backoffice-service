package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ClientSupervisorDTOMapper {

    @Mapping(source = "clientCompanyId", target = "clientCompanyId")
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "multipartFile", target = "multipartFile")
    ClientSupervisorToRegister toClientSupervisorToRegister (ClientSupervisorToRegisterDTO clientSupervisorToRegisterDTO);

    @Mapping(source = "clientSupervisorId", target = "clientSupervisorId")
    @Mapping(source = "clientCompanyId", target = "clientCompanyId")
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "multipartFile", target = "multipartFile")
    ClientSupervisorToEdit toClientSupervisorToEdit (ClientSupervisorToEditDTO clientSupervisorToEditDTO);
}
