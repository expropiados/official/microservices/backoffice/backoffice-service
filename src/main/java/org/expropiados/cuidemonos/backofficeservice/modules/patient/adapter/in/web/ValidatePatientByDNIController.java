package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientChecked;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.ValidatePatientByDocumentUseCase;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/patients")
public class ValidatePatientByDNIController {
    private final JwtHelper jwtHelper;
    private final ValidatePatientByDocumentUseCase validatePatientByDocumentUseCase;

    @GetMapping("/checking")
    public PatientChecked validatePatientByDocument(@RequestParam(name = "dni", required = false)String dni,
                                                    @RequestHeader("Authorization") String token,
                                                    @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return validatePatientByDocumentUseCase.validatePatientByDocument(dni);
    }
}
