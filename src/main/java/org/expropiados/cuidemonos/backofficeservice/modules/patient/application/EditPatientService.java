package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.EditPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.EditPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.PatientNotFoundException;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.EditUserPort;

@UseCase
@RequiredArgsConstructor
public class EditPatientService implements EditPatientUseCase {
    public final EditPatientPort editPatientPort;
    public final GetPatientPort getPatientPort;
    public final SaveImageProfilePort saveImageProfilePort;

    @Override
    public Patient editPatient(PatientToEdit patientToEdit){
        var result = getPatientPort.getByPatientId(patientToEdit.getPatientId());
        if (result.isEmpty()) throw new PatientNotFoundException();
        var patient = result.get();
        var file = patientToEdit.getMultipartFile();
        if (file != null){
            var imageUrl = saveImageProfilePort.saveImageProfile(file);
            patientToEdit.setProfileImage(imageUrl);
        }
        if (file == null && patient.getProfileImage() != null) patientToEdit.setProfileImage(patient.getProfileImage());
        return editPatientPort.editPatient(patientToEdit, patient);
    }
}
