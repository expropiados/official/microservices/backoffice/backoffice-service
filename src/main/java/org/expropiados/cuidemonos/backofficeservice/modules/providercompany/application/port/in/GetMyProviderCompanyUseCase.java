package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.Optional;
import java.util.UUID;

public interface GetMyProviderCompanyUseCase {
    ProviderCompany getMyProviderCompany();
}
