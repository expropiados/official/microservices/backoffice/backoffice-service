package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;

public interface GetAllClientBulkLoadsUseCase {
    Paginator<BulkLoadWithResponsible> getAllClientBulkLoads(Long clientCompanyId, Filters filters);
}
