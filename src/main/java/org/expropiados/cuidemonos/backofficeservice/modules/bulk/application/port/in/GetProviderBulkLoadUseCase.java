package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

public interface GetProviderBulkLoadUseCase {
    BulkLoadWithResponsible getProviderBulkLoad(Long bulkId);
}
