package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Data;

@Data
public class GetMonthlyGeneralReport {
    private String name;
    private double[] data;
}
