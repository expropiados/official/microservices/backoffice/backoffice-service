package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
public class ClientSupervisorToEdit {
    Long clientSupervisorId;
    UserToRegister user;
    MultipartFile multipartFile;
    String email;
    String phoneNumber;
    String address;
    String profileImage;
    Long clientCompanyId;
}
