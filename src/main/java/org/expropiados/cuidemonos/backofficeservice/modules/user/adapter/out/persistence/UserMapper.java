package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring", uses = {TypeDocumentMapper.class})
public interface UserMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "document", target = "document")
    @Mapping(source = "typeDocument", target = "typeDocument")
    @Mapping(source = "fatherLastname", target = "fatherLastname")
    @Mapping(source = "motherLastname", target = "motherLastname")
    @Mapping(source = "birthdate", target = "birthdate")
    User toUser(UserJpaEntity userJpaEntity);
    List<User> toUsers(List<UserJpaEntity> userJpaEntities);

    @InheritInverseConfiguration
    @Mapping(source = "typeDocument.value", target = "typeDocument")
    UserJpaEntity toUserJpaEntity(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "document", target = "document")
    @Mapping(source = "typeDocument.value", target = "typeDocument")
    @Mapping(source = "fatherLastname", target = "fatherLastname")
    @Mapping(source = "motherLastname", target = "motherLastname")
    @Mapping(source = "birthdate", target = "birthdate")
    UserJpaEntity toUserJpaEntity(UserToRegister registeredPerson);
}
