package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.AllArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientChecked;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.ValidatePatientByDocumentUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.ids.ApisNetIDsAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;

import java.io.IOException;

@UseCase
@AllArgsConstructor
public class ValidatePatientByDocumentService implements ValidatePatientByDocumentUseCase {

    private final GetUserPort getUserPort;
    private final GetPatientPort getPatientPort;

    @Override
    public PatientChecked validatePatientByDocument(String dni) {
        UserApi userApi=null;
        boolean isPatient;
        var user = getUserPort.getUserByDocument(dni);
        if (user != null) {
            isPatient = checkIfUserIsPatient(user.getId());
        }else {
            isPatient = false;
            userApi = validateExistingUserWithApi(dni);
        }
        return PatientChecked.builder()
                .isPatient(isPatient).user(user).userApi(userApi)
                .build();
    }

    private UserApi validateExistingUserWithApi(String dni){
        var apisNetIDsAdapter = new ApisNetIDsAdapter();
        try{
            return apisNetIDsAdapter.start(dni);
        }catch (IOException ex){
            return null;
        }
    }

    private boolean checkIfUserIsPatient(Long userId){
        var patient = getPatientPort.getByUserId(userId);
        return patient.isPresent();
    }
}
