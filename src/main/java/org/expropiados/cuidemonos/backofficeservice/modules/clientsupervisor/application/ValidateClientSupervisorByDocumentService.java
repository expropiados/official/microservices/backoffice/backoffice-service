package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.AllArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ValidateClientSupervisorByDocumentUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.ids.ApisNetIDsAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;

import java.io.IOException;

@UseCase
@AllArgsConstructor
public class ValidateClientSupervisorByDocumentService implements ValidateClientSupervisorByDocumentUseCase {

    private final GetUserPort getUserPort;
    private final GetClientSupervisorPort getClientSupervisorPort;

    @Override
    public ClientSupervisorChecked validateSpecialistByDocument(String dni) {
        UserApi userApi=null;
        boolean isClientSupervisor;
        var user = getUserPort.getUserByDocument(dni);
        if (user != null) {
            isClientSupervisor = checkIfUserIsClientSupervisor(user.getId());
        }else {
            isClientSupervisor = false;
            userApi = validateExistingUserWithApi(dni);
        }
        return ClientSupervisorChecked.builder()
                .isClientSupervisor(isClientSupervisor).user(user).userApi(userApi)
                .build();
    }

    private UserApi validateExistingUserWithApi(String dni){
        var apisNetIDsAdapter = new ApisNetIDsAdapter();
        try{
            return apisNetIDsAdapter.start(dni);
        }catch (IOException ex){
            return null;
        }
    }

    private boolean checkIfUserIsClientSupervisor(Long userId){
        var patient = getClientSupervisorPort.getClientSupervisorByUserId(userId);
        return patient.isPresent();
    }
}
