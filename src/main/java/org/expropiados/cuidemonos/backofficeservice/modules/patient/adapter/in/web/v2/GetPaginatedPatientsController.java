package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPaginatedPatientsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class GetPaginatedPatientsController {
    private final JwtHelper jwtHelper;
    private final GetPaginatedPatientsUseCase getPaginatedPatientsUseCase;

    @GetMapping(value = "patients")
    public Paginator<Patient> getPaginatedPatients(
            Pageable pageable,
            @RequestParam(value = "search", defaultValue = "") String search,
            @RequestHeader("Authorization") String token,
            @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        if (userRole != UserRole.CLIENT_SUPERVISOR) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        var clientCompanyId = tokenBody.getClientSupervisorCompanyId();
        return getPaginatedPatientsUseCase.getPaginatedPatients(filters, clientCompanyId);
    }
}
