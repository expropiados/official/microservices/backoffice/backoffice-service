package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.EditClientCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.EditClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;

@UseCase
@RequiredArgsConstructor
public class EditClientCompanyService implements EditClientCompanyUseCase {

    public final EditClientCompanyPort editClientCompanyPort;
    public final SaveImageProfilePort saveImageProfilePort;

    @Override
    public Company editClientCompany(ClientCompanyToEdit clientCompanyToEdit) {
        var imageFile = clientCompanyToEdit.getMultipartFile();
        if (imageFile != null) {
            var imageProfileUrl = saveImageProfilePort.saveImageProfile(imageFile);
            clientCompanyToEdit.setImageSourceFile(imageProfileUrl);
        }
        return editClientCompanyPort.editClientCompany(clientCompanyToEdit);
    }
}
