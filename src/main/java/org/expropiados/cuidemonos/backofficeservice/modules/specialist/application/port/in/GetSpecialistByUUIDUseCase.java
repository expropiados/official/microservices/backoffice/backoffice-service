package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

import java.util.UUID;

public interface GetSpecialistByUUIDUseCase {
    Specialist getSpecialistByUUID(UUID specialistUUID);
}
