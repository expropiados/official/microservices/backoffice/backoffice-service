package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

public interface GetPaginatedMedicalReasonsPort {
    Paginator<MedicalReason> getPaginatedMedicalReasons(Filters filters);
}
