package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

import java.util.Optional;

public interface GetMedicalReasonPort {
    Optional<MedicalReason> getMedicalReason(Long medicalReasonId);
}
