package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain;

import lombok.Data;

@Data
public class MedicalReason {
    Long id;
    String name;
    String description;
}
