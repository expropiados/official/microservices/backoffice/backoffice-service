package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;

public interface GetBulkLoadUseCase {
    BulkLoad getBulkLoad(Long bulkId);
}
