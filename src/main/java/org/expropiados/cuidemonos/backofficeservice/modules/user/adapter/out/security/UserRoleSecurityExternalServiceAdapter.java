package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.security;

import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.security.SecurityHostConfig;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.ExternalServiceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserRolePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

@ExternalServiceAdapter
@RequiredArgsConstructor
public class UserRoleSecurityExternalServiceAdapter implements RegisterUserRolePort {
    private final SecurityHostConfig securityHostConfig;
    private final Logger logger = LoggerFactory.getLogger(UserRoleSecurityExternalServiceAdapter.class);

    @Override
    public void registerUserRole(String email, UserRole userRole) {
        var microserviceHost = securityHostConfig.getHost();
        var gson = new GsonBuilder()
                .setLenient()
                .create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(SecurityMicroService.class);
        var body = new UserRoleRequestBody(email, userRole.name());

        try {
            service.registerUserRole(body).execute();
        } catch (IOException e) {
            logger.error("Cannot send the 'Register User Role' email to '{}' with role '{}'", email, userRole.name());
            logger.error(e.getMessage(), e);
        }
    }
}
