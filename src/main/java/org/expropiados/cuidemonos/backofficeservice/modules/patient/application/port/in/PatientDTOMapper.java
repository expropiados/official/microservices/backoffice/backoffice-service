package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface PatientDTOMapper {
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "typeDocument", target = "user.typeDocument")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "birthdate", target = "user.birthdate")
    @Mapping(source = "multipartFile", target = "multipartFile")
    PatientToRegister toPatientToRegister(PatientToRegisterDTO patientToRegisterDTO);

    @Mapping(source = "id", target = "patientId")
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "typeDocument", target = "user.typeDocument")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "birthdate", target = "user.birthdate")
    @Mapping(source = "multipartFile", target = "multipartFile")
    PatientToEdit toPatientToEdit(PatientToEditDTO patientToEditDTO);
}
