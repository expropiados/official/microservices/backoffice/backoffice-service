package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

public interface RegisterUserPort {
    User registerUser(UserToRegister userToRegister, UUID userUUID);
}
