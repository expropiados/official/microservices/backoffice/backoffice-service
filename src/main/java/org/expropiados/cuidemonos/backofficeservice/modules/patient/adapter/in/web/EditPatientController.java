package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.EditPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToEditDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.PatientNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/patients")
public class EditPatientController {
    private final JwtHelper jwtHelper;
    private final EditPatientUseCase editPatientUseCase;
    private final PatientDTOMapper patientDTOMapper;

    @PostMapping(value = "/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Patient editPatient(@ModelAttribute PatientToEditDTO patientToEditDTO,
                               @RequestHeader("Authorization") String token,
                               @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        try {
            var patientToEdit = patientDTOMapper.toPatientToEdit(patientToEditDTO);
            return editPatientUseCase.editPatient(patientToEdit);
        }catch(PatientNotFoundException ex){
            throw ex;
        }
    }
}
