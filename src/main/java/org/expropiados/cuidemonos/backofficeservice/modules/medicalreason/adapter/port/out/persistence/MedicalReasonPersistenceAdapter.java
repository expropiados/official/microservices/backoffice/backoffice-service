package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.*;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class MedicalReasonPersistenceAdapter implements
        RegisterMedicalReasonPort,
        GetPaginatedMedicalReasonsPort,
        GetMedicalReasonPort,
        UpdateMedicalReasonPort,
        CheckIfExistMedicalReasonPort,
        DeleteMedicalReasonPort {

    private final MedicalReasonJpaRepository repository;
    private final MedicalReasonMapper mapper;

    @Override
    public MedicalReason registerMedicalReason(MedicalReasonToRegister medicalReasonToRegister) {
        var row = mapper.toMedicalReasonJpaEntity(medicalReasonToRegister);
        var saved = repository.save(row);
        return mapper.toMedicalReason(saved);
    }

    @Override
    public Paginator<MedicalReason> getPaginatedMedicalReasons(Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = repository.searchMedicalReasons(pageable, filters.getSearch());

        var data = page.getContent()
                .stream()
                .map(mapper::toMedicalReason)
                .collect(Collectors.toList());

        return Paginator.<MedicalReason>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }

    @Override
    public Optional<MedicalReason> getMedicalReason(Long medicalReasonId) {
        return repository
                .findByIdAndDeletedIsFalse(medicalReasonId)
                .map(mapper::toMedicalReason);
    }

    @Override
    public MedicalReason updateMedicalReason(MedicalReason medicalReason) {
        var row = mapper.toMedicalReasonJpaEntity(medicalReason);
        repository.save(row);
        return medicalReason;
    }

    @Override
    public boolean checkIfExistMedicalReason(Long medicalReasonId) {
        return repository.existsByIdAndDeletedIsFalse(medicalReasonId);
    }

    @Override
    public void deleteMedicalReason(Long medicalReasonId) {
        var row = repository.getOne(medicalReasonId);
        row.setDeleted(true);
        repository.save(row);
    }
}
