package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.EditClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetPaginatedClientSupervisorsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.RegisterClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence.CompanyMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class ClientSupervisorPersistenceAdapter implements RegisterClientSupervisorPort, GetClientSupervisorPort, EditClientSupervisorPort, GetPaginatedClientSupervisorsPort {

    private final SpringJpaClientSupervisorRepository clientSupervisorRepository;
    private final ClientSupervisorMapper clientSupervisorMapper;
    private final UserMapper userMapper;
    private final CompanyMapper companyMapper;

    @Override
    public ClientSupervisor registerClientCompanySupervisor(
            ClientSupervisorToRegister clientSupervisorToRegister, User user, Company company, UUID uuid) {
        if (user == null) clientSupervisorToRegister.getUser().setUuid(UUID.randomUUID());
        var entity = clientSupervisorMapper.toClientSupervisorJpaEntity(clientSupervisorToRegister);
        if (user != null) entity.setUser(userMapper.toUserJpaEntity(user));
        entity.setClientCompany(companyMapper.toCompanyJpaEntity(company));
        entity.setUuid(uuid);
        entity.setIsBlocked(false);
        var result = clientSupervisorRepository.save(entity);
        return clientSupervisorMapper.toClientSupervisor(result);
    }

    @Override
    public Optional<ClientSupervisor> getClientSupervisorByUserId(Long idUser) {
        var clientSupervisorFound= clientSupervisorRepository.findByUserId(idUser);
        return clientSupervisorFound.map(clientSupervisorMapper::toClientSupervisor);
    }

    @Override
    public List<ClientSupervisor> getClientSupervisorsByCompanyId(Long companyId) {
        var clientSupervisors = clientSupervisorRepository.findAllByClientCompanyId(companyId);
        return clientSupervisorMapper.toClientSupervisors(clientSupervisors);
    }

    @Override
    public Optional<ClientSupervisor> getClientSupervisorByUUID(UUID clientSupervisorUUID) {
        var clientSupervisorFound= clientSupervisorRepository.findByUuid(clientSupervisorUUID);
        return clientSupervisorFound.map(clientSupervisorMapper::toClientSupervisor);
    }

    @Override
    public ClientSupervisor editClientSupervisor(ClientSupervisorToEdit clientSupervisorToEdit) {
        var clientSupervisor = clientSupervisorRepository.findById(clientSupervisorToEdit.getClientSupervisorId());
        if (clientSupervisor.isEmpty()) return null;
        var entity = clientSupervisor.get();
        var entityToEdit = clientSupervisorMapper.toClientSupervisorJpaEntity(clientSupervisorToEdit);
        entityToEdit.getUser().setUuid(entity.getUser().getUuid());
        entityToEdit.setUuid(entity.getUuid());
        entityToEdit.setClientCompany(entity.getClientCompany());
        var resultJpa = clientSupervisorRepository.save(entityToEdit);
        return clientSupervisorMapper.toClientSupervisor(resultJpa);
    }

    @Override
    public Paginator<ClientSupervisor> getPaginatedClientSupervisors(Filters filters, Long clientCompanyId) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = clientSupervisorRepository.searchClientSupervisors(pageable, filters.getSearch(), clientCompanyId);
        var data = page.getContent()
                .stream()
                .map(clientSupervisorMapper::toClientSupervisor)
                .collect(Collectors.toList());

        return Paginator.<ClientSupervisor>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }
}
