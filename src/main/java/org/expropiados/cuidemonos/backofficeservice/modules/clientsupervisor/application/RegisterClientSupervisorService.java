package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.RegisterUserException;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.RegisterClientSupervisorUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.RegisterClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserPort;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterClientSupervisorService implements RegisterClientSupervisorUseCase {
    public final SaveImageProfilePort saveImageProfilePort;
    public final RegisterUserPort registerUserPort;
    public final RegisterClientSupervisorPort registerClientSupervisorPort;
    public final GetClientCompanyPort getClientCompanyPort;
    private final GetUserPort getUserPort;
    private final ValidateClientSupervisorByDocumentService validateClientSupervisorByDocumentService;

    @Override
    public ClientSupervisor registerClientCompanySupervisor(ClientSupervisorToRegister clientSupervisorToRegister) {
        var imageFile = clientSupervisorToRegister.getMultipartFile();
        var result= validateClientSupervisorByDocumentService.validateSpecialistByDocument(clientSupervisorToRegister.getUser().getDocument());
        var user = getUserPort.getUserByDocument(clientSupervisorToRegister.getUser().getDocument());
        if (imageFile != null) {
            var imageProfileUrl = saveImageProfilePort.saveImageProfile(imageFile);
            clientSupervisorToRegister.setProfileImage(imageProfileUrl);
        }
        if (user == null) user = registerUserPort.registerUser(clientSupervisorToRegister.getUser(), UUID.randomUUID());
        var company = getClientCompanyPort.getClientCompanyById(clientSupervisorToRegister.getClientCompanyId());
        if (company.isEmpty()) return null;
        if(!result.getIsClientSupervisor())  return registerClientSupervisorPort.registerClientCompanySupervisor(
                clientSupervisorToRegister, user, company.get(), UUID.randomUUID());
        throw new RegisterUserException(user.getDocument()   ,"client supervisor");

    }
}
