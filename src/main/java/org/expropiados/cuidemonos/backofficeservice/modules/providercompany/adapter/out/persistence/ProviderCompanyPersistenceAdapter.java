package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.GetProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.UpdateProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.Optional;
import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class ProviderCompanyPersistenceAdapter implements UpdateProviderCompanyPort, GetProviderCompanyPort{
    private final SpringJpaProviderCompanyRepository providerCompanyRepository;
    private final ProviderCompanyMapper providerCompanyMapper;

    @Override
    public ProviderCompany updateProviderCompany(ProviderCompanyToUpdate providerCompany){
        ProviderCompanyJpaEntity entity = providerCompanyMapper.toProviderCompanyEntity(providerCompany);
        ProviderCompanyJpaEntity providerCompanyResponse = providerCompanyRepository.save(entity);
        return providerCompanyMapper.toProviderCompany(providerCompanyResponse);
    }

    @Override
    public Optional<ProviderCompany> getProviderCompany(UUID uuid){

        var entity = providerCompanyRepository.findFirstByUuid(uuid);

        return entity.map(providerCompanyMapper::toProviderCompany);
    }
}
