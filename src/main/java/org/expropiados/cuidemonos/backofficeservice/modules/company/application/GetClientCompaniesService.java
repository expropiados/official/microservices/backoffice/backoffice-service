package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompaniesUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetClientCompaniesService implements GetClientCompaniesUseCase {

    public final GetClientCompanyPort getClientCompanyPort;

    @Override
    public List<Company> getClientCompanies() {
        return getClientCompanyPort.getClientCompanies();
    }
}
