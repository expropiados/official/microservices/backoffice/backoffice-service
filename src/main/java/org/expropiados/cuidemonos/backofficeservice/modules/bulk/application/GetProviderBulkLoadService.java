package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetProviderBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetProviderBulkLoadPort;


@UseCase
@RequiredArgsConstructor
public class GetProviderBulkLoadService implements GetProviderBulkLoadUseCase {

    private final GetProviderBulkLoadPort getBulkLoadPort;

    @Override
    public BulkLoadWithResponsible getProviderBulkLoad(Long bulkId) {
        return getBulkLoadPort.getProviderBulkLoad(bulkId)
                .orElseThrow(() -> new BulkLoadNotFoundException(bulkId));
    }
}
