package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.BadDateRangeException;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyReport;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetReportDateRangeDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMedicalReasonMonthlyReportUseCase;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("/reports")
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyReportController {
    private final JwtHelper jwtHelper;
    private final GetMedicalReasonMonthlyReportUseCase getMedicalReasonMonthlyReportUseCase;

    @GetMapping("/pre-poll/monthly")
    public GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(@RequestHeader("Authorization") String token,
                                                                       @RequestHeader("role") Integer role,
                                                                       GetReportDateRangeDTO getReportDateRangeDTO) throws IOException {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.PROVIDER_SUPERVISOR) throw new AuthenticationException(userRole.name());
        if (getReportDateRangeDTO.getInitDate().isAfter(getReportDateRangeDTO.getEndDate()))
            throw new BadDateRangeException(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate());
        return getMedicalReasonMonthlyReportUseCase.getMedicalReasonMonthly(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate());
    }
}
