package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParameters;

import java.util.Optional;
import java.util.UUID;

public interface GetClientCompanyParametersPort {
    Optional<ClientCompanyParameters> getParametersFromClientCompany(Long clientCompanyId);
}
