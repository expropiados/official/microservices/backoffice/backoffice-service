package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetPromScoreReportPort;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreReport;

import java.io.IOException;

@UseCase
@RequiredArgsConstructor
public class GetPromScoreReportService implements GetPromScoreReportUseCase {

    public final GetPromScoreReportPort getPromScoreReportPort;

    @Override
    public GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO) throws IOException {
        return getPromScoreReportPort.getPromPostPollReport(getPromScoreReportDTO);
    }
}
