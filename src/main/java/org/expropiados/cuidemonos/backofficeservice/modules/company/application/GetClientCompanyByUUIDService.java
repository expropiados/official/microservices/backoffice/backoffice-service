package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompanyByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetClientCompanyByUUIDService implements GetClientCompanyByUUIDUseCase {

    public final GetClientCompanyPort getClientCompanyPort;

    @Override
    public Company getClientCompanyByUUID(UUID uuid) {
        var result = getClientCompanyPort.getClientCompanyByUUID(uuid);
        if (result.isEmpty()) return null;
        return result.get();
    }
}
