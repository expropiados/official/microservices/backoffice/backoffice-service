package org.expropiados.cuidemonos.backofficeservice.modules.patient.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class Patient {
    private Long id;
    private User user;
    private String email;
    private String phoneNumber;
    private String address;
    private String profileImage;
    private Boolean isBlocked;
    private Long clientCompanyId;
    private UUID uuid;
}
