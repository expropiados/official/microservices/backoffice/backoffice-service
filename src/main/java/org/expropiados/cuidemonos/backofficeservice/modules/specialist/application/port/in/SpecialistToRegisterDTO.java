package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class SpecialistToRegisterDTO {

    @NotBlank
    @Size(min=8, max=12)
    private String document;

    @NotBlank
    TypeDocument typeDocument;

    @NotBlank
    private String name;

    @NotBlank
    private String fatherLastname;

    @NotBlank
    private String motherLastname;

    @Email(message = "invalid email")
    private String email;

    private String phoneNumber;

    private String address;

    private String profileImage;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private LocalDate birthdate;

    private String specialty;

    private MultipartFile multipartFile;

}
