package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistExtraInfo;

import java.util.Optional;
import java.util.UUID;

public interface GetSpecialistExtraInfoForSpecialistPort {
    Optional<SpecialistExtraInfo> getSpecialistExtraInfoForSpecialist(UUID specialistUserUuid);
}
