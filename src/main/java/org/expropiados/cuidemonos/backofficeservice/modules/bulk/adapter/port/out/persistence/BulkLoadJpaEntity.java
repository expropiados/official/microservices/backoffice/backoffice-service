package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@ToString
@Getter @Setter
@RequiredArgsConstructor
@Entity @Table(name = "BULK_LOAD")
public class BulkLoadJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bulk_load_id")
    private Long id;

    @Column(name = "state")
    private Integer state;

    @Column(name = "type")
    private Integer type;

    @Column(name = "requested_at")
    private LocalDateTime requestedAt;

    @Column(name = "filename", length = 100)
    private String filename;

    @Column(name = "source_file_key", length = 300)
    private String sourceFileKey;

    @Column(name = "error_source_file_key", length = 300)
    private String errorSourceFileKey;

    @Column(name = "successful_registered")
    private Long successfulRegistered = 0L;

    @Column(name = "failed_registered")
    private Long failedRegistered = 0L;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BulkLoadJpaEntity that = (BulkLoadJpaEntity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 626757501;
    }
}
