package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkType;

import java.io.File;

public interface RegisterBulkLoadUseCase {
    Long registerBulkLoad(File file, String originalFilename, BulkType bulkType);
}
