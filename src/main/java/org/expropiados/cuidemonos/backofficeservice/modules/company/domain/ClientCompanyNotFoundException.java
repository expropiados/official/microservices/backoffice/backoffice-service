package org.expropiados.cuidemonos.backofficeservice.modules.company.domain;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

import java.util.UUID;

@Getter
public class ClientCompanyNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_CLC_001";
    private final String message;
    private final transient Object data;


    @Value
    static class Data {
        Long companyUUID;
    }

    public ClientCompanyNotFoundException(Long companyId) {
        super();
        this.message = String.format("Client Company %d not found", companyId);
        this.data = new Data(companyId);
    }
}
