package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetAllClientBulkLoadsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetClientBulkLoadPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterClientBulkRequestPort;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class ClientBulkRequestPersistenceAdapter implements RegisterClientBulkRequestPort, GetAllClientBulkLoadsPort, GetClientBulkLoadPort {

    private final ClientBulkRequestJpaRepository repository;
    private final ClientBulkRequestMapper mapper;

    @Override
    public void registerClientBulkRequest(Long clientSupervisorId, Long bulkId) {
        var requestRow = new ClientBulkRequestJpaEntity();
        requestRow.setBulkLoadId(bulkId);
        requestRow.setClientSupervisorId(clientSupervisorId);

        repository.save(requestRow);
    }

    @Override
    public Paginator<BulkLoadWithResponsible> getAllClientBulkLoads(Long clientCompanyId, Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = repository.findAllByCompanyIdOrderByBulkLoadRequestedAt(pageable, clientCompanyId);

        var content = page.getContent()
                .stream()
                .map(mapper::toBulkLoadWithResponsible)
                .collect(Collectors.toList());

        return Paginator.<BulkLoadWithResponsible>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(content)
                .build();
    }

    @Override
    public Optional<BulkLoadWithResponsible> getClientBulkLoad(Long bulkId, Long clientCompanyId) {
        return repository
                .findByBulkIdAndCompanyId(bulkId, clientCompanyId)
                .map(mapper::toBulkLoadWithResponsible);
    }
}
