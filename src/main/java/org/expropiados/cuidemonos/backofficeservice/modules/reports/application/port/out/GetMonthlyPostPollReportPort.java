package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMonthlyPostPollReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

public interface GetMonthlyPostPollReportPort {
    GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) throws IOException;
}
