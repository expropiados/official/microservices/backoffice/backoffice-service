package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out;

import org.springframework.web.multipart.MultipartFile;

public interface SaveImageProfilePort {
    String saveImageProfile(MultipartFile file);
}
