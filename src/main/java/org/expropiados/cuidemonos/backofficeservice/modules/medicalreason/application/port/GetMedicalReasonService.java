package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.GetMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonService implements GetMedicalReasonUseCase {
    private final GetMedicalReasonPort getMedicalReasonPort;

    @Override
    public MedicalReason getMedicalReason(Long medicalReasonId) {
        return getMedicalReasonPort
                .getMedicalReason(medicalReasonId)
                .orElseThrow(() -> new MedicalReasonNotFoundException(medicalReasonId));
    }
}
