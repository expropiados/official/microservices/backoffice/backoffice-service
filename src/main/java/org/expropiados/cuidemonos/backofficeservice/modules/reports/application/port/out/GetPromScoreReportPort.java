package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreReport;

import java.io.IOException;

public interface GetPromScoreReportPort {
    GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO) throws IOException;
}
