package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MedicalReasonMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "deleted", ignore = true)
    MedicalReasonJpaEntity toMedicalReasonJpaEntity(MedicalReasonToRegister medicalReasonToRegister);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "deleted", ignore = true)
    MedicalReasonJpaEntity toMedicalReasonJpaEntity(MedicalReason medicalReason);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    MedicalReason toMedicalReason(MedicalReasonJpaEntity medicalReasonJpaEntity);
}
