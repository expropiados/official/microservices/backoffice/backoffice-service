package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParameters;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface CompanyMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(source = "name", target = "name")
    @Mapping(source = "ruc", target = "ruc")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "imageSourceFile", target = "imageSourceFile")
    @Mapping(target = "appointmentDuration", ignore = true)
    @Mapping(target = "appointmentMaxCancelled", ignore = true)
    @Mapping(target = "appointmentMaxCant", ignore = true)
    @Mapping(target = "appointmentTimeToCancel", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    CompanyJpaEntity toCompanyEntity(ClientCompanyToRegister clientCompanyToRegister);


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "ruc", target = "ruc")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "imageSourceFile", target = "imageSourceFile")
    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "uuid", target = "uuid")
    Company toCompany(CompanyJpaEntity companyJpaEntity);
    List<Company> toClientCompanies(List<CompanyJpaEntity> companyJpaEntities);

    @InheritInverseConfiguration
    @Mapping(target = "appointmentDuration", ignore = true)
    @Mapping(target = "appointmentMaxCancelled", ignore = true)
    @Mapping(target = "appointmentMaxCant", ignore = true)
    @Mapping(target = "appointmentTimeToCancel", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    CompanyJpaEntity toCompanyJpaEntity(Company company);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "ruc", target = "ruc")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "imageSourceFile", target = "imageSourceFile")
    @Mapping(target = "appointmentDuration", ignore = true)
    @Mapping(target = "appointmentMaxCancelled", ignore = true)
    @Mapping(target = "appointmentMaxCant", ignore = true)
    @Mapping(target = "appointmentTimeToCancel", ignore = true)
    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    CompanyJpaEntity toCompanyEntity(ClientCompanyToEdit clientCompanyToEdit);

    @Mapping(source = "appointmentDuration", target = "appointmentDuration")
    @Mapping(source = "appointmentMaxCancelled", target = "appointmentMaxCancelled")
    @Mapping(source = "appointmentMaxCant", target = "appointmentMaxCant")
    @Mapping(source = "appointmentTimeToCancel", target = "appointmentTimeToCancel")
    ClientCompanyParameters toClientCompanyParameters(CompanyJpaEntity providerCompanyJpaEntity);
}
