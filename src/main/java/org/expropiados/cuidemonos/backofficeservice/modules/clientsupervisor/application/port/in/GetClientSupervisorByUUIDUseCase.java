package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

import java.util.UUID;

public interface GetClientSupervisorByUUIDUseCase {
    ClientSupervisor getClientSupervisorByUUID(UUID clientSupervisorUUID);
}
