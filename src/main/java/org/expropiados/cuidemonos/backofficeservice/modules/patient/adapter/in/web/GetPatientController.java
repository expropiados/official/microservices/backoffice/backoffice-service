package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetPatientController {
    private final JwtHelper jwtHelper;
    private final GetPatientUseCase getPatientUseCase;

    @GetMapping(value = "/patients")
    public List<Patient> getPatients(@RequestHeader("Authorization") String token,
                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getPatientUseCase.getPatients();
    }
}
