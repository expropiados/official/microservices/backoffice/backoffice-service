package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

public interface RegisterClientSupervisorPort {
    ClientSupervisor registerClientCompanySupervisor(
            ClientSupervisorToRegister clientSupervisorToRegister, User user,
            Company company, UUID uuid);
}
