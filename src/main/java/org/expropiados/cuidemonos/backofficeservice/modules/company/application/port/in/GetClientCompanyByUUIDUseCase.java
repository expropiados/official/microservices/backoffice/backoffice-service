package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.UUID;

public interface GetClientCompanyByUUIDUseCase {
    Company getClientCompanyByUUID(UUID uuid);
}
