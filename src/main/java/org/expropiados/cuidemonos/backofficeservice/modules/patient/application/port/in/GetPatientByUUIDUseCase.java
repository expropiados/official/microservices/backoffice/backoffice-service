package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

import java.util.UUID;

public interface GetPatientByUUIDUseCase {
    Patient getPatientByUUID(UUID patientUUID);
}
