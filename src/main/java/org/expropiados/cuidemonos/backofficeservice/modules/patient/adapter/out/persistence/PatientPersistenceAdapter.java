package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import io.jsonwebtoken.lang.Collections;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.EditPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPaginatedPatientsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.RegisterPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.PatientNotFoundException;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.GetPatientPort;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class PatientPersistenceAdapter implements RegisterPatientPort, GetPatientPort, EditPatientPort, GetPaginatedPatientsPort {

    private final SpringJpaPatientRepository patientRepository;
    private final PatientMapper patientMapper;
    private final UserMapper userMapper;

    @Override
    public Patient registerPatient(PatientToRegister userToRegister, User user, UUID uuid, Long clientCompanyId) {
        if (user == null) userToRegister.getUser().setUuid(UUID.randomUUID());
        var entity = patientMapper.toPatientJpaEntity(userToRegister);
        if (user != null) entity.setUser(userMapper.toUserJpaEntity(user));
        entity.setUuid(uuid);
        entity.setClientCompanyId(clientCompanyId);
        entity.setIsBlocked(false);
        var result = patientRepository.save(entity);
        return patientMapper.toPatient(result);
    }

    @Override
    public List<Patient> getPatients() {
        var result = patientRepository.findAll();
        return patientMapper.toPatients(result);
    }

    @Override
    public Optional<Patient> getByPatientId(Long patientId) {
        var result = patientRepository.findById(patientId);
        return result.map(patientMapper::toPatient);
    }

    public Optional<Patient> getByUserId(Long userId) {
        var result = patientRepository.findByUserId(userId);
        return result.map(patientMapper::toPatient);
    }

    @Override
    public Optional<Patient> getByDNI(String dni) {
        var result = patientRepository.findByUser_Document(dni);
        return result.map(patientMapper::toPatient);
    }

    @Override
    public Optional<Patient> getByPatientUUID(UUID patientUUID) {
        var result = patientRepository.findByUuid(patientUUID);
        return result.map(patientMapper::toPatient);
    }

    @Override
    public Patient editPatient(PatientToEdit patientToEdit, Patient patient) {
        var entity = patientMapper.toPatientJpaEntity(patientToEdit);
        entity.getUser().setId(patient.getUser().getId());
        entity.getUser().setUuid(patient.getUser().getUuid());
        entity.setClientCompanyId(patient.getClientCompanyId());
        entity.setIsBlocked(patient.getIsBlocked());
        entity.setUuid(patient.getUuid());
        var result = patientRepository.save(entity);
        return patientMapper.toPatient(result);
    }

    @Override
    public Paginator<Patient> getPaginatedPatients(Filters filters, Long clientCompanyId) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = patientRepository.searchPatients(pageable, filters.getSearch(), clientCompanyId);
        var data = page.getContent()
                .stream()
                .map(patientMapper::toPatient)
                .collect(Collectors.toList());

        return Paginator.<Patient>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }
}
