package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.SpecialistChecked;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.ValidateSpecialistByDocumentUseCase;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ValidateSpecialistByDocumentController {

    private final JwtHelper jwtHelper;
    private final ValidateSpecialistByDocumentUseCase validateSpecialistByDocumentUseCase;

    @GetMapping("/specialists/checking")
    public SpecialistChecked validateSpecialistByDNI(@RequestParam(name = "document", required = false)String document,
                                                     @RequestHeader("Authorization") String token,
                                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return validateSpecialistByDocumentUseCase.validateSpecialistByDocument(document);
    }
}
