package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.adapter.port.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MedicalReasonJpaRepository extends JpaRepository<MedicalReasonJpaEntity, Long> {
    @Query("SELECT MR FROM MedicalReasonJpaEntity MR WHERE (MR.deleted = false) AND (UPPER(MR.name) LIKE CONCAT('%', UPPER(:search), '%') OR MR.description LIKE CONCAT('%', UPPER(:search), '%')) ORDER BY MR.id")
    Page<MedicalReasonJpaEntity> searchMedicalReasons(Pageable pageable, @Param("search") String search);

    Optional<MedicalReasonJpaEntity> findByIdAndDeletedIsFalse(Long medicalReasonId);

    Boolean existsByIdAndDeletedIsFalse(Long medicalReasonId);
}
