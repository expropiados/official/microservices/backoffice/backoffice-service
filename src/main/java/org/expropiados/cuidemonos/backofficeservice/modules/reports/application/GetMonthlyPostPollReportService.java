package org.expropiados.cuidemonos.backofficeservice.modules.reports.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetMonthlyPostPollReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.GetMonthlyPostPollReportPort;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMonthlyPostPollReport;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetMonthlyPostPollReportService implements GetMonthlyPostPollReportUseCase {

    private final GetMonthlyPostPollReportPort getMonthlyPostPollReportPort;

    @Override
    public GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID)  throws IOException {
        return getMonthlyPostPollReportPort.getMonthlyPostPollReport(startDate, endDate, specialistUUID);
    }
}
