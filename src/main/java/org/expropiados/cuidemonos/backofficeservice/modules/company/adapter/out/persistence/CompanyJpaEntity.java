package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "CLIENT_COMPANY")
public class CompanyJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_company_id")
    private Long id;

    @Column(name = "business_name", length = 150, nullable = false)
    private String name;

    @Column(name = "ruc", length = 11, nullable = false, unique = true)
    private String ruc;

    @Column(name = "address", length = 150)
    private String address;

    @Column(name = "phone_number", length = 9)
    private String phone;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name = "image_source_file")
    private String imageSourceFile;

    @Column(name = "uuid", nullable = false, unique = true)
    private UUID uuid;

    @Column(name = "appointment_duration")
    private Integer appointmentDuration;

    @Column(name = "appointment_max_cant")
    private Integer appointmentMaxCant;

    @Column(name = "appointment_max_cancelled")
    private Integer appointmentMaxCancelled;

    @Column(name = "appointment_time_to_cancel")
    private Integer appointmentTimeToCancel;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "updated_at", insertable = false)
    private LocalDateTime updatedAt;
}
