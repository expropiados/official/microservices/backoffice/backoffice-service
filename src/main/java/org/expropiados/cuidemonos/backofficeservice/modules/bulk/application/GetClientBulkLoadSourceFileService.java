package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetClientBulkLoadSourceFileUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetClientBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetUrlBulkLoadFilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkSourceFileType;

import java.net.URL;


@UseCase
@RequiredArgsConstructor
public class GetClientBulkLoadSourceFileService implements GetClientBulkLoadSourceFileUseCase {

    private final GetClientBulkLoadUseCase getClientBulkLoadUseCase;
    private final GetUrlBulkLoadFilePort getUrlBulkLoadFilePort;

    @Override
    public URL getClientBulkLoadSourceFile(Long bulkId, Long clientCompanyId, BulkSourceFileType bulkSourceFileType) {
        var bulkLoad = getClientBulkLoadUseCase
                .getClientBulkLoad(bulkId, clientCompanyId)
                .getBulkLoad();

        var fileKey = bulkSourceFileType == BulkSourceFileType.ORIGINAL ?
                bulkLoad.getSourceFileKey() : bulkLoad.getErrorSourceFileKey();

        return getUrlBulkLoadFilePort.getUrlBulkLoadFile(fileKey);
    }
}
