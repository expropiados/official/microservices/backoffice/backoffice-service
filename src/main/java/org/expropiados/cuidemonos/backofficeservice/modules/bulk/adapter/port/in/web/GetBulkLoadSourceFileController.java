package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetClientBulkLoadSourceFileUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetProviderBulkLoadSourceFileUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkSourceFileType;
import org.springframework.web.bind.annotation.*;

import java.net.URL;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetBulkLoadSourceFileController {

    private final GetProviderBulkLoadSourceFileUseCase providerBulkLoadSourceFileUseCase;
    private final GetClientBulkLoadSourceFileUseCase clientBulkLoadSourceFileUseCase;
    private final JwtHelper jwtHelper;

    @GetMapping("/bulk-loads/{bulkId}/files")
    public URL getBulkLoad(
            @PathVariable Long bulkId,
            @RequestParam(value = "type", defaultValue = "ORIGINAL") BulkSourceFileType bulkSourceFileType,
            @RequestHeader("Authorization") String token,
            @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole == UserRole.CLIENT_SUPERVISOR) {
            var companyId = tokenBody.getClientSupervisorCompanyId();
            return clientBulkLoadSourceFileUseCase.getClientBulkLoadSourceFile(bulkId, companyId, bulkSourceFileType);
        }

        if (userRole == UserRole.PROVIDER_SUPERVISOR) {
            return providerBulkLoadSourceFileUseCase.getProviderBulkLoadSourceFile(bulkId, bulkSourceFileType);
        }

        throw new NotAuthorizedForRoleException(userRole);
    }
}
