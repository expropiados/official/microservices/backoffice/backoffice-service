package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.application.port.out.GetPaginatedProviderSupervisorsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain.ProviderSupervisor;
import org.springframework.data.domain.PageRequest;

import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class ProviderSupervisorPersistenceAdapter implements GetPaginatedProviderSupervisorsPort {

    private final ProviderSupervisorMapper providerSupervisorMapper;
    private final ProviderSupervisorJpaRepository providerSupervisorJpaRepository;

    @Override
    public Paginator<ProviderSupervisor> getPaginatedProviderSupervisors(Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = providerSupervisorJpaRepository.searchProviderSupervisors(pageable, filters.getSearch());
        var data = page.getContent()
                .stream()
                .map(providerSupervisorMapper::toProviderSupervisor)
                .collect(Collectors.toList());

        return Paginator.<ProviderSupervisor>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(data)
                .build();
    }
}
