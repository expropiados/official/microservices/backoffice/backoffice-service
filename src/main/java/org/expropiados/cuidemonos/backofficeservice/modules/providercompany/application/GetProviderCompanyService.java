package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.GetProviderCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.GetProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.Optional;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetProviderCompanyService implements GetProviderCompanyUseCase {

    public final GetProviderCompanyPort getProviderCompanyPort;

    @Override
    public Optional<ProviderCompany> getProviderCompany(UUID uuid) {
        return getProviderCompanyPort.getProviderCompany(uuid);

    }
}
