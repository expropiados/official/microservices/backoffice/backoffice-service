package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetClientSupervisorByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/companies/supervisor")
public class GetClientSupervisorByUUIDController {
    private final JwtHelper jwtHelper;
    private final GetClientSupervisorByUUIDUseCase getClientSupervisorByUUIDUseCase;

    @GetMapping(value = "/")
    public ClientSupervisor getClientSupervisorByUUID(@RequestParam UUID clientSupervisorUUID,
                                             @RequestHeader("Authorization") String token,
                                             @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getClientSupervisorByUUIDUseCase.getClientSupervisorByUUID(clientSupervisorUUID);
    }
}
