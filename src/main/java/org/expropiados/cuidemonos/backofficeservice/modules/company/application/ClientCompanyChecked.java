package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.CompanyApi;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

@Getter
@AllArgsConstructor
public class ClientCompanyChecked {
    private final Boolean isRegistered;
    private final CompanyApi companyApi;
    private final Company clientCompany;

}
