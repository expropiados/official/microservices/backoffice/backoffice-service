package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.out.persistence;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "PROVIDER_COMPANY")
public class ProviderCompanyJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ruc", length = 15, nullable = false)
    private String ruc;

    @Column(name = "uuid")
    private UUID uuid;

    @Column(name = "business_name", length = 150, nullable = false)
    private String businessName;

    @Column(name = "email", length = 150)
    private String email;

    @Column(name = "responsible_email", length = 150)
    private String responsibleEmail;

    @Column(name = "responsible_name", length = 150)
    private String responsibleName;

    @Column(name = "address", length = 150)
    private String address;

    @Column(name = "phone_number", length = 15)
    private String phoneNumber;

    @Column(name = "image_source_file", length=300)
    private String imageUrl;
}
