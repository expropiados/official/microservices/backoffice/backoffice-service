package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.BlockSpecialistUseCase;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/specialists")
public class BlockSpecialistController {

    private final JwtHelper jwtHelper;
    public final BlockSpecialistUseCase blockSpecialistUseCase;

    @PostMapping(value = "/block")
    public Long blockSpecialist(@RequestParam UUID specialistUUID,
                                @RequestHeader("Authorization") String token,
                                @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return blockSpecialistUseCase.blockSpecialist(specialistUUID);
    }

}
