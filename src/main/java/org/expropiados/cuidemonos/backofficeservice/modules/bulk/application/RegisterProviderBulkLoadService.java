package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterProviderBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterProviderBulkRequestPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkType;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterProviderBulkLoadService implements RegisterProviderBulkLoadUseCase {

    private final RegisterBulkLoadUseCase registerBulkLoadUseCase;
    private final RegisterProviderBulkRequestPort registerProviderBulkRequestPort;

    @Override
    public void registerBulkLoad(File file, String originalFilename, Long providerSupervisorId) {
        var bulkType = checkForBulkType(file);
        var bulkId = registerBulkLoadUseCase.registerBulkLoad(file, originalFilename, bulkType);

        registerProviderBulkRequestPort.registerProviderBulkRequest(providerSupervisorId, bulkId);
    }

    private BulkType checkForBulkType(File file) {
        return BulkType.SPECIALISTS;
    }
}
