package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ClientSupervisor {
    private Long id;
    private User user;
    private String email;
    private String phoneNumber;
    private String profileImage;
    private Boolean isBlocked;
    private UUID uuid;
    private Company clientCompany;
}
