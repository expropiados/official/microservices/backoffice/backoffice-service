package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.RegisterUserRoleUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserRolePort;

@UseCase
@RequiredArgsConstructor
public class RegisterUserRoleService implements RegisterUserRoleUseCase {
    private final RegisterUserRolePort registerUserRolePort;

    @Override
    public void registerUserRole(String email, UserRole userRole) {
        registerUserRolePort.registerUserRole(email, userRole);
    }
}
