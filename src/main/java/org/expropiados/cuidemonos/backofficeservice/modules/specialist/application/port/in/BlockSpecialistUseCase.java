package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import java.util.UUID;

public interface BlockSpecialistUseCase {
    Long blockSpecialist(UUID specialistUUID);
}
