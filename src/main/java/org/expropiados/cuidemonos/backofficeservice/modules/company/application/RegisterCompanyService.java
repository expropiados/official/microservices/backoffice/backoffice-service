package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.RegisterCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.RegisterCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.SaveClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterCompanyService implements RegisterCompanyUseCase {

    public final RegisterCompanyPort registerCompanyPort;
    public final SaveClientCompanyPort saveClientCompanyPort;

    @Override
    public Company registerCompany(ClientCompanyToRegister company) {
        var imageFile = company.getMultipartFile();
        if(company.getMultipartFile() != null) {
            var imageSourceURL =saveClientCompanyPort.saveClientCompanyImageFile(imageFile);
            company.setImageSourceFile(imageSourceURL);
        }
        return registerCompanyPort.registerCompany(company, UUID.randomUUID());
    }
}
