package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;


@Getter
@AllArgsConstructor
public class SpecialistChecked {
    private final Boolean isSpecialist;
    private final UserApi userApi;
    private final User user;
}
