package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

public interface EditSpecialistUseCase {
    Specialist editSpecialist(SpecialistToEdit specialistToEdit);
}
