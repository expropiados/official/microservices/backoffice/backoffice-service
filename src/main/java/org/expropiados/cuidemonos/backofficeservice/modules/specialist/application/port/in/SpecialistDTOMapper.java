package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SpecialistDTOMapper {
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "typeDocument", target = "user.typeDocument")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "address", target = "address")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "birthdate", target = "user.birthdate")
    @Mapping(source = "multipartFile", target = "multipartFile")
    @Mapping(source = "specialty", target = "specialty")
    @Mapping(target = "aboutMe", ignore = true)
    @Mapping(target = "degree", ignore = true)
    SpecialistToRegister toSpecialistToRegister(SpecialistToRegisterDTO specialistToRegisterDTO);

    @Mapping(source = "id", target = "specialistId")
    @Mapping(source = "document", target = "user.document")
    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "fatherLastname", target = "user.fatherLastname")
    @Mapping(source = "motherLastname", target = "user.motherLastname")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "typeDocument", target = "user.typeDocument")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(source = "birthdate", target = "user.birthdate")
    @Mapping(source = "multipartFile", target = "multipartFile")
    @Mapping(target = "aboutMe", ignore = true)
    @Mapping(target = "degree", ignore = true)
    SpecialistToEdit toSpecialistToEdit(SpecialistToEditDTO specialistToEditDTO);
}
