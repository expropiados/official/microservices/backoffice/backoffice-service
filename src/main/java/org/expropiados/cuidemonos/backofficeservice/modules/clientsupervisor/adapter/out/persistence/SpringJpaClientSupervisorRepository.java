package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SpringJpaClientSupervisorRepository extends JpaRepository<ClientSupervisorJpaEntity, Long> {
    Optional<ClientSupervisorJpaEntity> findByUserId(Long idUser);
    Optional<ClientSupervisorJpaEntity> findByUuid(UUID uuid);
    List<ClientSupervisorJpaEntity> findAllByClientCompanyId(Long clientCompanyId);

    @Query("SELECT CS FROM ClientSupervisorJpaEntity CS WHERE CS.clientCompanyId = :companyId AND (UPPER(CS.user.document) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(CS.user.name) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(CS.user.fatherLastname) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(CS.user.motherLastname) LIKE CONCAT('%', UPPER(:search), '%')) ORDER BY CS.user.id")
    Page<ClientSupervisorJpaEntity> searchClientSupervisors(Pageable pageable, @Param("search") String search, @Param("companyId") Long companyId);
}
