package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetHoursReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetReportDateRangeSpecialistDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.BadDateRangeException;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetHoursReport;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("/reports")
@RequiredArgsConstructor
public class GetHoursReportController {
    private final JwtHelper jwtHelper;
    private final GetHoursReportUseCase getHoursReportUseCase;

    @GetMapping("/availabilities/hours")
    public GetHoursReport getHoursReport(@RequestHeader("Authorization") String token,
                                         @RequestHeader("role") Integer role,
                                         GetReportDateRangeSpecialistDTO getReportDateRangeDTO) throws IOException {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.PROVIDER_SUPERVISOR) throw new AuthenticationException(userRole.name());
        if (getReportDateRangeDTO.getInitDate().isAfter(getReportDateRangeDTO.getEndDate()))
            throw new BadDateRangeException(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate());

        return getHoursReportUseCase.getHoursReport(getReportDateRangeDTO.getInitDate(), getReportDateRangeDTO.getEndDate(), getReportDateRangeDTO.getSpecialistUUID());
    }
}
