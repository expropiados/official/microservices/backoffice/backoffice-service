package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkState;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BulkStateMapper {

    default BulkState toBulkState(Integer value) {
        switch (value) {
            case 0:
                return BulkState.PENDING;
            case 1:
                return BulkState.PROGRESS;
            case 2:
                return BulkState.FINISHED;
            case 3:
                return BulkState.FAILED;
            default:
                return null;
        }
    }
}
