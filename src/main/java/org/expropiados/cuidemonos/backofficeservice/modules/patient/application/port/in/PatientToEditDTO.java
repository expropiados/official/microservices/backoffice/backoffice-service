package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class PatientToEditDTO {

    @NotNull
    private Long id;

    @Size(min=8, max=12)
    String document;

    @NotBlank
    TypeDocument typeDocument;

    private String name;

    private String fatherLastname;

    private String motherLastname;

    @Email(message = "invalid email")
    private String email;

    private String phoneNumber;

    private String profileImage;

    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    private MultipartFile multipartFile;
}
