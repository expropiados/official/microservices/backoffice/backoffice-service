package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

public interface ValidatePatientByDocumentUseCase {
    PatientChecked validatePatientByDocument(String document);
}
