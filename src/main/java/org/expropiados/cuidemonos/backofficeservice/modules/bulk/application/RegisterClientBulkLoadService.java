package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterClientBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterClientBulkRequestPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkType;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterClientBulkLoadService implements RegisterClientBulkLoadUseCase {

    private final RegisterBulkLoadUseCase registerBulkLoadUseCase;
    private final RegisterClientBulkRequestPort registerClientBulkRequestPort;

    @Override
    public void registerClientBulkLoad(File file, String originalFilename, Long clientSupervisorId) {
        var bulkType = checkForBulkType(file);
        var bulkId = registerBulkLoadUseCase.registerBulkLoad(file, originalFilename, bulkType);

        registerClientBulkRequestPort.registerClientBulkRequest(clientSupervisorId, bulkId);
    }

    private BulkType checkForBulkType(File file) {
        return BulkType.PATIENTS;
    }
}
