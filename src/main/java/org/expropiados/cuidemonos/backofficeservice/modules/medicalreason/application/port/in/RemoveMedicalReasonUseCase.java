package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in;

public interface RemoveMedicalReasonUseCase {
    void removeMedicalReason(Long medicalReasonId);
}
