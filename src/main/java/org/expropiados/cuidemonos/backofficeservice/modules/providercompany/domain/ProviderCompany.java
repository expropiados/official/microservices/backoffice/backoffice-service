package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class ProviderCompany {
    private Long id;
    private UUID uuid;
    private String ruc;
    private String businessName;
    private String email;
    private String responsibleEmail;
    private String responsibleName;
    private String address;
    private String phoneNumber;
    private String imageUrl;
}
