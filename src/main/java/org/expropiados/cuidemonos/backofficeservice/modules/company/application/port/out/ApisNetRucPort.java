package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.CompanyApi;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApisNetRucPort {

    @GET("ruc")
    Call<CompanyApi> getUserInformation(@Query("numero") String numero);
}
