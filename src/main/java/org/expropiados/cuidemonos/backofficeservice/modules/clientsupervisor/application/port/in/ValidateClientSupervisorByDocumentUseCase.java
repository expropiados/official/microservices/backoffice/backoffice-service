package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.ClientSupervisorChecked;

public interface ValidateClientSupervisorByDocumentUseCase {

    ClientSupervisorChecked validateSpecialistByDocument(String dni);
}
