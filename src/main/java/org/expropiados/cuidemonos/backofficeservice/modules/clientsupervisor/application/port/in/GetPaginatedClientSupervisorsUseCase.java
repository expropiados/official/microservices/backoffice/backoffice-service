package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

public interface GetPaginatedClientSupervisorsUseCase {
    Paginator<ClientSupervisor> getPaginatedClientSupervisors(Filters filters, Long clientCompanyId);
}
