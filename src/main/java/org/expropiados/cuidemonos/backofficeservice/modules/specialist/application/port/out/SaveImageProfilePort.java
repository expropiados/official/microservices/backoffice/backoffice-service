package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;

import org.springframework.web.multipart.MultipartFile;

public interface SaveImageProfilePort {
    String saveImageProfile(MultipartFile file);
}
