package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SpringJpaProviderCompanyRepository extends JpaRepository<ProviderCompanyJpaEntity, Long> {

    Optional<ProviderCompanyJpaEntity> findFirstByUuid(UUID uuid);
}
