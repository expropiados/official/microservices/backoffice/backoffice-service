package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreReport;

import java.io.IOException;

public interface GetPromScoreReportUseCase {
    GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO) throws IOException;
}
