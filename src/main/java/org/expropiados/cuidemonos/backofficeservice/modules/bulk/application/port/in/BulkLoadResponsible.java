package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BulkLoadResponsible {
    Long id;
    String names;
    String fatherLastname;
    String motherLastname;
}
