package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetPaginatedClientSupervisorsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetPaginatedClientSupervisorsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

@UseCase
@RequiredArgsConstructor
public class GetPaginatedClientSupervisorsService implements GetPaginatedClientSupervisorsUseCase {
    private final GetPaginatedClientSupervisorsPort getPaginatedClientSupervisorsPort;

    @Override
    public Paginator<ClientSupervisor> getPaginatedClientSupervisors(Filters filters, Long clientCompanyId) {
        return getPaginatedClientSupervisorsPort.getPaginatedClientSupervisors(filters, clientCompanyId);
    }
}
