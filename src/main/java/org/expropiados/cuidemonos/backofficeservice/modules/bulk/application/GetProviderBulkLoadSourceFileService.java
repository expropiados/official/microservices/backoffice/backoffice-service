package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetProviderBulkLoadSourceFileUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetProviderBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetUrlBulkLoadFilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkSourceFileType;

import java.net.URL;


@UseCase
@RequiredArgsConstructor
public class GetProviderBulkLoadSourceFileService implements GetProviderBulkLoadSourceFileUseCase {

    private final GetProviderBulkLoadUseCase getProviderBulkLoadUseCase;
    private final GetUrlBulkLoadFilePort getUrlBulkLoadFilePort;

    @Override
    public URL getProviderBulkLoadSourceFile(Long bulkId, BulkSourceFileType bulkSourceFileType) {
        var bulkLoad = getProviderBulkLoadUseCase
                .getProviderBulkLoad(bulkId)
                .getBulkLoad();

        var fileKey = bulkSourceFileType == BulkSourceFileType.ORIGINAL ?
                bulkLoad.getSourceFileKey() : bulkLoad.getErrorSourceFileKey();

        return getUrlBulkLoadFilePort.getUrlBulkLoadFile(fileKey);
    }
}
