package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

public interface EditClientSupervisorUseCase {
    ClientSupervisor editClientSupervisor(ClientSupervisorToEdit clientSupervisorToEdit);
}
