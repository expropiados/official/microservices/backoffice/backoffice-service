package org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

import java.util.UUID;

@Getter
public class SpecialistNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_SPE_001";
    private final String message;
    private final transient Object data;


    @Value
    static class Data {
        Long userId;
        UUID specialistUserUuid;
    }

    public SpecialistNotFoundException(Long userId) {
        super();
        this.message = String.format("Specialist %d not found", userId);
        this.data = new Data(userId, null);
    }

    public SpecialistNotFoundException(UUID specialistUserUuid) {
        super();
        this.message = String.format("Specialist with user UUID '%s' not found", specialistUserUuid.toString());
        this.data = new Data(null, specialistUserUuid);
    }
}
