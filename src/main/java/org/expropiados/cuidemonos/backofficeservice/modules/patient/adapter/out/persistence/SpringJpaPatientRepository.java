package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface SpringJpaPatientRepository extends JpaRepository<PatientJpaEntity, Long> {
    Optional<PatientJpaEntity> findByUuid(UUID uuid);

    Optional<PatientJpaEntity> findByUserId(Long userId);

    Optional<PatientJpaEntity> findByUser_Document(String document);

    @Query("SELECT P FROM PatientJpaEntity P WHERE P.clientCompanyId = :companyId AND (UPPER(P.user.document) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.name) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.fatherLastname) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.motherLastname) LIKE CONCAT('%', UPPER(:search), '%')) ORDER BY P.user.id")
    Page<PatientJpaEntity> searchPatients(Pageable pageable, @Param("search") String search, @Param("companyId") Long companyId);
}
