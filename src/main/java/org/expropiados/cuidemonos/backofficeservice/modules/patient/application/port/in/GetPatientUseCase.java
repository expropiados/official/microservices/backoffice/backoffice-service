package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

import java.util.List;

public interface GetPatientUseCase {
    List<Patient> getPatients();
}
