package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class GetReportDateRangeSpecialistDTO {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate initDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private UUID specialistUUID;
}
