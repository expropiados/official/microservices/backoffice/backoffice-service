package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.BadDateRangeException;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetPromScoreReport;
import org.springframework.web.bind.annotation.*;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;


@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/reports")
public class GetPromScoreReportController {
    private final JwtHelper jwtHelper;
    private final GetPromScoreReportUseCase getPromScoreReportUseCase;

    @GetMapping("/appointment/prom_score")
    public GetPromScoreReport getPromScoreReport(@RequestHeader("Authorization") String token,
                                                       @RequestHeader("role") Integer role,
                                                       GetPromScoreReportDTO getPromScoreReportDTO) throws IOException {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.CLIENT_SUPERVISOR) throw new AuthenticationException(userRole.name());
        if (getPromScoreReportDTO.getInitDate().isAfter(getPromScoreReportDTO.getEndDate()))
            throw new BadDateRangeException(getPromScoreReportDTO.getInitDate(), getPromScoreReportDTO.getEndDate());
        return getPromScoreReportUseCase.getPromPostPollReport(getPromScoreReportDTO);
    }
}
