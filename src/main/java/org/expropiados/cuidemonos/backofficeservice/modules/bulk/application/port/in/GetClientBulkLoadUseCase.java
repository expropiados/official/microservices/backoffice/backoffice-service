package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

public interface GetClientBulkLoadUseCase {
    BulkLoadWithResponsible getClientBulkLoad(Long bulkId, Long clientCompanyId);
}
