package org.expropiados.cuidemonos.backofficeservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.RegisterUserException;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.RegisterPatientUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.RegisterPatientPort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.ValidateSpecialistByDocumentService;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserPort;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterPatientService implements RegisterPatientUseCase {
    private final RegisterPatientPort registerPatientPort;
    private final RegisterUserPort registerUserPort;
    private final SaveImageProfilePort saveImageProfilePort;
    private final GetUserPort getUserPort;
    private final ValidatePatientByDocumentService validatePatientByDocumentService;


    @Override
    public Patient registerPatient(PatientToRegister patientToRegister, Long clientCompanyId) {
        var imageFile = patientToRegister.getMultipartFile();
        var result= validatePatientByDocumentService.validatePatientByDocument(patientToRegister.getUser().getDocument());
        var user = getUserPort.getUserByDocument(patientToRegister.getUser().getDocument());
        if (imageFile != null) {
            var imageProfileUrl = saveImageProfilePort.saveImageProfile(imageFile);
            patientToRegister.setProfileImage(imageProfileUrl);
        }
        if (user == null) user = registerUserPort.registerUser(patientToRegister.getUser(), UUID.randomUUID());
        if(!result.getIsPatient()) return registerPatientPort.registerPatient(patientToRegister, user,UUID.randomUUID(), clientCompanyId);
        throw new RegisterUserException(user.getDocument()   ,"patient");
    }
}
