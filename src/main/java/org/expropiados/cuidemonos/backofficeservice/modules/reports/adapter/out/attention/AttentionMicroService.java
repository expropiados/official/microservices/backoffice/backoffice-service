package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.out.attention;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.*;
import retrofit2.Call;
import retrofit2.http.*;

import java.time.LocalDate;
import java.util.UUID;

public interface AttentionMicroService {

    @GET("report/monthly")
    @Headers({"Accept: application/json"})
    Call<GetMedicalReasonMonthlyReport> getMedicalReasonsMonthlyReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate);

    @GET("report/freq_monthly")
    @Headers({"Accept: application/json"})
    Call<GetMedicalReasonMonthlyFreqReport> getMedicalReasonsMonthlyFreqReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate, @Query("clientCompanyId") Long clientCompanyId);

    @GET("report/post-poll/monthly")
    @Headers({"Accept: application/json"})
    Call<GetMonthlyPostPollReport> getMonthlyPostPollReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate, @Query("specialistUUID") UUID specialistUUID);

    @GET("report/appointment/score")
    @Headers({"Accept: application/json"})
    Call<GetPromScoreReport> getPromScoreReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate, @Query("clientCompanyId") Long clientCompanyId);

    @GET("report/post-poll/score")
    @Headers({"Accept: application/json"})
    Call<GetPromScoreMonthlyReport> getPromScoreMonthlyReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate);

    @GET("report/availabilities/hours")
    @Headers({"Accept: application/json"})
    Call<GetHoursReport> getHoursReport(@Query("startDate") LocalDate startDate, @Query("endDate") LocalDate endDate, @Query("specialistUUID") UUID specialistUUID);
}
