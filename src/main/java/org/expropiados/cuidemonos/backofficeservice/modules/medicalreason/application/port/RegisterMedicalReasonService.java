package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.MedicalReasonToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.RegisterMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.RegisterMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

@UseCase
@RequiredArgsConstructor
public class RegisterMedicalReasonService implements RegisterMedicalReasonUseCase {
    private final RegisterMedicalReasonPort registerMedicalReasonPort;

    @Override
    public MedicalReason registerMedicalReason(MedicalReasonToRegister medicalReasonToRegister) {
        return registerMedicalReasonPort.registerMedicalReason(medicalReasonToRegister);
    }
}
