package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetClientBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetClientBulkLoadPort;


@UseCase
@RequiredArgsConstructor
public class GetClientBulkLoadService implements GetClientBulkLoadUseCase {

    private final GetClientBulkLoadPort getBulkLoadPort;

    @Override
    public BulkLoadWithResponsible getClientBulkLoad(Long bulkId, Long clientCompanyId) {
        return getBulkLoadPort.getClientBulkLoad(bulkId, clientCompanyId)
                .orElseThrow(() -> new BulkLoadNotFoundException(bulkId));
    }
}
