package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in;

import lombok.Data;

@Data
public class MedicalReasonToRegister {
    private String name;
    private String description;
}
