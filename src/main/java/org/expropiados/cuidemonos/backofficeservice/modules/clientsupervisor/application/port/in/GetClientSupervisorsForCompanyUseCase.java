package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

import java.util.List;

public interface GetClientSupervisorsForCompanyUseCase {
    List<ClientSupervisor> getClientSupervisorsForCompany(Long clientCompanyId);
}
