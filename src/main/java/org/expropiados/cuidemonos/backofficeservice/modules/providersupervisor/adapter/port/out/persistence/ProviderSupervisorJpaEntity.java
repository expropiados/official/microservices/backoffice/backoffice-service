package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence;

import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserJpaEntity;

import javax.persistence.*;

@Data @Entity
@Table(name = "PROVIDER_SUPERVISOR")
public class ProviderSupervisorJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "provider_supervisor_id")
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name = "phone_number", length = 9)
    private String phoneNumber;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserJpaEntity user;
}
