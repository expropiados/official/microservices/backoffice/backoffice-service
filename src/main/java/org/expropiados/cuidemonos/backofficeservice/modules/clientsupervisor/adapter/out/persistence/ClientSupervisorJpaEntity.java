package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence;

import lombok.*;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence.CompanyJpaEntity;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserJpaEntity;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@ToString
@Getter @Setter
@RequiredArgsConstructor
@Entity @Table(name= "CLIENT_SUPERVISOR")
public class ClientSupervisorJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_supervisor_id")
    private Long id;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name = "client_company_id", nullable = false)
    private Long clientCompanyId;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name ="profile_image", length = 300)
    private String profileImage;

    @Column(name = "phone_number", length = 9)
    private String phoneNumber;

    @Column(name = "is_blocked")
    private Boolean isBlocked;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private UserJpaEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_company_id", insertable = false, updatable = false)
    @ToString.Exclude
    private CompanyJpaEntity clientCompany;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ClientSupervisorJpaEntity that = (ClientSupervisorJpaEntity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 1001106131;
    }
}
