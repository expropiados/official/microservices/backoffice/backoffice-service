package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParametersDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.Optional;

public interface UpdateClientCompanyParametersPort {
    Optional<Company> updateProviderCompanyParameters(Long clientCompanyId, ClientCompanyParametersDTO clientParameters);
}
