package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.s3;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.thirdparty.AwsStorageAdapter;
import org.springframework.web.multipart.MultipartFile;

@PersistenceAdapter
@RequiredArgsConstructor
public class PatientImageS3Adapter implements SaveImageProfilePort {

    private final AwsStorageAdapter client;

    @Override
    public String saveImageProfile(MultipartFile file) {
        return client.saveFile(file);
    }
}
