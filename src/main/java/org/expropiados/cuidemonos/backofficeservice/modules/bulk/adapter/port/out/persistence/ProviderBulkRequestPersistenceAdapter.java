package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetAllProviderBulkLoadsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetProviderBulkLoadPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterProviderBulkRequestPort;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class ProviderBulkRequestPersistenceAdapter implements RegisterProviderBulkRequestPort, GetAllProviderBulkLoadsPort, GetProviderBulkLoadPort {

    private final ProviderBulkRequestJpaRepository repository;
    private final ProviderBulkRequestMapper mapper;

    @Override
    public void registerProviderBulkRequest(Long providerSupervisorId, Long bulkId) {
        var requestRow = new ProviderBulkRequestJpaEntity();
        requestRow.setBulkLoadId(bulkId);
        requestRow.setProviderSupervisorId(providerSupervisorId);

        repository.save(requestRow);
    }

    @Override
    public Paginator<BulkLoadWithResponsible> getAllProviderBulkLoads(Filters filters) {
        var pageable = PageRequest.of(filters.getPage(), filters.getPageSize());
        var page = repository.findAllOrderByBulkLoadRequestedAt(pageable);

        var content = page.getContent()
                .stream()
                .map(mapper::toBulkLoadWithResponsible)
                .collect(Collectors.toList());

        return Paginator.<BulkLoadWithResponsible>builder()
                .page(filters.getPage())
                .pageSize(filters.getPageSize())
                .total(page.getTotalElements())
                .data(content)
                .build();
    }

    @Override
    public Optional<BulkLoadWithResponsible> getProviderBulkLoad(Long bulkId) {
        return repository.findByBulkLoadId(bulkId).map(mapper::toBulkLoadWithResponsible);
    }
}
