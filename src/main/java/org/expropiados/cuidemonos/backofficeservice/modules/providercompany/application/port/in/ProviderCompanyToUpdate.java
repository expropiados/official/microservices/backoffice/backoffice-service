package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in;

import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = false)
public class ProviderCompanyToUpdate {
    @NotNull
    String ruc;

    @NotNull
    String businessName;

    Long id;

    UUID uuid;

    String email;

    String responsibleEmail;

    String responsibleName;

    String address;

    String phoneNumber;

    String imageUrl;
}
