package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.storage;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.backoffice.storage.StoragePathsConfig;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.StorageAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetUrlBulkLoadFilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.StoreBulkLoadFilePort;
import org.expropiados.cuidemonos.backofficeservice.thirdparty.AwsStorageExternalService;

import java.io.File;
import java.net.URL;

@StorageAdapter
@RequiredArgsConstructor
public class BulkLoadAwsStorageAdapter implements StoreBulkLoadFilePort, GetUrlBulkLoadFilePort {

    private final StoragePathsConfig storagePathsConfig;
    private final AwsStorageExternalService awsStorageExternalService;

    @Override
    public String storeBulkLoadFile(File file) {
        var path = storagePathsConfig.getBulk();
        return awsStorageExternalService.saveFile(file, path);
    }

    @Override
    public URL getUrlBulkLoadFile(String fileKey) {
        return awsStorageExternalService.generatePresignedUrlForObject(fileKey);
    }
}
