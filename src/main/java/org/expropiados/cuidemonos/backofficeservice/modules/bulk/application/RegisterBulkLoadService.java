package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterBulkJobUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterBulkLoadPort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.StoreBulkLoadFilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoadToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkType;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterBulkLoadService implements RegisterBulkLoadUseCase {

    private final StoreBulkLoadFilePort storeBulkLoadFilePort;
    private final RegisterBulkLoadPort registerBulkLoadPort;
    private final RegisterBulkJobUseCase registerBulkJobUseCase;

    @Override
    public Long registerBulkLoad(File file, String originalFilename, BulkType bulkType) {
        var sourceFileKey = storeBulkLoadFilePort.storeBulkLoadFile(file);
        var requestedAt = LocalDateTimePeruZone.now();

        var bulkLoadToRegister = new BulkLoadToRegister(sourceFileKey, originalFilename, bulkType, requestedAt);
        var bulkId = registerBulkLoadPort.registerBulkLoad(bulkLoadToRegister);

        registerBulkJobUseCase.registerBulkJob(bulkId);
        return bulkId;
    }
}
