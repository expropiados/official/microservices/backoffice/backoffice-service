package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyFreqReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetMedicalReasonMonthlyFreqReportUseCase {
    GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(LocalDate initDate, LocalDate endDate, Long clientCompanyId) throws IOException;

}
