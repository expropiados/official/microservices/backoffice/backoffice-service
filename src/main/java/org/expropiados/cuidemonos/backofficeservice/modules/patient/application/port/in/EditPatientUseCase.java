package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

public interface EditPatientUseCase {
    Patient editPatient(PatientToEdit patientToEdit);
}
