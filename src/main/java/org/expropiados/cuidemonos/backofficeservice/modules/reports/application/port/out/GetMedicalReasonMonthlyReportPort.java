package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetMedicalReasonMonthlyReportPort {
    GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException;
}
