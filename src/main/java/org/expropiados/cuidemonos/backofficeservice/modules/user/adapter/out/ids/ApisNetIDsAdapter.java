package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.ids;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.ApisNetIDsPort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class ApisNetIDsAdapter implements Callback<UserApi>{
    static final String BASE_URL = "https://api.apis.net.pe/v1/";

    public UserApi start(String dni) throws IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ApisNetIDsPort port = retrofit.create(ApisNetIDsPort.class);

        Call<UserApi> call = port.getUserInformation(dni);
        Response<UserApi> response = call.execute();
        if (!response.isSuccessful()){
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }
        return response.body();
    }

    @Override
    public void onResponse(Call<UserApi> call, Response<UserApi> response) {
        System.out.println(call);
        System.out.println(response);
        if(response.isSuccessful()) {
            UserApi user = response.body();
            System.out.println(user);
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<UserApi> call, Throwable t) {
        t.printStackTrace();
    }
}
