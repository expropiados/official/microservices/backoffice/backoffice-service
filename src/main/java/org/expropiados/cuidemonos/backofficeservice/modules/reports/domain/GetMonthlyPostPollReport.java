package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetMonthlyPostPollReport {
    List<GetMonthlyGeneralReport> seriesLine;
    List<GetMonthlyPostPollByRatingReport> seriesStackedColumn;
}
