package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.EditSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToEditDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class EditSpecialistController {

    private final JwtHelper jwtHelper;
    private final EditSpecialistUseCase editSpecialistUseCase;
    private final SpecialistDTOMapper specialistDTOMapper;

    @PostMapping(value = "/specialists/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Specialist editSpecialist(@ModelAttribute SpecialistToEditDTO specialistToEditDTO,
                                     @RequestHeader("Authorization") String token,
                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var specialistToEdit = specialistDTOMapper.toSpecialistToEdit(specialistToEditDTO);
        return editSpecialistUseCase.editSpecialist(specialistToEdit);
    }
}
