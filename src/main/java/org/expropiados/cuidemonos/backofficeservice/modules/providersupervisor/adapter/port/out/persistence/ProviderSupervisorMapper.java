package org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.domain.ProviderSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface ProviderSupervisorMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "isBlocked", target = "isBlocked")
    @Mapping(target = "profileImage", ignore = true)
    ProviderSupervisor toProviderSupervisor(ProviderSupervisorJpaEntity providerSupervisorJpaEntity);
}
