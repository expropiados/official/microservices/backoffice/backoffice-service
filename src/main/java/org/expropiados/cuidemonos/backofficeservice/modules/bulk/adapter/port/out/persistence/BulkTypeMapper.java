package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BulkTypeMapper {

    default BulkType toBulkType(Integer value) {
        switch (value) {
            case 0:
                return BulkType.PATIENTS;
            case 1:
                return BulkType.SPECIALISTS;
            default:
                return null;
        }
    }
}
