package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.GetPaginatedMedicalReasonsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.GetPaginatedMedicalReasonsPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

@UseCase
@RequiredArgsConstructor
public class GetPaginatedMedicalReasonsService implements GetPaginatedMedicalReasonsUseCase {
    private final GetPaginatedMedicalReasonsPort getPaginatedMedicalReasonsPort;

    @Override
    public Paginator<MedicalReason> getPaginatedMedicalReasons(Filters filters) {
        return getPaginatedMedicalReasonsPort.getPaginatedMedicalReasons(filters);
    }
}
