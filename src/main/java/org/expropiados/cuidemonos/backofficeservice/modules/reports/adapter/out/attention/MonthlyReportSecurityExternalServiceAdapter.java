package org.expropiados.cuidemonos.backofficeservice.modules.reports.adapter.out.attention;

import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.attention.AttentionHostConfig;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.ExternalServiceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out.*;
import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.*;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

@ExternalServiceAdapter
@RequiredArgsConstructor
public class MonthlyReportSecurityExternalServiceAdapter implements GetMedicalReasonMonthlyReportPort, GetMedicalReasonMonthlyFreqReportPort, GetMonthlyPostPollReportPort, GetPromScoreReportPort, GetPromScoreMonthlyReportPort, GetHoursReportPort {
    private final AttentionHostConfig attentionHostConfig;


    @Override
    public GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException {
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call =  service.getMedicalReasonsMonthlyReport(initDate, endDate);
        var response = call.execute();
        return response.body();
    }

    @Override
    public GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(LocalDate initDate, LocalDate endDate, Long clientCompanyId) throws IOException {
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call =  service.getMedicalReasonsMonthlyFreqReport(initDate, endDate, clientCompanyId);
        var response = call.execute();
        return response.body();
    }

    @Override
    public GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate,  UUID specialistUUID) throws IOException {
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call =  service.getMonthlyPostPollReport(startDate, endDate, specialistUUID);
        var response = call.execute();
        return response.body();
    }

    @Override
    public GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO) throws IOException{
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call = service.getPromScoreReport(getPromScoreReportDTO.getInitDate(), getPromScoreReportDTO.getEndDate(), getPromScoreReportDTO.getClientCompanyId());
        var response = call.execute();
        return response.body();
    }

    @Override
    public GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate initDate, LocalDate endDate) throws IOException{
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call = service.getPromScoreMonthlyReport(initDate, endDate);
        var response = call.execute();
        return response.body();
    }

    @Override
    public GetHoursReport getHoursReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) throws IOException{
        var microserviceHost = attentionHostConfig.getHost();
        var gson = new GsonBuilder().setLenient().create();

        var retrofit = new Retrofit.Builder()
                .baseUrl(microserviceHost)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        var service = retrofit.create(AttentionMicroService.class);
        var call = service.getHoursReport(startDate, endDate, specialistUUID);
        var response = call.execute();
        return response.body();
    }
}
