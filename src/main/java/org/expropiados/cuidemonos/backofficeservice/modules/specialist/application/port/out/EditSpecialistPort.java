package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

public interface EditSpecialistPort {
    Specialist editSpecialist(SpecialistToEdit specialistToEdit, Specialist specialist);
}
