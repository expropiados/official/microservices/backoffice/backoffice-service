package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import lombok.Data;

@Data
public class ClientCompanyParametersDTO {
    private Integer appointmentDuration;
    private Integer appointmentMaxCant;
    private Integer appointmentMaxCancelled;
    private Integer appointmentTimeToCancel;
}
