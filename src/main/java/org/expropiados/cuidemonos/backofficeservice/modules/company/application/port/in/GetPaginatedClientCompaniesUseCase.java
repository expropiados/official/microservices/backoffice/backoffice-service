package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

public interface GetPaginatedClientCompaniesUseCase {
    Paginator<Company> getPaginatedClientCompanies(Filters filters);
}
