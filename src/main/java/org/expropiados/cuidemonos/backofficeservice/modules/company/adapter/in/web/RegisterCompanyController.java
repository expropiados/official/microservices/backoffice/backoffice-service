package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.RegisterCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RegisterCompanyController {
    private final JwtHelper jwtHelper;
    private final RegisterCompanyUseCase registerCompanyUseCase;

    @PostMapping(value = "/companies", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Company registerCompany(@ModelAttribute ClientCompanyToRegister clientCompanyToRegister,
                                   @RequestHeader("Authorization") String token,
                                   @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return registerCompanyUseCase.registerCompany(clientCompanyToRegister);
    }
}
