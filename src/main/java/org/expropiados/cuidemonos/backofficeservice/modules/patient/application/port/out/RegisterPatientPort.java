package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.PatientToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

public interface RegisterPatientPort {
    Patient registerPatient(PatientToRegister patientToRegister, User user, UUID uuid, Long clientCompanyId);
}
