package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.List;

public interface GetClientCompaniesUseCase {
    List<Company> getClientCompanies();
}
