package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetExtraInfoForSpecialistByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistExtraInfo;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.GetSpecialistExtraInfoForSpecialistPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.SpecialistNotFoundException;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetExtraInfoForSpecialistByUUIDService implements GetExtraInfoForSpecialistByUUIDUseCase {
    private final GetSpecialistExtraInfoForSpecialistPort getSpecialistExtraInfoForSpecialistPort;

    @Override
    public SpecialistExtraInfo getExtraInfoForSpecialistByUUID(UUID specialistUserUuid) {
        return getSpecialistExtraInfoForSpecialistPort.getSpecialistExtraInfoForSpecialist(specialistUserUuid)
                .orElseThrow(() -> new SpecialistNotFoundException(specialistUserUuid));
    }
}
