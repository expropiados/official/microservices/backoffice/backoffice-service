package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;


import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

public interface RegisterCompanyUseCase {
    Company registerCompany(ClientCompanyToRegister company);
}
