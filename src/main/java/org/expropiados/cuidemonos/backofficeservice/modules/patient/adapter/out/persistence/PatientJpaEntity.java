package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.out.persistence;

import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence.CompanyJpaEntity;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserJpaEntity;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name= "PATIENT")
public class PatientJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "patient_id")
    private Long id;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name = "client_company_id")
    private Long clientCompanyId;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name ="address", length = 100)
    private String address;

    @Column(name ="profile_image", length = 300)
    private String profileImage;

    @Column(name = "phone_number", length = 9)
    private String phoneNumber;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private UserJpaEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_company_id", insertable = false, updatable = false)
    private CompanyJpaEntity clientCompany;
}
