package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.backoffice.ProviderCompanyConfig;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.GetMyProviderCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out.GetProviderCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

@UseCase
@RequiredArgsConstructor
public class GetMyProviderCompanyService implements GetMyProviderCompanyUseCase {

    private final ProviderCompanyConfig providerCompanyConfig;
    public final GetProviderCompanyPort getProviderCompanyPort;

    @Override
    public ProviderCompany getMyProviderCompany() {
        var uuid = providerCompanyConfig.getUuid();
        return getProviderCompanyPort.getProviderCompany(uuid).orElse(null);
    }
}
