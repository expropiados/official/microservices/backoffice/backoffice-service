package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyFreqReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetMedicalReasonMonthlyFreqReportPort {
    GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(LocalDate initDate, LocalDate endDate, Long clientCompanyId) throws IOException;
}
