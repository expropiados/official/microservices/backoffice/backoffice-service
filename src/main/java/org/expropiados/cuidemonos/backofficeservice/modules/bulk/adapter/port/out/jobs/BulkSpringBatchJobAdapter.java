package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.jobs;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.JobAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterBulkJobPort;

@JobAdapter
@RequiredArgsConstructor
public class BulkSpringBatchJobAdapter implements RegisterBulkJobPort {

    @Override
    public Long registerBulkJob(Long bulkId) {
        return null;
    }
}
