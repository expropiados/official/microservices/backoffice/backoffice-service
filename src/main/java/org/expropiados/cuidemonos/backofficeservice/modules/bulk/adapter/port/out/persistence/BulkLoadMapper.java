package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoadToRegister;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {BulkTypeMapper.class, BulkStateMapper.class})
public interface BulkLoadMapper {

    @Mapping(target = "state", source = "state.value")
    @Mapping(target = "type", source = "type.value")
    @Mapping(target = "requestedAt", source = "requestedAt")
    @Mapping(target = "sourceFileKey", source = "sourceFileKey")
    @Mapping(target = "filename", source = "filename")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "errorSourceFileKey", ignore = true)
    @Mapping(target = "successfulRegistered", ignore = true)
    @Mapping(target = "failedRegistered", ignore = true)
    BulkLoadJpaEntity toBulkLoadJpaEntity(BulkLoadToRegister bulkLoadToRegister);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "requestedAt", source = "requestedAt")
    @Mapping(target = "filename", source = "filename")
    @Mapping(target = "failedRegistered", source = "failedRegistered")
    @Mapping(target = "successfulRegistered", source = "successfulRegistered")
    @Mapping(target = "sourceFileKey", source = "sourceFileKey")
    @Mapping(target = "errorSourceFileKey", source = "errorSourceFileKey")
    BulkLoad toBulkLoad(BulkLoadJpaEntity bulkLoadJpaEntity);
}
