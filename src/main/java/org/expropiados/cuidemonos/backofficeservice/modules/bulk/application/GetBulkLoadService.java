package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetBulkLoadUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.GetBulkLoadPort;


@UseCase
@RequiredArgsConstructor
public class GetBulkLoadService implements GetBulkLoadUseCase {

    private final GetBulkLoadPort getBulkLoadPort;

    @Override
    public BulkLoad getBulkLoad(Long bulkId) {
        return getBulkLoadPort.getBulkLoad(bulkId)
                .orElseThrow(() -> new BulkLoadNotFoundException(bulkId));
    }
}
