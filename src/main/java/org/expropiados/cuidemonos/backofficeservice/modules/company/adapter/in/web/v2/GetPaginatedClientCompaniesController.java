package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetPaginatedClientCompaniesUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class GetPaginatedClientCompaniesController {
    private final JwtHelper jwtHelper;
    private final GetPaginatedClientCompaniesUseCase getPaginatedClientCompaniesUseCase;

    @GetMapping(value = "/companies")
    public Paginator<Company> getCompanies(Pageable pageable,
                                      @RequestParam(value = "search", defaultValue = "") String search,
                                      @RequestHeader("Authorization") String token,
                                      @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        if (userRole != UserRole.PROVIDER_SUPERVISOR) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        return getPaginatedClientCompaniesUseCase.getPaginatedClientCompanies(filters);
    }
}
