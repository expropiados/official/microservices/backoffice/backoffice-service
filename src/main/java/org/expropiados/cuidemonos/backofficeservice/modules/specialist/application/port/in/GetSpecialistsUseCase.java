package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

import java.util.List;

public interface GetSpecialistsUseCase {
    Paginator<Specialist> getSpecialists(Filters filters);
}
