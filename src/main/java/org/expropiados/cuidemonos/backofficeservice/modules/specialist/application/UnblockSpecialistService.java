package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.UnblockSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.BlockSpecialistPort;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class UnblockSpecialistService implements UnblockSpecialistUseCase {

    private final BlockSpecialistPort blockSpecialistPort;

    @Override
    public Long unblockSpecialist(UUID specialistUUID) {
        return blockSpecialistPort.blockSpecialist(false, specialistUUID);
    }

}
