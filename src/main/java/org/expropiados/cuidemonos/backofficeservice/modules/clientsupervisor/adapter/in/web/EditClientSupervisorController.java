package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToEditDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.EditClientSupervisorUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/companies/supervisor")
public class EditClientSupervisorController {
    private final JwtHelper jwtHelper;
    private final EditClientSupervisorUseCase editClientSupervisorUseCase;
    private final ClientSupervisorDTOMapper clientSupervisorDTOMapper;

    @PostMapping(value = "/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ClientSupervisor editClientSupervisor(@ModelAttribute ClientSupervisorToEditDTO clientSupervisorToEditDTO,
                                                 @RequestHeader("Authorization") String token,
                                                 @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole != UserRole.PROVIDER_SUPERVISOR) {
            throw new NotAuthorizedForRoleException(userRole);
        }
        var clientSupervisorToEdit = clientSupervisorDTOMapper.toClientSupervisorToEdit(clientSupervisorToEditDTO);
        return editClientSupervisorUseCase.editClientSupervisor(clientSupervisorToEdit);
    }
}
