package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetMedicalReasonMonthlyReport {
    private List<GetMedicalReasonMonthly> seriesLine;
    private List<String> categories;
}
