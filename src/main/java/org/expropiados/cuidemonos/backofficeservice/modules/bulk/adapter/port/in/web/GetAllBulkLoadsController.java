package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetAllClientBulkLoadsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.GetAllProviderBulkLoadsUseCase;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetAllBulkLoadsController {
    private final GetAllProviderBulkLoadsUseCase providerBulkLoadsUseCase;
    private final GetAllClientBulkLoadsUseCase clientBulkLoadsUseCase;
    private final JwtHelper jwtHelper;

    @GetMapping("/bulk-loads")
    public Paginator<BulkLoadWithResponsible> getAllBulkLoads(Pageable pageable,
                                                              @RequestHeader("Authorization") String token,
                                                              @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .build();

        if (userRole == UserRole.CLIENT_SUPERVISOR) {
            var companyId = tokenBody.getClientSupervisorCompanyId();
            return clientBulkLoadsUseCase.getAllClientBulkLoads(companyId, filters);
        }

        if (userRole == UserRole.PROVIDER_SUPERVISOR) {
            return providerBulkLoadsUseCase.getAllProviderBulkLoads(filters);
        }

        throw new NotAuthorizedForRoleException(userRole);
    }
}
