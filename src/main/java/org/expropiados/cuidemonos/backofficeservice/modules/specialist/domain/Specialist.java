package org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class Specialist {
    private Long id;
    private User user;
    private String email;
    private String phoneNumber;
    private String address;
    private String profileImage;
    private Boolean isBlocked;
    private UUID uuid;

    private String aboutMe;
    private String degree;
    private Float rating;
    private Long totalAppointments;
}
