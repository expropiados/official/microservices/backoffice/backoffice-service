package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserApi;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;


@Getter
@Builder
@AllArgsConstructor
public class ClientSupervisorChecked {
    private final Boolean isClientSupervisor;
    private final UserApi userApi;
    private final User user;
}
