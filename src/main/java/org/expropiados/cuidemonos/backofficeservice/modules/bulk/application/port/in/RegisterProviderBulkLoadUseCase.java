package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import java.io.File;

public interface RegisterProviderBulkLoadUseCase {
    void registerBulkLoad(File file, String originalFilename, Long providerSupervisorId);
}
