package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.springframework.web.multipart.MultipartFile;

public interface SaveClientCompanyPort {
    String saveClientCompanyImageFile(MultipartFile file);
}
