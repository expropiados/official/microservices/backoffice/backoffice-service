package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.ruc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.CompanyApi;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.ApisNetRucPort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class ApisNetRucAdapter implements Callback<CompanyApi> {
    static final String BASE_URL = "https://api.apis.net.pe/v1/";

    public CompanyApi start(String ruc) throws IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ApisNetRucPort port = retrofit.create(ApisNetRucPort.class);

        Call<CompanyApi> call = port.getUserInformation(ruc);
        Response<CompanyApi> response = call.execute();
        if (!response.isSuccessful()){
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }
        return response.body();
    }

    @Override
    public void onResponse(Call<CompanyApi> call, Response<CompanyApi> response) {
        System.out.println(call);
        System.out.println(response);
        if(response.isSuccessful()) {
            CompanyApi company = response.body();
            System.out.println(company);
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<CompanyApi> call, Throwable t) {
        t.printStackTrace();
    }
}
