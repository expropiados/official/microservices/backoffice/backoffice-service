package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import lombok.*;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence.ProviderSupervisorJpaEntity;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@ToString
@Getter @Setter
@RequiredArgsConstructor
@Entity @Table(name = "PROV_BULK_REQUEST")
public class ProviderBulkRequestJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prov_bulk_request_id")
    private Long id;

    @Column(name = "provider_supervisor_id", nullable = false)
    private Long providerSupervisorId;

    @Column(name = "bulk_load_id", nullable = false)
    private Long bulkLoadId;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_supervisor_id", insertable = false, updatable = false)
    private ProviderSupervisorJpaEntity providerSupervisor;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bulk_load_id", insertable = false, updatable = false)
    private BulkLoadJpaEntity bulkLoad;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ProviderBulkRequestJpaEntity that = (ProviderBulkRequestJpaEntity) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return 1115626779;
    }
}
