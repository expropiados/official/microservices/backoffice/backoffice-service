package org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.TypeDocument;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
public class UserToRegister {

    @NotBlank
    @Size(min=8, max=12)
    String document;

    @NotBlank
    TypeDocument typeDocument;

    @NotBlank
    String name;

    @NotBlank
    String fatherLastname;

    @NotBlank
    String motherLastname;

    private LocalDate birthdate;

    private UUID uuid;
}
