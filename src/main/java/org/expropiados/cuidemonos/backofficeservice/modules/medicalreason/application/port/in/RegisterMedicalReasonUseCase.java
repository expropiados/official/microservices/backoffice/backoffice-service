package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.domain.MedicalReason;

public interface RegisterMedicalReasonUseCase {
    MedicalReason registerMedicalReason(MedicalReasonToRegister medicalReasonToRegister);
}
