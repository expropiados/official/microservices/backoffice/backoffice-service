package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkLoad;

import java.util.Optional;

public interface GetBulkLoadPort {
    Optional<BulkLoad> getBulkLoad(Long bulkId);
}
