package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.s3;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.SaveClientCompanyPort;
import org.expropiados.cuidemonos.backofficeservice.thirdparty.AwsStorageAdapter;
import org.springframework.web.multipart.MultipartFile;

@PersistenceAdapter
@RequiredArgsConstructor
public class ClientCompanyImageS3Adapter implements SaveClientCompanyPort {

    private final AwsStorageAdapter client;

    @Override
    public String saveClientCompanyImageFile(MultipartFile file) {
        return client.saveFile(file);
    }
}
