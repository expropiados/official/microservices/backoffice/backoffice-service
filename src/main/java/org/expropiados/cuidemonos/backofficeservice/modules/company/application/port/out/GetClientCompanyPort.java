package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GetClientCompanyPort {
    List<Company> getClientCompanies();
    Company getClientCompanyByRUC(String ruc);
    Optional<Company> getClientCompanyById(Long id);
    Optional<Company> getClientCompanyByUUID(UUID uuid);
}
