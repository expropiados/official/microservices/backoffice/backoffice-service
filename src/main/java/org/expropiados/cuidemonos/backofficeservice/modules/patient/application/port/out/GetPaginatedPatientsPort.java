package org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;

public interface GetPaginatedPatientsPort {
    Paginator<Patient> getPaginatedPatients(Filters filters, Long clientCompanyId);
}
