package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.UserInputException;

import java.time.LocalDate;

public class BadDateRangeException extends RuntimeException implements UserInputException {

    private final String code = "BOF_REP_001";
    private final String message;
    private final transient Object data;

    @Value
    static class Data {
        LocalDate startDate;
        LocalDate endDate;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public Object getData() {
        return data;
    }

    public String getMessage(){return message;}

    public BadDateRangeException(LocalDate startDate, LocalDate endDate){
        super();
        this.message = String.format("Start Date [%s] is greater than End Date [%s]", startDate.toString(), endDate.toString());
        this.data = new BadDateRangeException.Data(startDate, endDate);
    }
}
