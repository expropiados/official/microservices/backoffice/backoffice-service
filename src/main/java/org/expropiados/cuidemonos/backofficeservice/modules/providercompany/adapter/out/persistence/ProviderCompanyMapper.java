package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface ProviderCompanyMapper {

    @Mapping(source = "businessName", target = "businessName")
    @Mapping(source = "ruc", target = "ruc")
    @Mapping(source = "id", target = "id")
    @Mapping(source = "address", target="address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "imageUrl", target = "imageUrl")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "responsibleName", source = "responsibleName")
    @Mapping(source = "responsibleEmail", target = "responsibleEmail")
    @Mapping(source = "uuid", target = "uuid")
    ProviderCompanyJpaEntity toProviderCompanyEntity(ProviderCompanyToUpdate providerCompanyToUpdate);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "ruc", target = "ruc")
    @Mapping(source = "businessName", target = "businessName")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "imageUrl", target = "imageUrl")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "responsibleName", source = "responsibleName")
    @Mapping(source = "responsibleEmail", target = "responsibleEmail")
    @Mapping(source = "uuid", target = "uuid")
    ProviderCompany toProviderCompany(ProviderCompanyJpaEntity providerCompanyJpaEntity);
}
