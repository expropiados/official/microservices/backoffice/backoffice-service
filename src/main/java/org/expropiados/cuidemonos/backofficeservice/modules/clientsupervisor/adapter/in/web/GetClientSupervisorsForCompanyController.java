package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetClientSupervisorsForCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetClientSupervisorsForCompanyController {

    private final JwtHelper jwtHelper;
    private final GetClientSupervisorsForCompanyUseCase getClientSupervisorsForCompanyUseCase;

    @GetMapping(value = "/companies/supervisor")
    public List<ClientSupervisor> getClientSupervisorsForCompany(@RequestParam Long clientCompanyId,
                                                                 @RequestHeader("Authorization") String token,
                                                                 @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getClientSupervisorsForCompanyUseCase.getClientSupervisorsForCompany(clientCompanyId);
    }
}
