package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.GetClientSupervisorsForCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out.GetClientSupervisorPort;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetClientSupervisorsForCompanyService implements GetClientSupervisorsForCompanyUseCase {

    public final GetClientSupervisorPort getClientSupervisorPort;

    @Override
    public List<ClientSupervisor> getClientSupervisorsForCompany(Long clientCompanyId){
        return getClientSupervisorPort.getClientSupervisorsByCompanyId(clientCompanyId);
    }
}
