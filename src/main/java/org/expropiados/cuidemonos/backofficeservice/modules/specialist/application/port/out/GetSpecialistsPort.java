package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;

import java.util.Optional;
import java.util.UUID;

public interface GetSpecialistsPort {
    Specialist getSpecialistByIdUser(Long idUser);

    Paginator<Specialist> getSpecialists(Filters filters);

    Optional<Specialist> getSpecialistByUUID(UUID uuid);

    Optional<Specialist> getBySpecialistId(Long specialistId);
}

