package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain.BulkSourceFileType;

import java.net.URL;

public interface GetClientBulkLoadSourceFileUseCase {
    URL getClientBulkLoadSourceFile(Long bulkId, Long clientCompanyId, BulkSourceFileType bulkSourceFileType);
}
