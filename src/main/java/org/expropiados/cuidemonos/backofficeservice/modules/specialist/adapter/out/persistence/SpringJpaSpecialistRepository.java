package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface SpringJpaSpecialistRepository extends JpaRepository<SpecialistJpaEntity, Long> {

    SpecialistJpaEntity findByUserId(Long idUser);

    Optional<SpecialistJpaEntity> findByUuid(UUID specialistUUID);

    @Query("SELECT S FROM SpecialistJpaEntity S WHERE S.user.document LIKE %:search% OR UPPER(S.user.name) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(S.user.fatherLastname) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(S.user.motherLastname) LIKE CONCAT('%', UPPER(:search), '%') ORDER BY S.user.id")
    Page<SpecialistJpaEntity> searchSpecialists(Pageable pageable, @Param("search") String search);

    Optional<SpecialistJpaEntity> findByUser_Uuid(UUID specialistUserUuid);
}
