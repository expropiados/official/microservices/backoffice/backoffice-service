package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence.ClientSupervisorJpaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {BulkLoadMapper.class})
public interface ClientBulkRequestMapper {

    @Mapping(target = "bulkLoad", source = "bulkLoad")
    @Mapping(target = "responsible", source = "clientSupervisor")
    BulkLoadWithResponsible toBulkLoadWithResponsible(ClientBulkRequestJpaEntity clientBulkRequestJpaEntity);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "names", source = "user.name")
    @Mapping(target = "fatherLastname", source = "user.fatherLastname")
    @Mapping(target = "motherLastname", source = "user.motherLastname")
    BulkLoadResponsible toBulkLoadResponsible(ClientSupervisorJpaEntity clientSupervisorJpaEntity);
}
