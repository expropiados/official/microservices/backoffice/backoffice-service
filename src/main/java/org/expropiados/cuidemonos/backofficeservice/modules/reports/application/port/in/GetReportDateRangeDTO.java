package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class GetReportDateRangeDTO {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate initDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
}
