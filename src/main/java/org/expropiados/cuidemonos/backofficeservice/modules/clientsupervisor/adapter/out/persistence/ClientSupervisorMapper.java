package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence.CompanyMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = { UserMapper.class, CompanyMapper.class })
public interface ClientSupervisorMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "clientCompany", target = "clientCompany")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "profileImage", target = "profileImage")
    @Mapping(source = "isBlocked", target = "isBlocked")
    @Mapping(source = "uuid", target = "uuid")
    ClientSupervisor toClientSupervisor(ClientSupervisorJpaEntity clientSupervisorJpaEntity);
    List<ClientSupervisor> toClientSupervisors(List<ClientSupervisorJpaEntity> patientJpaEntities);

    @InheritInverseConfiguration
    @Mapping(target = "clientCompanyId", ignore = true)
    ClientSupervisorJpaEntity toClientSupervisorJpaEntity(ClientSupervisor clientSupervisor);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "user",target = "user")
    @Mapping(target = "uuid", ignore = true)
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "clientCompany", ignore = true)
    ClientSupervisorJpaEntity toClientSupervisorJpaEntity(ClientSupervisorToRegister clientSupervisorToRegister);

    @Mapping(source = "clientSupervisorId", target = "id")
    @Mapping(source = "user",target = "user")
    @Mapping(target = "uuid", ignore = true)
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(target = "profileImage", ignore = true)
    @Mapping(target = "isBlocked", ignore = true)
    @Mapping(target = "clientCompany", ignore = true)
    ClientSupervisorJpaEntity toClientSupervisorJpaEntity(ClientSupervisorToEdit clientSupervisorToEdit);

}

