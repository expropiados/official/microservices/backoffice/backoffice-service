package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.AllArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.ruc.ApisNetRucAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.CompanyApi;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ValidateClientCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.GetClientCompanyPort;

import java.io.IOException;

@UseCase
@AllArgsConstructor
public class ValidateClientCompanyService implements ValidateClientCompanyUseCase {

    private final GetClientCompanyPort port;

    @Override
    public ClientCompanyChecked validateClientCompany(String ruc) {
        CompanyApi companyApi = null;
        boolean isRegistered = false;
        var clientCompany = port.getClientCompanyByRUC(ruc);
        if (clientCompany != null){
            isRegistered = true;
        }else{
            companyApi = validateExistingCompanyWithApi(ruc);
        }
        return new ClientCompanyChecked(isRegistered,companyApi,clientCompany);
    }

    private CompanyApi validateExistingCompanyWithApi(String ruc){
        var  apisNetRucAdapter = new ApisNetRucAdapter();
        try{
            return apisNetRucAdapter.start(ruc);
        }catch (IOException ex){
            return null;
        }
    }
}
