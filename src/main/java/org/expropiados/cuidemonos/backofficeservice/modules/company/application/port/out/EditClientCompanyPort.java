package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToEdit;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

public interface EditClientCompanyPort {
    Company editClientCompany(ClientCompanyToEdit clientCompanyToEdit);
}
