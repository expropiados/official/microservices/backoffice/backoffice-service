package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.in.RemoveMedicalReasonUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.CheckIfExistMedicalReasonPort;
import org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out.DeleteMedicalReasonPort;

import javax.transaction.Transactional;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RemoveMedicalReasonService implements RemoveMedicalReasonUseCase {
    private final CheckIfExistMedicalReasonPort checkIfExistMedicalReasonPort;
    private final DeleteMedicalReasonPort deleteMedicalReasonPort;

    @Override
    public void removeMedicalReason(Long medicalReasonId) {
        var exists = checkIfExistMedicalReasonPort.checkIfExistMedicalReason(medicalReasonId);

        if (!exists) {
            throw new MedicalReasonNotFoundException(medicalReasonId);
        }

        deleteMedicalReasonPort.deleteMedicalReason(medicalReasonId);
    }
}
