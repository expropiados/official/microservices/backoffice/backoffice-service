package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetExtraInfoForSpecialistByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetSpecialistByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistExtraInfo;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping("v2")
public class GetExtraInfoForSpecialistByUUIDController {

    private final JwtHelper jwtHelper;
    public final GetExtraInfoForSpecialistByUUIDUseCase getExtraInfoForSpecialistByUUIDUseCase;

    @GetMapping(value = "/specialists/{specialistUserUuid}/extra-info")
    public SpecialistExtraInfo getExtraInfoForSpecialistByUUID(@RequestHeader("Authorization") String token,
                                                               @RequestHeader("role") Integer role,
                                                               @PathVariable UUID specialistUserUuid) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getExtraInfoForSpecialistByUUIDUseCase.getExtraInfoForSpecialistByUUID(specialistUserUuid);
    }
}
