package org.expropiados.cuidemonos.backofficeservice.modules.bulk.adapter.port.out.persistence;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;
import org.expropiados.cuidemonos.backofficeservice.modules.providersupervisor.adapter.port.out.persistence.ProviderSupervisorJpaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {BulkLoadMapper.class})
public interface ProviderBulkRequestMapper {

    @Mapping(target = "bulkLoad", source = "bulkLoad")
    @Mapping(target = "responsible", source = "providerSupervisor")
    BulkLoadWithResponsible toBulkLoadWithResponsible(ProviderBulkRequestJpaEntity providerBulkRequestJpaEntity);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "names", source = "user.name")
    @Mapping(target = "fatherLastname", source = "user.fatherLastname")
    @Mapping(target = "motherLastname", source = "user.motherLastname")
    BulkLoadResponsible toBulkLoadResponsible(ProviderSupervisorJpaEntity providerSupervisorJpaEntity);
}
