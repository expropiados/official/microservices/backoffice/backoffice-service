package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web.v2;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParameters;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.GetClientCompanyParametersUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.ClientCompanyNotFoundException;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
public class GetClientCompanyParametersController {
    private final JwtHelper jwtHelper;
    private final GetClientCompanyParametersUseCase getClientCompanyParametersUseCase;

    @GetMapping("/companies/{clientCompanyId}/parameters")
    public ClientCompanyParameters updateProviderCompanyParameters(@RequestHeader("Authorization") String token,
                                                   @RequestHeader("role") Integer role,
                                                   @PathVariable("clientCompanyId") Long clientCompanyId) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        if (userRole != UserRole.PROVIDER_SUPERVISOR) throw new NotAuthorizedForRoleException(userRole);
        return getClientCompanyParametersUseCase.getParametersFromClientCompany(clientCompanyId).orElseThrow(()->new ClientCompanyNotFoundException(clientCompanyId));
    }
}
