package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.out.persistence;

import lombok.Data;
import org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence.UserJpaEntity;

import javax.persistence.*;
import java.util.UUID;

@Data @Entity
@Table(name= "SPECIALIST")
public class SpecialistJpaEntity {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specialist_id")
    private Long id;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name ="email", length = 50, nullable = false)
    private String email;

    @Column(name ="address", length = 100)
    private String address;

    @Column(name ="profile_image", length = 300)
    private String profileImage;

    @Column(name = "phone_number", length = 9)
    private String phoneNumber;

    @Column(name = "about_me", length = 500)
    private String aboutMe;

    @Column(name = "degree", length = 500)
    private String degree;

    @Column(name = "total_rating")
    private Long totalRating = 0L;

    @Column(name = "total_appointments")
    private Long totalAppointments = 0L;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private UserJpaEntity user;

    public Float rating() {
        if (this.totalAppointments == 0L) {
            return 0F;
        }

        return (float) (this.totalRating / this.totalAppointments);
    }
}
