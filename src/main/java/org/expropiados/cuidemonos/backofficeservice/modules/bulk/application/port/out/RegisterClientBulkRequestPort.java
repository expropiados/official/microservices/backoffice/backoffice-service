package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

public interface RegisterClientBulkRequestPort {
    void registerClientBulkRequest(Long clientSupervisorId, Long bulkId);
}
