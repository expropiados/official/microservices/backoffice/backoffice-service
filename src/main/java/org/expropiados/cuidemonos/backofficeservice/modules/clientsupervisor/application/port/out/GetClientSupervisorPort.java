package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;

import java.util.Optional;
import java.util.List;
import java.util.UUID;

public interface GetClientSupervisorPort {
    Optional<ClientSupervisor> getClientSupervisorByUserId(Long idUser);
    List<ClientSupervisor> getClientSupervisorsByCompanyId(Long companyId);
    Optional<ClientSupervisor> getClientSupervisorByUUID(UUID clientSupervisorUUID);
}
