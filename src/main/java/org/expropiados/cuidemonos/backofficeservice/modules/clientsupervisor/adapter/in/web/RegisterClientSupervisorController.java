package org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorDTOMapper;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.ClientSupervisorToRegisterDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.application.port.in.RegisterClientSupervisorUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.clientsupervisor.domain.ClientSupervisor;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.RegisterUserRoleUseCase;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RegisterClientSupervisorController {

    private final JwtHelper jwtHelper;
    public final RegisterClientSupervisorUseCase registerClientSupervisorUseCase;
    public final ClientSupervisorDTOMapper clientSupervisorDTOMapper;
    private final RegisterUserRoleUseCase registerUserRoleUseCase;

    @PostMapping(value = "/companies/supervisor", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ClientSupervisor registerCompany(@ModelAttribute ClientSupervisorToRegisterDTO clientSupervisorToRegisterDTO,
                                            @RequestHeader("Authorization") String token,
                                            @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var clientSupervisorToRegister = clientSupervisorDTOMapper.toClientSupervisorToRegister(clientSupervisorToRegisterDTO);
        var clientSupervisor = registerClientSupervisorUseCase.registerClientCompanySupervisor(clientSupervisorToRegister);
        registerUserRoleUseCase.registerUserRole(clientSupervisor.getEmail(), UserRole.CLIENT_SUPERVISOR);
        return clientSupervisor;
    }
}
