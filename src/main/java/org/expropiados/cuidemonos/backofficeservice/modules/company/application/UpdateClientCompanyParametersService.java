package org.expropiados.cuidemonos.backofficeservice.modules.company.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyParametersDTO;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.UpdateClientCompanyParametersUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out.UpdateClientCompanyParametersPort;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class UpdateClientCompanyParametersService implements UpdateClientCompanyParametersUseCase {

    public final UpdateClientCompanyParametersPort updateClientCompanyParametersPort;


    @Override
    public Optional<Company> updateClientCompanyParameters(Long clientCompanyId, ClientCompanyParametersDTO clientParameters) {
        return updateClientCompanyParametersPort.updateProviderCompanyParameters(clientCompanyId, clientParameters);
    }
}
