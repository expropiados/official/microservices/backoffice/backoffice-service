package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

public interface RegisterBulkJobPort {
    Long registerBulkJob(Long bulkId);
}
