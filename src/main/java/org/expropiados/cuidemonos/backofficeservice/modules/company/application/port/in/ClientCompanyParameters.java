package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ClientCompanyParameters {
    Integer appointmentDuration;
    Integer appointmentMaxCancelled;
    Integer appointmentMaxCant;
    Integer appointmentTimeToCancel;
}
