package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.Optional;
import java.util.UUID;

public interface UpdateClientCompanyParametersUseCase {
    Optional<Company> updateClientCompanyParameters(Long clientCompanyId, ClientCompanyParametersDTO clientCompanyParametersDTO);
}
