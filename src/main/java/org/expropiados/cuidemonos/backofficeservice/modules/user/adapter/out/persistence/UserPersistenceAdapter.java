package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.EditUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements RegisterUserPort, GetUserPort, EditUserPort {

    private final SpringJpaUserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public User registerUser(UserToRegister userToRegister, UUID userUUID) {
        var entity = userMapper.toUserJpaEntity(userToRegister);
        entity.setUuid(userUUID);
        var result = userRepository.save(entity);
        return userMapper.toUser(result);
    }

    @Override
    public User getUserByDocument(String document) {
        var userFound = userRepository.findByDocument(document);
        return userMapper.toUser(userFound);
    }

    @Override
    public User editUser(Long userId, UUID uuid, UserToRegister userToEdit) {
        var entity = userMapper.toUserJpaEntity(userToEdit);
        entity.setId(userId);
        entity.setUuid(uuid);
        var result = userRepository.save(entity);
        return userMapper.toUser(result);
    }
}
