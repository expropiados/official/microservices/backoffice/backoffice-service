package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in;


import lombok.*;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.UserToRegister;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Data
@NoArgsConstructor
public class SpecialistToRegister {

    String specialty;

    @Valid
    UserToRegister user;

    MultipartFile multipartFile;

    String email;

    String phoneNumber;

    String address;

    String profileImage;

    String aboutMe;

    String degree;
}
