package org.expropiados.cuidemonos.backofficeservice.modules.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class User {

    private Long id;
    private UUID uuid;
    private String document;
    private TypeDocument typeDocument;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private LocalDate birthdate;

}
