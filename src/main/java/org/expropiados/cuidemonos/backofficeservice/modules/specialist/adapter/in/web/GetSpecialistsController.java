package org.expropiados.cuidemonos.backofficeservice.modules.specialist.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.GetSpecialistsUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GetSpecialistsController {

    private final JwtHelper jwtHelper;
    private final GetSpecialistsUseCase getSpecialistsUseCase;

    @GetMapping(value = "/specialists")
    public Paginator<Specialist> getSpecialists(Pageable pageable,
                                                @RequestParam(value = "search", defaultValue = "") String search,
                                                @RequestHeader("Authorization") String token,
                                                @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pageSize(pageable.getPageSize())
                .search(search)
                .build();

        return getSpecialistsUseCase.getSpecialists(filters);
    }
}
