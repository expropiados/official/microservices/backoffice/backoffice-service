package org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class BulkLoad {
    Long id;
    BulkType type;
    BulkState state;
    LocalDateTime requestedAt;

    String filename;
    String sourceFileKey;
    String errorSourceFileKey;

    Long successfulRegistered;
    Long failedRegistered;
}
