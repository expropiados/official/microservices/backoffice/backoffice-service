package org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain;

public enum BulkSourceFileType {
    ORIGINAL,
    ERROR
}
