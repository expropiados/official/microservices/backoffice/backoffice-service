package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.config.backoffice.ProviderCompanyConfig;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.GetProviderCompanyUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
public class GetProviderCompanyController {

    private final JwtHelper jwtHelper;
    private final ProviderCompanyConfig providerCompanyConfig;
    private final GetProviderCompanyUseCase getProviderCompanyUseCase;

    @GetMapping("/provider-companies")
    public ProviderCompany getProviderCompany(@RequestHeader("Authorization") String token,
                                              @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var companyUuid = providerCompanyConfig.getUuid();
        return getProviderCompanyUseCase.getProviderCompany(companyUuid)
                .orElseThrow(()->new RuntimeException("provider company not found"));
    }

}
