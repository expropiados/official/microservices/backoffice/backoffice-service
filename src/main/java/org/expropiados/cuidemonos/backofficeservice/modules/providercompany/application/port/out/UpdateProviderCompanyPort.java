package org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.application.port.in.ProviderCompanyToUpdate;
import org.expropiados.cuidemonos.backofficeservice.modules.providercompany.domain.ProviderCompany;

import java.util.UUID;

public interface UpdateProviderCompanyPort {
    ProviderCompany updateProviderCompany(ProviderCompanyToUpdate providerCompany);
}
