package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ClientCompanyToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.company.domain.Company;

import java.util.UUID;

public interface RegisterCompanyPort {
    Company registerCompany(ClientCompanyToRegister company, UUID uuid);
}
