package org.expropiados.cuidemonos.backofficeservice.modules.reports.application.port.in;

import org.expropiados.cuidemonos.backofficeservice.modules.reports.domain.GetMedicalReasonMonthlyReport;

import java.io.IOException;
import java.time.LocalDate;

public interface GetMedicalReasonMonthlyReportUseCase {
    GetMedicalReasonMonthlyReport getMedicalReasonMonthly(LocalDate initDate, LocalDate endDate) throws IOException;
}
