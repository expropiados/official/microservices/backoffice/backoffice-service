package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.out.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface SpringJpaCompanyRepository extends JpaRepository<CompanyJpaEntity, Long> {
    CompanyJpaEntity findByRuc(String ruc);
    Optional<CompanyJpaEntity> findByUuid(UUID uuid);

    @Query("SELECT CC FROM CompanyJpaEntity CC WHERE UPPER(CC.name) LIKE CONCAT('%', UPPER(:search), '%') OR CC.ruc LIKE CONCAT('%', UPPER(:search), '%') ORDER BY CC.id")
    Page<CompanyJpaEntity> searchClientCompanies(Pageable pageable, @Param("search") String search);
}
