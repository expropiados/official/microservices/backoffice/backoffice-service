package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.RegisterBulkJobUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out.RegisterBulkJobPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UseCase
@RequiredArgsConstructor
public class RegisterBulkJobService implements RegisterBulkJobUseCase {

    private final Logger logger = LoggerFactory.getLogger(RegisterBulkJobService.class);
    private final RegisterBulkJobPort registerBulkJobPort;

    @Override
    public void registerBulkJob(Long bulkId) {
        var jobId = registerBulkJobPort.registerBulkJob(bulkId);
        logger.info("Bulk Job has been registered with id {}", jobId);
    }
}
