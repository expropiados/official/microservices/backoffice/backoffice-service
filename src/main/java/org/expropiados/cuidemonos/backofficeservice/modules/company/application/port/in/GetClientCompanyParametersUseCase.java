package org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in;

import java.util.Optional;

public interface GetClientCompanyParametersUseCase {
    Optional<ClientCompanyParameters> getParametersFromClientCompany(Long clientCompanyId);
}
