package org.expropiados.cuidemonos.backofficeservice.modules.bulk.domain;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class BulkLoadToRegister {
    String sourceFileKey;
    String filename;
    BulkType type;
    BulkState state = BulkState.PENDING;
    LocalDateTime requestedAt;
}
