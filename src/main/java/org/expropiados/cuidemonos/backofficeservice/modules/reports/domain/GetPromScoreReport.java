package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetPromScoreReport {
    private double averageScore;
    private List<GetPromScoreReason> scoreXReason;
    private int[][] scoreDetails;
}
