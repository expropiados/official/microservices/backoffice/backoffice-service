package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port;

import lombok.Getter;
import lombok.Value;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.NotFoundException;

import java.io.Serializable;

@Getter
public class MedicalReasonNotFoundException extends RuntimeException implements NotFoundException {
    private final String code = "BOF_MRE_001";
    private final String message;
    private final transient Object data;

    @Value
    static class Data implements Serializable {
        Long medicalReasonId;
    }

    public MedicalReasonNotFoundException(Long medicalReasonId) {
        super();
        this.message = String.format("Medical reason with ID '%d' not found", medicalReasonId);
        this.data = new MedicalReasonNotFoundException.Data(medicalReasonId);
    }
}
