package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application;


import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.errors.RegisterUserException;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.RegisterSpecialistUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.SaveImageProfilePort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out.SaveSpecialistPort;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.in.RegisterUserRoleUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.backofficeservice.modules.user.application.port.out.RegisterUserPort;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;


@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterSpecialistService implements RegisterSpecialistUseCase {

    private final SaveSpecialistPort saveSpecialistPort;
    private final RegisterUserPort registerUserPort;
    private final SaveImageProfilePort saveImageProfilePort;
    private final GetUserPort getUserPort;
    private final ValidateSpecialistByDocumentService validateSpecialistByDocumentService;

    @Override
    public Specialist registerSpecialist(SpecialistToRegister specialistToRegister) {
        var imageFile = specialistToRegister.getMultipartFile();
        var document = specialistToRegister.getUser().getDocument();

        var result= validateSpecialistByDocumentService.validateSpecialistByDocument(document);
        var user = getUserPort.getUserByDocument(document);

        if (imageFile != null){
            var imageProfileUrl = saveImageProfilePort.saveImageProfile(imageFile);
            specialistToRegister.setProfileImage(imageProfileUrl);
        }

        if (user == null) {
            user = registerUserPort.registerUser(specialistToRegister.getUser(), UUID.randomUUID());
        }

        if(!result.getIsSpecialist()) {
            return saveSpecialistPort.saveSpecialist(specialistToRegister, user);
        }

        throw new RegisterUserException(user.getDocument(), "specialist");
    }
}
