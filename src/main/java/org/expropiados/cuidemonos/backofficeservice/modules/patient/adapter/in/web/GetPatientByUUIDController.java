package org.expropiados.cuidemonos.backofficeservice.modules.patient.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.application.port.in.GetPatientByUUIDUseCase;
import org.expropiados.cuidemonos.backofficeservice.modules.patient.domain.Patient;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/patients")
public class GetPatientByUUIDController {

    private final JwtHelper jwtHelper;
    private final GetPatientByUUIDUseCase getPatientByUUIDUseCase;

    @GetMapping(value = "/")
    public Patient getPatientByUUID(@RequestParam UUID patientUUID,
                                    @RequestHeader("Authorization") String token,
                                    @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return getPatientByUUIDUseCase.getPatientByUUID(patientUUID);
    }
}
