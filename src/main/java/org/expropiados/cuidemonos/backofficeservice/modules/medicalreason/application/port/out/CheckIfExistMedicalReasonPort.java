package org.expropiados.cuidemonos.backofficeservice.modules.medicalreason.application.port.out;

public interface CheckIfExistMedicalReasonPort {
    boolean checkIfExistMedicalReason(Long medicalReasonId);
}
