package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Filters;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.queries.Paginator;
import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;

public interface GetAllClientBulkLoadsPort {
    Paginator<BulkLoadWithResponsible> getAllClientBulkLoads(Long clientCompanyId, Filters filters);
}
