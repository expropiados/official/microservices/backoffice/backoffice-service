package org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.out;

import org.expropiados.cuidemonos.backofficeservice.modules.bulk.application.port.in.BulkLoadWithResponsible;

import java.util.Optional;

public interface GetClientBulkLoadPort {
    Optional<BulkLoadWithResponsible> getClientBulkLoad(Long bulkId, Long clientCompanyId);
}
