package org.expropiados.cuidemonos.backofficeservice.modules.user.adapter.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SpringJpaUserRepository extends JpaRepository<UserJpaEntity, Long> {

    UserJpaEntity findByDocument(String document);
}
