package org.expropiados.cuidemonos.backofficeservice.modules.company.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.backofficeservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.backofficeservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.ClientCompanyChecked;
import org.expropiados.cuidemonos.backofficeservice.modules.company.application.port.in.ValidateClientCompanyUseCase;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ValidateClientCompanyController {
    private final JwtHelper jwtHelper;
    private final ValidateClientCompanyUseCase useCase;

    @GetMapping("/companies/checking")
    public ClientCompanyChecked validateClientCompany(@RequestParam(name = "ruc", required = false)String ruc,
                                                      @RequestHeader("Authorization") String token,
                                                      @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return useCase.validateClientCompany(ruc);

    }
}
