package org.expropiados.cuidemonos.backofficeservice.modules.reports.domain;

import lombok.Data;

@Data
public class GetMonthlyPostPollByRatingReport {
    private String name;
    private int[] data;
}
