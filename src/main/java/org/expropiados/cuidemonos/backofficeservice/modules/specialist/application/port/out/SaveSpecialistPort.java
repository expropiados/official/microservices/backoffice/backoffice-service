package org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.out;


import org.expropiados.cuidemonos.backofficeservice.modules.specialist.application.port.in.SpecialistToRegister;
import org.expropiados.cuidemonos.backofficeservice.modules.specialist.domain.Specialist;
import org.expropiados.cuidemonos.backofficeservice.modules.user.domain.User;

public interface SaveSpecialistPort {

    Specialist saveSpecialist(SpecialistToRegister specialistToRegister, User user);
}
