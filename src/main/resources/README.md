# Recursos de la Aplicación

En esta sección se colocarán las variables de entorno usadas para el ambiente de desarrollo.
La manera en como se elige el archivo correcto con la configuración depende del perfil de spring
que esté declarado.

- primero se leen las variables globales. Los nombres de las variables que coincidan con las variables de Spring serán
  priorizadas. Para mayor info, revisar [doc de Spring](https://docs.spring.io/spring-boot/docs/1.5.6.RELEASE/reference/html/boot-features-external-config.html#boot-features-external-config-relaxed-binding).
- luego, según el perfil declarado en la variable de entorno `SPRING_PROFILES_ACTIVE`, se leen las variables en el archivo `application-<profile>.yml`
    ```yaml
      # archivo .env
      SPRING_PROFILES_ACTIVE=dev-local
      
      # archivo application-dev-local.yml
      # ...
        datasource:
          url: jdbc:postgresql://localhost:5432/proof_micro
      # ...
    ```

Actualmente, el archivo `application-dev-local.yml` es ignorado por Git para evitar subir credenciales
por error del ambiente de desarrollo. Por ello, antes de iniciar con el desarrollo, es necesario crear el archivo
`application-dev-local.yml`.

## Recomendación
Puedes copiar el archivo `application-dev.yml` y pegarlo con este nuevo nombre
`application-dev-local.yml`.
```bash
### For H2 In Memory Database
### this config is used to run tests without postgres database
cp application-test.yml application-dev-local.yml

### For PostgreSQL Database
### this config is used to develop
cp application-dev-postgres.yml application-dev-local.yml
```

Sin embargo, puedes cambiar algunas o todas las variables declaradas en ese archivo por credenciales otorgadas para un
ambiente de desarrollo (como base de datos compartida debido a que una en local consume muchos recursos).

## Precaución
Evitar en lo posible modificar los archivos `application-prod.yml`, `application-test.yml` y `application-dev.yml`
a menos que todo el equipo dé su conformidad.

También, no es posible subir otros archivos de configuración en el formato `application-<profile>.yml`, puesto que se ha configurado
Git para ignorar esos archivos.

## Configuración para Producción
La configuración para el ambiente de producción considera las variables de entorno que genera Kubernetes y también se usa
el microservicio de configuración, el cual permite descargar el archivo `.yml` según el perfil solicitado a Spring Cloud
mediante la variable `spring.cloud.config.profile`.
